-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 22, 2018 lúc 04:19 AM
-- Phiên bản máy phục vụ: 10.1.31-MariaDB
-- Phiên bản PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `mixone`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address_setting`
--

CREATE TABLE `address_setting` (
  `id_as` int(11) NOT NULL,
  `name_as` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_as` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone_as` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_as` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city_as` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `districtId_as` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `address_setting`
--

INSERT INTO `address_setting` (`id_as`, `name_as`, `email_as`, `phone_as`, `address_as`, `city_as`, `districtId_as`, `created_at`, `updated_at`) VALUES
(2, 'Shop Anh Duc 2', 'admin@gmail.com', '123654789', 'Quan hoa', 'Hà Nội', 'Cầu Giấy', '2018-08-07 07:01:13', '2018-08-20 07:58:04'),
(3, 'Tầng 6, Tòa nhà Kim Khí Thăng Long, Số 01 Lương Yên,', 'tuananh6@gmail.com', '0963.346.820', 'Q. Hai Bà Trưng', 'Q. Hai Bà Trưng', 'Q. Hai Bà Trưng', '2018-08-20 21:20:14', '2018-08-20 21:20:14');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` char(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` tinyint(4) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `avatar`, `password`, `level`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'toilatoi', 'toilatoi@gmail.com', '508754665Capture.JPG', '$2y$10$Bx2kYpk.pq0.3L4lLhp3COx2..BOYnnlMmzTjL8vf3angelXspvHq', 1, 'HsfkPS7y3az79YUTv2XjTtXiQpjKlZpLcEkBpt8R', '2018-07-17 17:00:00', '2018-08-16 20:27:39'),
(5, 'toilatoi434432', 'toilatoi12231354@gmail.com', '1973183822mypham2.JPG', '$2y$10$r13zELVKK0blThOT2OHsKe125Idz24LNNsseEoBfkGoJmpi0G37Am', NULL, 'HsfkPS7y3az79YUTv2XjTtXiQpjKlZpLcEkBpt8R', '2018-07-24 17:00:00', '2018-08-16 20:27:27'),
(6, 'tuan', 'tuan@gmail.com', '274486511mypham2.JPG', '$2y$10$k2764DgRG./oBn37I8A/tulROLgRJlK.WW0ZR7W6iXlUgByLzomAm', 1, NULL, '2018-08-16 20:26:23', '2018-08-16 20:28:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `banner`
--

CREATE TABLE `banner` (
  `id` int(10) UNSIGNED NOT NULL,
  `bn_images` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `banner`
--

INSERT INTO `banner` (`id`, `bn_images`, `created_at`, `updated_at`) VALUES
(4, '1136601275slider1.jpg', NULL, NULL),
(5, '1330743803banner2.png', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blogs`
--

CREATE TABLE `blogs` (
  `bl_id` int(10) UNSIGNED NOT NULL,
  `bl_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bl_content` longtext COLLATE utf8mb4_unicode_ci,
  `bl_summary` longtext CHARACTER SET utf8,
  `bl_titlepage` longtext COLLATE utf8mb4_unicode_ci,
  `bl_des` longtext COLLATE utf8mb4_unicode_ci,
  `bl_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bl_tags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bl_active` int(11) NOT NULL DEFAULT '0',
  `bl_images` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bl_author` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'admin',
  `bl_cate` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `blogs`
--

INSERT INTO `blogs` (`bl_id`, `bl_title`, `bl_content`, `bl_summary`, `bl_titlepage`, `bl_des`, `bl_alias`, `bl_tags`, `bl_active`, `bl_images`, `bl_author`, `bl_cate`, `created_at`, `updated_at`) VALUES
(2, 'sao lưu dữ liệu', '<p>Blog_cateBlog_cateBlog_cateBlog_cateBlog_cateBlog_cateBlog_cate</p>', '<p>Blog_cateBlog_cate</p>', 'sao lưu dữ liệu', 'Blog_cateBlog_cateBlog_cate', 'sao-luu-du-lieu', 'sao-luu', 0, '3598344222.JPG', 'toilatoi', 6, '2018-08-18 08:37:39', '2018-08-18 08:37:39');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blog_cates`
--

CREATE TABLE `blog_cates` (
  `bc_id` int(10) UNSIGNED NOT NULL,
  `bc_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bc_summary` longtext COLLATE utf8mb4_unicode_ci,
  `bc_alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bc_meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bc_meta_description` longtext COLLATE utf8mb4_unicode_ci,
  `bc_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bc_active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `blog_cates`
--

INSERT INTO `blog_cates` (`bc_id`, `bc_name`, `bc_summary`, `bc_alias`, `bc_meta_title`, `bc_meta_description`, `bc_avatar`, `bc_active`, `created_at`, `updated_at`) VALUES
(6, 'Dịch vụ', '<h1>Đếm Ng&agrave;y Xa Em | Only C ft. Lou Ho&agrave;ng | Official MV | Nhạc trẻ mới hay tuyển chọn</h1>', 'dich-vu', 'Dịch vụ', 'Đếm Ngày Xa Em | Only C ft. Lou Hoàng | Official MV | Nhạc trẻ mới hay tuyển chọn', '10326043542.JPG', 1, '2018-08-18 01:07:15', '2018-08-18 01:07:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menus`
--

CREATE TABLE `menus` (
  `mn_id` int(10) UNSIGNED NOT NULL,
  `mn_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mn_content` longtext CHARACTER SET utf8,
  `mn_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menus`
--

INSERT INTO `menus` (`mn_id`, `mn_name`, `mn_content`, `mn_alias`, `created_at`, `updated_at`) VALUES
(1, 'Hướng Dẫn', '<h1>[ Thuyết Minh ] Người Bảo Vệ Bầy S&oacute;i - Phim H&agrave;nh Động Hay Nhất 2018</h1>\r\n\r\n<h1>&nbsp;</h1>\r\n\r\n<p>&nbsp;</p>', 'huong-dan', '2018-08-17 22:39:08', '2018-08-20 02:25:17'),
(3, 'Về chúng tôi', '<h1>[ Thuyết Minh ] Người Bảo Vệ Bầy S&oacute;i - Phim H&agrave;nh Động Hay Nhất 2018</h1>\r\n\r\n<h1>[ Thuyết Minh ] Người Bảo Vệ Bầy S&oacute;i - Phim H&agrave;nh Động Hay Nhất 2018</h1>\r\n\r\n<h1>[ Thuyết Minh ] Người Bảo Vệ Bầy S&oacute;i - Phim H&agrave;nh Động Hay Nhất 2018</h1>\r\n\r\n<h1>[ Thuyết Minh ] Người Bảo Vệ Bầy S&oacute;i - Phim H&agrave;nh Động Hay Nhất 2018</h1>\r\n\r\n<h1>[ Thuyết Minh ] Người Bảo Vệ Bầy S&oacute;i - Phim H&agrave;nh Động Hay Nhất 2018</h1>', 've-chung-toi', '2018-08-17 23:57:57', '2018-08-17 23:57:57');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2018_07_26_042229_create_product_category_table', 1),
(2, '2018_07_26_042923_create_product_vendor_table', 1),
(3, '2018_07_26_043119_create_orders_table', 1),
(4, '2018_07_26_045457_create_product_type_table', 1),
(5, '2018_07_26_053015_create_products_table', 1),
(6, '2018_07_29_052910_create_admins_table', 2),
(7, '2018_07_30_151224_create_settings_table', 2),
(8, '2018_07_30_154215_create_settings_table', 3),
(9, '2018_08_03_083446_create_product_categoies_table', 4),
(10, '2018_08_03_084238_create_product_type_table', 4),
(11, '2018_08_03_085036_create_product_vendor_table', 4),
(12, '2018_08_03_086019_create_products_table', 4),
(13, '2018_08_06_091849_create_ImageDetails_table', 5),
(14, '2018_08_09_094927_create_users_table', 6),
(15, '2018_08_09_101150_create_users_table', 7),
(16, '2018_08_13_032423_create_blog_cates_table', 8),
(17, '2018_08_13_042544_create_blog_cates_table', 9),
(18, '2018_08_13_043359_create_blogs_table', 10),
(19, '2018_08_17_095516_create_menus_table', 11),
(20, '2018_08_21_022231_create_banner_table', 12),
(21, '2018_08_21_090519_create_suppliers_table', 13);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `orders`
--

CREATE TABLE `orders` (
  `od_id` int(10) UNSIGNED NOT NULL,
  `od_code` int(11) NOT NULL,
  `od_product` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `od_price` int(11) DEFAULT NULL,
  `od_sale` text COLLATE utf8mb4_unicode_ci,
  `od_trans_mth` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `od_trans_fee` int(11) DEFAULT NULL,
  `od_total_price` int(11) DEFAULT NULL,
  `od_customer` text COLLATE utf8mb4_unicode_ci,
  `od_tags` text COLLATE utf8mb4_unicode_ci,
  `od_payment` tinyint(4) NOT NULL DEFAULT '0',
  `od_status` tinyint(4) NOT NULL DEFAULT '0',
  `od_ship` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `pro_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pro_content` longtext COLLATE utf8mb4_unicode_ci,
  `pro_summary` longtext COLLATE utf8mb4_unicode_ci,
  `pro_images` text COLLATE utf8mb4_unicode_ci,
  `pro_price` int(11) DEFAULT NULL,
  `pro_com_price` int(11) DEFAULT NULL,
  `pro_taxable` tinyint(4) DEFAULT NULL,
  `pro_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_barcode` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_qty` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_policy` tinyint(4) NOT NULL DEFAULT '0',
  `pro_sta_shopping` tinyint(4) DEFAULT NULL,
  `pro_mass` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `pro_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_material` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_status` tinyint(4) NOT NULL DEFAULT '1',
  `pro_type` int(10) UNSIGNED NOT NULL,
  `pro_vendor` int(10) UNSIGNED NOT NULL,
  `pro_cate` int(10) UNSIGNED NOT NULL,
  `pro_tags` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_meta_des` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pro_alias` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `products`
--

INSERT INTO `products` (`id`, `pro_name`, `pro_content`, `pro_summary`, `pro_images`, `pro_price`, `pro_com_price`, `pro_taxable`, `pro_code`, `pro_barcode`, `pro_qty`, `pro_policy`, `pro_sta_shopping`, `pro_mass`, `pro_size`, `pro_color`, `pro_material`, `pro_status`, `pro_type`, `pro_vendor`, `pro_cate`, `pro_tags`, `pro_meta_title`, `pro_meta_des`, `pro_alias`, `created_at`, `updated_at`) VALUES
(10, 'a', NULL, NULL, '', 0, NULL, 0, 'masp55', NULL, '1', 0, 1, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 'a', 'a', NULL, 'a', '2018-08-07 02:23:41', '2018-08-07 02:23:41'),
(11, 'Phầm mềm 1', '<div class=\"col740 fr\">\r\n<div id=\"main-detail\">\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<div class=\"share_image\">\r\n<div class=\"item_gallery\">\r\n<div class=\"thumb_detail\"><img alt=\"undefined\" class=\"img-responsive\" src=\"https://media.thethao247.vn/upload/kienlv/2018/08/12/Screenshot_17.jpg\" title=\"Trang Vidio chiếu trực tiếp các trận đấu bóng đá nam ASIAD 2018\" /></div>\r\nTrang Vidio chiếu trực tiếp c&aacute;c trận đấu b&oacute;ng đ&aacute; nam ASIAD 2018</div>\r\n</div>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Vidio - đơn vị nắm bản quyền truyền h&igrave;nh ASIAD 2018 của nước chủ nh&agrave; Indonesia sẽ ph&aacute;t s&oacute;ng miễn ph&iacute; to&agrave;n bộ c&aacute;c m&ocirc;n thi đấu của c&aacute;c nước, trong đ&oacute; c&oacute; b&oacute;ng đ&aacute;. Điều n&agrave;y mở ra cơ hội cho người h&acirc;m mộ ở c&aacute;c quốc gia kh&ocirc;ng hoặc chưa c&oacute; bản quyền truyền h&igrave;nh như Việt Nam c&oacute; thể xem trực tiếp c&aacute;c m&ocirc;n thi c&oacute; VĐV nước nh&agrave; th&ocirc;ng qua h&igrave;nh thức internet.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Du lịch T&acirc;y Java xem U23 Việt Nam vs U23 Nhật Bản thi đấu</strong></p>\r\n\r\n<p style=\"text-align:justify\">Theo th&ocirc;ng tin từ nước chủ nh&agrave; Indonesia, s&acirc;n vận động (SVĐ) Wibawa Mukti được chọn l&agrave;m địa điểm tổ chức c&aacute;c trận đấu thuộc bảng D m&ocirc;n b&oacute;ng đ&aacute; nam ASIAD 2018. Bảng đấu c&oacute; sự hiện diện của tuyển <strong>U23 Việt Nam</strong>, c&ugrave;ng ba đối thủ l&agrave; <a href=\"https://www.google.com/url?q=https://thethao247.vn/team/u23-nhat-ban-tlq1228/&amp;sa=U&amp;ved=0ahUKEwjvx6CFiPPcAhVFQN4KHd9lAIIQFggKMAI&amp;client=internal-uds-cse&amp;cx=006273320419413644978:o25achcd1tw&amp;usg=AOvVaw17fRTZBbYJmNlA8HFTSSyw\" target=\"_blank\">U23 Nhật Bản</a>, Nepal v&agrave; Pakistan.</p>\r\n\r\n<div class=\"584125381\" id=\"video-ads-show\">&nbsp;</div>\r\n\r\n<div class=\"ads-detail-inline ads_content\" id=\"ads_content\">\r\n<div id=\"bs_mobileinpage\">&nbsp;</div>\r\n\r\n<div id=\"bdb398f5bd47907074cbf67f6c3c3567_sync\">\r\n<div id=\"sas_44269\">&nbsp;</div>\r\n\r\n<div id=\"wrapper-inread-video-670bb33931534c96a41e79d98f19dc8c\" style=\"background:transparent none repeat scroll 0% 0%; border:medium none; display:block; height:0px; left:0px; margin-left:auto; margin-right:auto; overflow:hidden; padding:0px; right:0px; width:100%\"><iframe id=\"content-inread-video-670bb33931534c96a41e79d98f19dc8c\" style=\"display: block; overflow: hidden; background: transparent none repeat scroll 0% 0%; width: 100%; height: 276px; right: 0px; left: 0px; margin-left: auto; margin-right: auto; padding: 0px; position: relative; z-index: 2147483647; border: medium none;\"></iframe></div>\r\n</div>\r\n</div>\r\n\r\n<div id=\"AdAsia\">&nbsp;</div>\r\n\r\n<div id=\"bs_mobileinpage\">\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n\r\n<p style=\"text-align:justify\">Wibawa Mukti (hoặc Stadion Wibawa Mukti) l&agrave; s&acirc;n vận động đa năng, nằm ở Bekasi Regency với sức chứa gần 30.000 người. C&aacute;ch thủ đ&ocirc; Jakarta khoảng 50 km, Bekasi l&agrave; một th&agrave;nh phố thuộc tỉnh T&acirc;y Java, tr&ecirc;n đảo Java, đ&acirc;y l&agrave; th&agrave;nh phố lớn thứ tư của Indonesia.</p>\r\n\r\n<p style=\"text-align:justify\">Ngo&agrave;i Bekasi, NHM c&ograve;n c&oacute; thể du lịch nhiều địa điểm tr&ecirc;n đảo Java, bởi đ&acirc;y c&oacute; thể coi như điểm đến &ldquo;định nghĩa&rdquo; ch&iacute;nh x&aacute;c nhất đất nước Indonesia: m&uacute;a rối b&oacute;ng, lễ nghi cổ truyền, những điệu nhảy đi&ecirc;u luyện hay n&uacute;i lửa phun tr&agrave;o c&ugrave;ng c&aacute;c cảnh quan v&ocirc; tận. C&aacute;c điểm tham quan ch&iacute;nh ở h&ograve;n đảo n&agrave;y l&agrave; đảo n&uacute;i lửa Krakatoa v&agrave; c&ocirc;ng vi&ecirc;n quốc gia Ujung Kulon - qu&ecirc; hương của lo&agrave;i t&ecirc; gi&aacute;c Java đang đứng trước nguy cơ tuyệt chủng.</p>\r\n\r\n<p style=\"text-align:justify\">Tỉnh Banyunwangi ở miền Đ&ocirc;ng đảo Java lại khiến du th&iacute;ch say m&ecirc; với suối nước n&oacute;ng, c&aacute;c đồn điền c&agrave; ph&ecirc;, vườn gia vị, n&uacute;i lửa dạng tầng v&agrave; hồ nham thạch. Trong khi đ&oacute;, về ph&iacute;a T&acirc;y, sự h&ugrave;ng vĩ của biểu tượng đất nước Indonesia &ndash; n&uacute;i Bromo, bao quanh l&agrave; sa mạc với b&atilde;i c&aacute;t đen khiến nhiều du kh&aacute;ch th&iacute;ch th&uacute;.</p>\r\n\r\n<p style=\"text-align:justify\"><strong>Lịch thi đấu của <a href=\"https://www.google.com/url?q=https://thethao247.vn/team/u23-viet-nam-tlq981/&amp;sa=U&amp;ved=0ahUKEwiKiuSZiPPcAhWXUd4KHTGECrMQFggEMAA&amp;client=internal-uds-cse&amp;cx=006273320419413644978:o25achcd1tw&amp;usg=AOvVaw2aS1B-NGV4dtJgpOLVez9a\" target=\"_blank\">U23 Việt Nam</a> tại ASIAD 2018</strong></p>\r\n\r\n<p style=\"text-align:justify\">Ng&agrave;y 19/8:</p>\r\n\r\n<p style=\"text-align:justify\">16h00: U23 Việt Nam vs U23 Nhật Bản</p>\r\n\r\n<p style=\"text-align:justify\">16h00: U23 Pakistan - U23 Nepal</p>\r\n\r\n<hr />\r\n<p>Thể Thao 247 cung cấp link xem trực tiếp <a href=\"https://www.google.com/url?q=https://thethao247.vn/team/u23-viet-nam-tlq981/&amp;sa=U&amp;ved=0ahUKEwiKiuSZiPPcAhWXUd4KHTGECrMQFggEMAA&amp;client=internal-uds-cse&amp;cx=006273320419413644978:o25achcd1tw&amp;usg=AOvVaw2aS1B-NGV4dtJgpOLVez9a\" target=\"_blank\">U23 Việt Nam</a> tại ASIAD 2018. Cập nhật <a href=\"https://thethao247.vn/72-lich-thi-dau-bong-da-nam-asiad-2018-ltd-bong-da-asiad-2018-d162819.html\" target=\"_blank\">lịch thi đấu ASIAD 2018 của U23 Việt Nam</a>. Cập nhật trực tiếp những tin tức n&oacute;ng hổi của U23 Việt Nam tr&ecirc;n đất Indonesia. Trực tiếp U23 Việt Nam vs <a href=\"https://www.google.com/url?q=https://thethao247.vn/team/u23-nhat-ban-tlq1228/&amp;sa=U&amp;ved=0ahUKEwjvx6CFiPPcAhVFQN4KHd9lAIIQFggKMAI&amp;client=internal-uds-cse&amp;cx=006273320419413644978:o25achcd1tw&amp;usg=AOvVaw17fRTZBbYJmNlA8HFTSSyw\" target=\"_blank\">U23 Nhật Bản</a>, xem trực tiếp U23 Việt Nam ở đ&acirc;u, trực tiếp ASIAD 2018...</p>\r\n</div>\r\n\r\n<div class=\"mar_bottom20 soucre_news\">Thể Thao 247 - Tinnhanhonline.vn</div>\r\n\r\n<div class=\"ads\">\r\n<div class=\"ads-detail-bottom ads_content\" id=\"ads_content\">\r\n<div id=\"07e1cd7dca89a1678042477183b7ac3f\">\r\n<div class=\"head_119\">&nbsp;</div>\r\n\r\n<table class=\"block_119\">\r\n	<tbody>\r\n		<tr>\r\n			<td>\r\n			<div class=\"img_wrap_119\"><a href=\"https://click.advertnative.com/move/?i=MTc4LDExOQ==&amp;h=488c829a149ef6a9f21f8fb316574181\" target=\"_blank\"><img alt=\"\" class=\"img_119\" src=\"https://advertnative.com/storage/teaser/image/3/28/1534234977-BbR_200.jpg\" /></a></div>\r\n\r\n			<div class=\"title_119\"><a href=\"https://click.advertnative.com/move/?i=MTc4LDExOQ==&amp;h=488c829a149ef6a9f21f8fb316574181\" target=\"_blank\">Thanh l&yacute; iPhone 8 gi&aacute; sỉ. Chỉ c&ograve;n lại 2 ng&agrave;y - khuyến m&atilde;i shock </a></div>\r\n\r\n			<div class=\"description_119\"><a href=\"https://click.advertnative.com/move/?i=MTc4LDExOQ==&amp;h=488c829a149ef6a9f21f8fb316574181\" target=\"_blank\">Nhanh tay đặt h&agrave;ng! Chỉ c&ograve;n lại 25 sản phẩm. - Giảm gi&aacute; 50%</a></div>\r\n			</td>\r\n			<td>\r\n			<div class=\"img_wrap_119\"><a href=\"https://click.advertnative.com/move/?i=MTI2LDExOQ==&amp;h=f4e3b89eba8b39d9a754b8111b0fc89f\" target=\"_blank\"><img alt=\"\" class=\"img_119\" src=\"https://advertnative.com/storage/teaser/image/3/1/1533831930-BF5_200.jpg\" /></a></div>\r\n\r\n			<div class=\"title_119\"><a href=\"https://click.advertnative.com/move/?i=MTI2LDExOQ==&amp;h=f4e3b89eba8b39d9a754b8111b0fc89f\" target=\"_blank\">Điện thoại giảm gi&aacute; 50%! Samsung S8! C&ograve;n lại 6 sản phẩm.</a></div>\r\n\r\n			<div class=\"description_119\"><a href=\"https://click.advertnative.com/move/?i=MTI2LDExOQ==&amp;h=f4e3b89eba8b39d9a754b8111b0fc89f\" target=\"_blank\">Điện thoại gi&aacute; ưu đ&atilde;i</a></div>\r\n			</td>\r\n			<td>\r\n			<div class=\"img_wrap_119\"><a href=\"https://click.advertnative.com/move/?i=MTIzLDExOQ==&amp;h=ea0ddad64440b8fceae623f140fb5445\" target=\"_blank\"><img alt=\"\" class=\"img_119\" src=\"https://advertnative.com/storage/teaser/image/3/4/1533799298-7KK_200.jpg\" /></a></div>\r\n\r\n			<div class=\"title_119\"><a href=\"https://click.advertnative.com/move/?i=MTIzLDExOQ==&amp;h=ea0ddad64440b8fceae623f140fb5445\" target=\"_blank\">Bằng c&aacute;ch n&agrave;y, h&agrave;ng x&oacute;m của bạn từ l&acirc;u đ&atilde; kh&ocirc;ng phải trả tiền điện</a></div>\r\n\r\n			<div class=\"description_119\"><a href=\"https://click.advertnative.com/move/?i=MTIzLDExOQ==&amp;h=ea0ddad64440b8fceae623f140fb5445\" target=\"_blank\">Chỉ cần t&iacute;ch hợp c&ocirc;ng cụ n&agrave;y v&agrave;o đồng hồ điện&hellip;</a></div>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', 'undefined\r\nTrang Vidio chiếu trực tiếp các trận đấu bóng đá nam ASIAD 2018\r\n\r\nVidio - đơn vị nắm bản quyền truyền hình ASIAD 2018 của nước chủ nhà Indonesia sẽ phát sóng miễn phí toàn bộ các môn thi đấu của các nước, trong đó có bóng đá. Điều này mở ra cơ hội cho người hâm mộ ở các quốc gia không hoặc chưa có bản quyền truyền hình như Việt Nam có thể xem trực tiếp các môn thi có VĐV nước nhà thông qua hình thức internet.\r\n\r\nDu lịch Tây Java xem U23 Việt Nam vs U23 Nhật Bản thi đấu\r\n\r\nTheo thông tin từ nước chủ nhà Indonesia, sân vận động (SVĐ) Wibawa Mukti được chọn làm địa điểm tổ chức các trận đấu thuộc bảng D môn bóng đá nam ASIAD 2018. Bảng đấu có sự hiện diện của tuyển U23 Việt Nam, cùng ba đối thủ là U23 Nhật Bản, Nepal và Pakistan.\r\n\r\nWibawa Mukti (hoặc Stadion Wibawa Mukti) là sân vận động đa năng, nằm ở Bekasi Regency với sức chứa gần 30.000 ngư', '', 0, NULL, 0, NULL, NULL, '1', 0, 1, NULL, NULL, NULL, NULL, 0, 1, 1, 1, 'bac', 'Phầm mềm 1', NULL, 'pham-mem-1', '2018-08-18 05:33:45', '2018-08-18 05:33:45'),
(12, 'Thiết bị 1', '<p>the&iacute;t bịthe&iacute;t bịthe&iacute;t bịthe&iacute;t bịthe&iacute;t bị</p>', 'theít bịtheít bịtheít bịtheít bị', '1234162336mypham2.JPG', NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 3, 1, 1, 'sf', 'Thiết bị 1', 'theít bị', 'thiet-bi-1', '2018-08-18 05:37:58', '2018-08-20 00:18:29'),
(13, 'thiết bị 2', '<p>Dữ liệu l&agrave; t&agrave;i nguy&ecirc;n qu&yacute; gi&aacute; của bất kỳ doanh nghiệp hay tổ chức n&agrave;o. Tuy nhi&ecirc;n t&agrave;i nguy&ecirc;n qu&yacute; gi&aacute; đ&oacute; đang đối diện với c&aacute;c nguy cơ v&agrave; th&aacute;ch thức. Ch&uacute;ng t&ocirc;i thấu hiểu được sự quan trọng của dữ liệu. Do đ&oacute;, ch&uacute;ng t&ocirc;i đưa ra giải ph&aacute;p sao lưu chuy&ecirc;n dụng với những thiết bị v&agrave; c&ocirc;ng nghệ lu&ocirc;n cập nhật mới nhất, ph&ugrave;</p>', 'Dữ liệu là tài nguyên quý giá của bất kỳ doanh nghiệp hay tổ chức nào. Tuy nhiên tài nguyên quý giá đó đang đối diện với các nguy cơ và thách thức. Chúng tôi thấu hiểu được sự quan trọng của dữ liệu. Do đó, chúng tôi đưa ra giải pháp sao lưu chuyên dụng với những thiết bị và công nghệ luôn cập nhật mới nhất, phù', '14267205182.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 3, 1, 1, 'test', 'thiết bị 2', 'was only partially uploaded', 'thiet-bi-2', '2018-08-19 23:46:11', '2018-08-20 00:20:19'),
(14, 'thiết bị 3', '<h3><a href=\"https://translate.google.com/?hl=vi\">Google Dịch</a></h3>\r\n\r\n<div class=\"s\">\r\n<div>\r\n<div class=\"TbwUpd f hJND5c\" style=\"white-space:nowrap\"><cite>https://translate.google.com/?hl=vi</cite>\r\n\r\n<div class=\"ab_ctl action-menu\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<ol>\r\n</ol>\r\n\r\n<p><em>Dịch</em> vụ miễn ph&iacute; của Google <em>dịch</em> nhanh c&aacute;c từ, cụm từ v&agrave; trang web giữa tiếng Việt v&agrave; hơn 100 ng&ocirc;n ngữ kh&aacute;c.</p>', 'Google Dịch\r\nhttps://translate.google.com/?hl=vi\r\n\r\nDịch vụ miễn phí của Google dịch nhanh các từ, cụm từ và trang web giữa tiếng Việt và hơn 100 ngôn ngữ khác.', '1765592646mypham2.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 3, 1, 1, 'thietbi', 'thiết bị 3', 'Google Dịch\r\nhttps://translate.google.com/?hl=vi\r\n\r\nDịch vụ miễn phí của Google dịch nhanh các từ, cụm từ và trang web giữa tiếng Việt và hơn 100 ngôn ngữ khác.', 'thiet-bi-3', '2018-08-20 00:23:18', '2018-08-20 07:47:18'),
(15, 'sp thanh lý', '<h3><a href=\"https://translate.google.com/?hl=vi\">Google Dịch</a></h3>\r\n\r\n<div class=\"s\">\r\n<div>\r\n<div class=\"TbwUpd f hJND5c\" style=\"white-space:nowrap\"><cite>https://translate.google.com/?hl=vi</cite>\r\n\r\n<div class=\"ab_ctl action-menu\">&nbsp;</div>\r\n</div>\r\n</div>\r\n</div>\r\n\r\n<ol>\r\n</ol>\r\n\r\n<p><em>Dịch</em> vụ miễn ph&iacute; của Google <em>dịch</em> nhanh c&aacute;c từ, cụm từ v&agrave; trang web giữa tiếng Việt v&agrave; hơn 100 ng&ocirc;n ngữ kh&aacute;c.</p>', 'Google Dịch\r\nhttps://translate.google.com/?hl=vi\r\n\r\nDịch vụ miễn phí của Google dịch nhanh các từ, cụm từ và trang web giữa tiếng Việt và hơn 100 ngôn ngữ khác.', '157502783mypham2.JPG', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 4, 1, 1, 'thanhly', 'sp thanh lý', 'Google Dịch\r\nhttps://translate.google.com/?hl=vi\r\n\r\nDịch vụ miễn phí của Google dịch nhanh các từ, cụm từ và trang web giữa tiếng Việt và hơn 100 ngôn ngữ khác.', 'sp-thanh-ly', '2018-08-20 00:26:20', '2018-08-20 00:26:20'),
(17, 'tessstttttttttttt', '<p>&nbsp;return redirect(&#39;admin/products/listPro&#39;)&nbsp;return redirect(&#39;admin/products/listPro&#39;)&nbsp;return redirect(&#39;admin/products/listPro&#39;)</p>', 'return redirect(\'admin/products/listPro\')', '171073023626685273_954937841322178_7894771701088621173_o.jpg', NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 3, 1, 1, 'abcv', 'tessstttttttttttt', 'return redirect(\'admin/products/listPro\')', 'tessstttttttttttt', '2018-08-20 07:50:20', '2018-08-20 07:50:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_categoies`
--

CREATE TABLE `product_categoies` (
  `pc_id` int(10) UNSIGNED NOT NULL,
  `pc_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pc_summary` longtext COLLATE utf8mb4_unicode_ci,
  `pc_alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pc_meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pc_meta_description` longtext COLLATE utf8mb4_unicode_ci,
  `pc_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pc_active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_categoies`
--

INSERT INTO `product_categoies` (`pc_id`, `pc_name`, `pc_summary`, `pc_alias`, `pc_meta_title`, `pc_meta_description`, `pc_avatar`, `pc_active`, `created_at`, `updated_at`) VALUES
(1, 'sản phẩm', '<h1>Th&igrave; Th&ocirc;i - Reddy | MV Lyrics HD</h1>', 'san-pham', 'sản phẩm', 'Thì Thôi - Reddy | MV Lyrics HD', '1362284751Capture.JPG', 1, '2018-08-03 02:46:34', '2018-08-18 01:15:37');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_type`
--

CREATE TABLE `product_type` (
  `pt_id` int(10) UNSIGNED NOT NULL,
  `pt_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pt_cate` int(11) DEFAULT NULL,
  `pt_summary` longtext COLLATE utf8mb4_unicode_ci,
  `pt_alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pt_meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pt_meta_description` longtext COLLATE utf8mb4_unicode_ci,
  `pt_avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pt_active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_type`
--

INSERT INTO `product_type` (`pt_id`, `pt_name`, `pt_cate`, `pt_summary`, `pt_alias`, `pt_meta_title`, `pt_meta_description`, `pt_avatar`, `pt_active`, `created_at`, `updated_at`) VALUES
(1, 'Phần mềm', 1, '<p>sản phẩm mới</p>', 'phan-mem', 'Phần mềm', 'sản phẩm mớisản phẩm mới', '17157731933.JPG', 1, '2018-08-03 02:04:27', '2018-08-18 04:39:51'),
(3, 'Thiết bị', 1, '<p>U23 Việt Nam nằm tại bảng D ASIAD 2018 với&nbsp;Nhật Bản, Nepal v&agrave; Pakistan. C&aacute;c chuy&ecirc;n gia h&agrave;ng đầu Ch&acirc;u &Aacute; nhận định ngo&agrave;i Nhật Bản th&igrave; Việt Nam l&agrave; đội c&oacute; cơ hội rất lớn để đi tiếp, bởi Nepal v&agrave; Pakistan đều rất yếu.</p>', 'thiet-bi', NULL, NULL, '', 1, '2018-08-18 05:20:14', '2018-08-18 05:20:14'),
(4, 'Thanh lý', 1, '<p>Mới đ&acirc;y, h&atilde;ng Vidio li&ecirc;n tục ph&aacute;t s&oacute;ng trực tiếp c&aacute;c m&ocirc;n thi đấu, trong đ&oacute; c&oacute; cả m&ocirc;n b&oacute;ng đ&aacute; nam. Cụ thể ở đ&acirc;y l&agrave; c&aacute;c trận đấu trong khu&ocirc;n khổ bảng A với chất lượng h&igrave;nh ảnh rất tốt.</p>', 'thanh-ly', 'Thanh lý', 'Mới đây, hãng Vidio liên tục phát sóng trực tiếp các môn thi đấu, trong đó có cả môn bóng đá nam. Cụ thể ở đây là các trận đấu trong khuôn khổ bảng A với chất lượng hình ảnh rất tốt.', '1806342120mypham2.JPG', 1, '2018-08-18 05:23:44', '2018-08-18 05:23:44');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_vendor`
--

CREATE TABLE `product_vendor` (
  `pv_id` int(10) UNSIGNED NOT NULL,
  `pv_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pv_summary` longtext COLLATE utf8mb4_unicode_ci,
  `pv_alias` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv_meta_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pv_meta_description` longtext COLLATE utf8mb4_unicode_ci,
  `pv_avatar` longtext COLLATE utf8mb4_unicode_ci,
  `pv_active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `product_vendor`
--

INSERT INTO `product_vendor` (`pv_id`, `pv_name`, `pv_summary`, `pv_alias`, `pv_meta_title`, `pv_meta_description`, `pv_avatar`, `pv_active`, `created_at`, `updated_at`) VALUES
(1, 'samsung', NULL, 'samsung', 'samsung', NULL, '', 1, '2018-08-03 02:45:13', '2018-08-03 02:45:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `webname` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `company` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_one` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address_two` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fax` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email2` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `iframe_map` text CHARACTER SET utf8,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `google` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `GA` text CHARACTER SET utf8,
  `fbPixel` text CHARACTER SET utf8,
  `favicon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) CHARACTER SET utf8 DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `settings`
--

INSERT INTO `settings` (`id`, `title`, `webname`, `company`, `address_one`, `address_two`, `phone1`, `phone2`, `fax`, `email`, `email2`, `iframe_map`, `facebook`, `google`, `GA`, `fbPixel`, `favicon`, `logo`, `keywords`, `created_at`, `updated_at`) VALUES
(1, 'mixoneeereêrerdfádf', 'mixone.vn', 'Công ty mixonee', 'quản ngọc,quản xương', NULL, '0987465', '097546513', '24123416516', 'tuansanh@gmail.com', 'mixone@gmail.com', '<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3723.9259660277094!2d105.78964151451423!3d21.03564808599467!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab482480e97f%3A0x25001096b082a48a!2zMzMxIEPhuqd1IEdp4bqleQ!5e0!3m2!1svi!2s!4v1534836614176\" width=\"100%\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>', 'Facebook.com', NULL, NULL, NULL, '2053844544bocongthuong1-min.png', '382365059logo.png', 'đây là trang chủ', '2018-08-01 02:16:15', '2018-08-01 02:16:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `Spli_images` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `checkBuy` int(11) NOT NULL DEFAULT '0',
  `point` int(11) DEFAULT NULL,
  `addresss` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthdays` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sex` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `address_setting`
--
ALTER TABLE `address_setting`
  ADD PRIMARY KEY (`id_as`);

--
-- Chỉ mục cho bảng `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Chỉ mục cho bảng `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`bl_id`),
  ADD KEY `blogs_bl_cate_foreign` (`bl_cate`);

--
-- Chỉ mục cho bảng `blog_cates`
--
ALTER TABLE `blog_cates`
  ADD PRIMARY KEY (`bc_id`);

--
-- Chỉ mục cho bảng `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`mn_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`od_id`),
  ADD UNIQUE KEY `orders_od_code_unique` (`od_code`);

--
-- Chỉ mục cho bảng `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_pro_alias_unique` (`pro_alias`),
  ADD UNIQUE KEY `products_pro_code_unique` (`pro_code`),
  ADD KEY `products_pro_type_foreign` (`pro_type`),
  ADD KEY `products_pro_vendor_foreign` (`pro_vendor`),
  ADD KEY `products_pro_cate_foreign` (`pro_cate`),
  ADD KEY `pro_name` (`pro_name`),
  ADD KEY `pro_code` (`pro_code`);

--
-- Chỉ mục cho bảng `product_categoies`
--
ALTER TABLE `product_categoies`
  ADD PRIMARY KEY (`pc_id`);

--
-- Chỉ mục cho bảng `product_type`
--
ALTER TABLE `product_type`
  ADD PRIMARY KEY (`pt_id`);

--
-- Chỉ mục cho bảng `product_vendor`
--
ALTER TABLE `product_vendor`
  ADD PRIMARY KEY (`pv_id`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `address_setting`
--
ALTER TABLE `address_setting`
  MODIFY `id_as` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `blogs`
--
ALTER TABLE `blogs`
  MODIFY `bl_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `blog_cates`
--
ALTER TABLE `blog_cates`
  MODIFY `bc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `menus`
--
ALTER TABLE `menus`
  MODIFY `mn_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `orders`
--
ALTER TABLE `orders`
  MODIFY `od_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `product_categoies`
--
ALTER TABLE `product_categoies`
  MODIFY `pc_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `product_type`
--
ALTER TABLE `product_type`
  MODIFY `pt_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `product_vendor`
--
ALTER TABLE `product_vendor`
  MODIFY `pv_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_bl_cate_foreign` FOREIGN KEY (`bl_cate`) REFERENCES `blog_cates` (`bc_id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_pro_cate_foreign` FOREIGN KEY (`pro_cate`) REFERENCES `product_categoies` (`pc_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_pro_type_foreign` FOREIGN KEY (`pro_type`) REFERENCES `product_type` (`pt_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_pro_vendor_foreign` FOREIGN KEY (`pro_vendor`) REFERENCES `product_vendor` (`pv_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
