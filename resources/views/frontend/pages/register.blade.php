@extends('frontend.layouts.master')
@section('content')
	<div class="lzd-playground">
   <div class="lzd-playground-main">
      <div class="lzd-playground-nav"></div>
      <div id="container" class="container">
         <div class="login register">
            <div class="login-title">
               <h3>Tạo tài khoản Lazada</h3>
               <div class="login-other"><span>Bạn đã là thành viên? <a href="/user/login">Đăng nhập</a> tại đây</span></div>
            </div>
            <div>
               <form>
                  <div class="mod-login">
                     <div class="mod-login-col1">
                        <div class="mod-input mod-login-input-phone mod-input-phone">
                           <label>Số điện thoại</label>
                           <input type="number" placeholder="Nhập số điện thoại của bạn" data-meta="Field" value="" inputmode="numeric" pattern="[0-9]*">
                           <b></b><span></span>
                        </div>
                        <div class="mod-input mod-input-password mod-login-input-password mod-input-password">
                           <label>Mật khẩu</label>
                           <input type="password" placeholder="Tối thiểu 6 kí tự bao gồm cả chữ và số" data-meta="Field" value="">
                           <b></b><span></span>
                           <div class="mod-input-password-icon"></div>
                        </div>
                        <div class="mod-input mod-input-password mod-login-input-re-password mod-input-re-password">
                           <label>Nhập lại mật khẩu</label>
                           <input type="password" placeholder="Vui lòng nhập lại mật khẩu" data-meta="Field" value="">
                           <b></b><span></span>
                           <div class="mod-input-password-icon"></div>
                        </div>
                        <div class="mod-login-row clearfix">
                           <div class="mod-login-birthday clearfix">
                              <div class="mod-login-birthday-hd">Ngày sinh</div>
                              <input type="text" name="day" value="">
                           </div>
                           <div class="mod-login-birthday clearfix">
                              <div class="mod-login-birthday-hd">tháng</div>
                              <input type="text">
                           </div>
                           <div class="mod-login-birthday clearfix">
                              <div class="mod-login-birthday-hd">năm</div>
                              <input type="text">
                           </div>
                           <div class="mod-login-gender">
                              <div class="mod-gender">
                                 <div class="mod-gender-hd">Giới tính</div>
                                 <span data-meta="Field" id="gender" class="next-select large mod-fusion-select mod-gender-gender" tabindex="0"><input type="hidden" name="select-faker" value=""><span class="next-select-inner"><span class="next-select-placeholder" data-spm-anchor-id="a2o4n.login_signup.0.i5.2709705bWsLH0R">Chọn</span></span><i class="next-icon next-icon-arrow-down next-icon-medium next-select-arrow"></i></span>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="mod-login-col2">
                        <div class="mod-input mod-login-input-name mod-input-name"><label>Tên</label><input type="text" placeholder="Họ Tên" data-meta="Field" value=""><b></b><span></span></div>
                        <div class="mod-input mod-login-input-email mod-input-email"><label>Địa chỉ email</label><input type="text" placeholder="Vui lòng nhập email của bạn" data-meta="Field" value=""><b></b><span></span></div>
                        <div class="mod-login-receive">
                           <label class="next-checkbox checked "><span class="next-checkbox-inner"><i class="next-icon next-icon-select next-icon-xs"></i></span><input type="checkbox" data-meta="Field" id="enableNewsletter" aria-checked="true" value="on"></label>
                           <p>Tôi muốn nhận các thông tin khuyến mãi từ Lazada.</p>
                        </div>
                        <div class="mod-login-btn"><button type="submit" class="next-btn next-btn-primary next-btn-large">ĐĂNG KÍ</button></div>
                        <div class="mod-login-policy"><span>Tôi đồng ý với <a href="https://www.lazada.vn/privacy-policy/?hybrid=1" target="_blank" rel="noopener noreferrer">Chính sách bảo mật Lazada</a></span></div>
                        <div class="mod-login-change-register">
                           <p class="mod-change-register-title">Hoặc Đăng kí với</p>
                           <div class="mod-change-register-btn"><button type="button" class="next-btn next-btn-secondary next-btn-large">Đăng ký bằng Email</button></div>
                        </div>
                        <div class="mod-login-third">
                           <div class="mod-third-party-login mod-third-party-login-in-two-lines">
                              <div class="mod-third-party-login-line"><span></span></div>
                              <div class="mod-third-party-login-bd"><button class="mod-button mod-button mod-third-party-login-btn mod-third-party-login-fb">Facebook</button><button class="mod-button mod-button mod-third-party-login-btn mod-third-party-login-google">Google</button></div>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection