@extends('frontend.layouts.master')
@section('content')
<br><br>
	

<main class="main-content" role="main">
   <section id="contactUs-map">
      <div class="wrapper">
         <div class="inner">
            {!! $setting->iframe_map !!}
         </div>
      </div>
   </section>
   <section id="contactUs-content">
      <div class="wrapper">
         <div class="inner">
            <div class="grid">
               <div class="grid__item large--six-twelfths">
                  <div class="contactUs-desc">
                     <p class="title">
                        {{ $setting->company }}
                     </p>
                     @foreach($address as $item)
                     <p>
                        Địa Chỉ: {{ $item->name_as }},{{ $item->districtId_as }},{{ $item->city_as }}
                     </p>
                     @endforeach
                     <p>
                        Tổng đài hỗ trợ : <a href="tel:{{ $setting->phone1 }}">{{ $setting->phone1 }}</a>
                     </p>
                     <p>
                        Email : <a href="mailto:{{ $setting->email }}">{{ $setting->email }}</a>
                     </p>
                     <p>
                        Website: <a href="{{ url('/') }}" target="_blank">{{ $setting->webname }}</a>
                     </p>
                  </div>
               </div>
               <div class="grid__item large--six-twelfths">
                  <h1>
                     Liên hệ với chúng tôi
                  </h1>
                  <div class="form-vertical contact-us-form">
                     <form accept-charset="UTF-8" action="/contact" class="contact-form" method="post">
                        <input name="form_type" value="contact" type="hidden">
                        <input name="utf8" value="✓" type="hidden">
                        <label for="ContactFormName" class="hidden-label">Họ tên của bạn</label>
                        <input id="ContactFormName" class="input-full" name="contact[name]" placeholder="Họ tên của bạn" autocapitalize="words" value="" type="text">
                        <label for="ContactFormEmail" class="hidden-label">Địa chỉ email của bạn</label>
                        <input id="ContactFormEmail" class="input-full" name="contact[email]" placeholder="Địa chỉ email của bạn" autocorrect="off" autocapitalize="off" value="" type="email">
                        <label for="ContactFormPhone" class="hidden-label">Số điện thoại của bạn</label>
                        <input id="ContactFormPhone" class="input-full" name="contact[phone]" placeholder="Số điện thoại của bạn" pattern="[0-9\-]*" value="" type="tel">
                        <label for="ContactFormMessage" class="hidden-label">Nội dung</label>
                        <textarea rows="10" id="ContactFormMessage" class="input-full" name="contact[body]" placeholder="Nội dung"></textarea>
                        <button type="submit" class="btn right">Gửi</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</main>


@endsection