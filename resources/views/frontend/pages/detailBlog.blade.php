@extends('frontend.layouts.master')
@section('content')
<br><br>
<main class="main-content" role="main">
   <div class="wrapper">
      <div class="grid--rev">
         <div class="grid__item large--three-quarters">
            <header class="section-header">
               <h1 class="section-header__title section-header__left">{{ $detailblog->bl_title }}</h1>
            </header>
            <div class="grid-uniform">
               <!-- begin product list output -->
               
               <div class="grid__item">
                  {!! $detailblog->bl_content !!}
               </div>
               
               <!-- //product list output -->
            </div>
         </div>
         @include('frontend.layouts.spnoibat');
      </div>
   </div>
</main>
@endsection