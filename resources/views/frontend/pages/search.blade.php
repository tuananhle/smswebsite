@extends('frontend.layouts.master')
@section('content')
<br><br>
<main class="main-content" role="main">
   <div class="wrapper">
      <div class="grid--rev">
         <div class="grid__item large--three-quarters">
            <header class="section-header">
               <h1 class="section-header__title section-header__left"></h1>
            </header>
            <div class="grid-uniform" id="ajaxtable">
               <h3>Tìm thấy {{ count($search) }} sản phẩm cho từ khóa '{{ $keyword }}'</h3>
               <!-- begin product list output -->
               @foreach($search as $item)
               <div class="grid__item">
                  <div class="grid large--display-table">
                     <div class="grid__item large--one-fifth large--display-table-cell medium--one-third">
                        <a href="{{ url('san-pham/'.$item->pro_alias) }}.html">
                           @php
                              $arrImg = json_decode($item->pro_images);
                           @endphp
                        <img src="{{ url('uploads/imageProduct/'.$item->pro_images) }}" alt="" class="grid__image">
                        </a>
                     </div>
                     <div class="grid__item large--four-fifths large--display-table-cell medium--two-thirds">
                        <div class="grid">
                           <div class="grid__item large--three-quarters medium--two-thirds">
                              <p class="h6"><a href="{{ url('san-pham/'.$item->pro_alias) }}.html">{{ $item->pro_name }}</a></p>
                              <div class="rte">
                                 <p>{{ $item->pro_summary }}</p>
                              </div>
                           </div>
                           
                        </div>
                     </div>
                  </div>
               </div>
               @endforeach
               <!-- //product list output -->
            </div>
         </div>
        @include('frontend.layouts.spnoibat');
      </div>
   </div>
</main>
@endsection