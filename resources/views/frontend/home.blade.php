@extends('frontend.layouts.master')
@section('content')
	<section id="home-about-us">
            <div class="wrapper">
                <div class="inner">
                    <div>
                        <h2 class="title__ads01 text-center">
                            Tại sao <span> nên sử dụng </span> dịch vụ khôi phục & sao lưu dữ liệu ?
                        </h2>
                        <p class="text-title__ads01">Dữ liệu là tài nguyên quý giá của bất kỳ doanh nghiệp hay tổ chức nào. Tuy nhiên tài nguyên quý giá đó đang đối diện với các nguy cơ và thách thức. Chúng tôi thấu hiểu được sự quan trọng của dữ liệu. Do đó, chúng tôi đưa ra giải pháp sao lưu chuyên dụng với những thiết bị và công nghệ luôn cập nhật mới nhất, phù hợp với từng hạ tầng công nghệ thông tin của quý khách hàng.</p>
                    </div>
                    <div class="grid">
                        <div class="group-grid__item ads01">
                            <div class="grid__item large--three-twelfths">
                                <div class="wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                    <div class="unit_sms_trade">
                                        <span class="icon_trade_1 spn_icon_dv"></span>
                                        <h3>Bảo mật thông tin khách hàng</h3>
                                        <p>Bảo mật thông tin khách hàng</p>
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item large--three-twelfths">
                                <div class="wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                    <div class="unit_sms_trade">
                                        <span class="icon_trade_2 spn_icon_dv"></span>
                                        <h3>Thực hiện trong thời gian nhanh nhất</h3>
                                        <p>Thực hiện trong thời gian nhanh nhất</p>
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item large--three-twelfths">
                                <div class="wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                    <div class="unit_sms_trade">
                                        <span class="icon_trade_3 spn_icon_dv"></span>
                                        <h3>Tư vấn sử dụng các dịch vụ đi kèm</h3>
                                        <p>Tư vấn sử dụng các dịch vụ đi kèm</p>
                                    </div>
                                </div>
                            </div>
                            <div class="grid__item large--three-twelfths">
                                <div class="wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                    <div class="unit_sms_trade">
                                        <span class="icon_trade_4 spn_icon_dv"></span>
                                        <h3>Chi phí dịch vụ thấp</h3>
                                        <p> Chi phí dịch vụ thấp</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="home-statistics">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item large--two-tenths">
                            <div class="numStaff">
                                <p class="counter" data-count="150">150</p>
                                <p>
                                    Nhân viên
                                </p>
                            </div>
                        </div>
                        <div class="grid__item large--two-tenths">
                            <div class="numSupport">
                                <p>
                                    <span class="counter" data-count="24">24</span>
                                    /
                                    <span class="counter" data-count="7">7</span>
                                </p>
                                <p>
                                    Hỗ trợ
                                </p>
                            </div>
                        </div>
                        <div class="grid__item large--two-tenths">
                            <div class="numClient">
                                <p>
                                    <span class="counter" data-count="1000">1000</span>+
                                </p>
                                <p>
                                    Khách hàng
                                </p>
                            </div>
                        </div>
                        <div class="grid__item large--two-tenths">
                            <div class="numPartner">
                                <p class="counter" data-count="150">150</p>
                                <p>
                                    Đối tác
                                </p>
                            </div>
                        </div>
                        <div class="grid__item large--two-tenths">
                            <div class="numPrize">
                                <p>
                                    <span class="counter" data-count="50">50</span>+
                                </p>
                                <p>
                                    Giải thưởng
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="home-services">
            <div class="wrapper">
                <div class="inner">
                    <h2 class="our-services">
                        <span>Dịch vụ</span> của chúng tôi
                    </h2>
                    <div class="grid grid-our-services">
                        <div class="grid__item large--three-twelfths">
                            <div class="item wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="unit_sms_trade">
                                    <div class="item-img">
                                        <img src="{{ url('frontend/img/GPS.png') }}">
                                    </div>
                                    <h3>Thiết bị định vị GPS</h3>
                                </div>
                            </div>
                        </div>
                        <div class="grid__item large--three-twelfths">
                            <div class="item wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="unit_sms_trade">
                                    <div class="item-img">
                                        <img src="{{ url('frontend/img/iCloud-Icon.png') }}">
                                    </div>
                                    <h3>Phần mềm sao lưu IOS</h3>
                                </div>
                            </div>
                        </div>
                        <div class="grid__item large--three-twelfths">
                            <div class="item wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="unit_sms_trade">
                                    <div class="item-img">
                                        <img src="{{ url('frontend/img/datab-1.png') }}">
                                    </div>
                                    <h3>Sao lưu dữ liệu</h3>
                                </div>
                            </div>
                        </div>
                        <div class="grid__item large--three-twelfths">
                            <div class="item wow fadeInLeft animated" style="visibility: visible; animation-name: fadeInLeft;">
                                <div class="unit_sms_trade">
                                    <div class="item-img">
                                        <img src="{{ url('frontend/img/application-backup.png') }}">
                                    </div>
                                    <h3>Khôi phục dữ liệu</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
@endsection