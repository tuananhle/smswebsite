<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>{{ $setting->title }}</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ url('uploads/image/'.$setting->favicon) }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="DC.title" content="Dịch vụ khôi phục và sao lưu dữ liệu" />
    <meta name="DC.identifier" content="etailingvietnam.com" />
    <meta name="DC.description" content="Dịch vụ khôi phục và sao lưu dữ liệu" />
    <meta name="DC.subject" content="Dịch vụ khôi phục và sao lưu dữ liệu" />
    <meta name="DC.language" scheme="ISO639-1" content="en" />
    <meta name="geo.region" content="VN" />
    {{-- <link href='{{ asset('frontend/css/bootstrap.min.css') }}' rel='stylesheet' type='text/css'  media='all'  /> --}}
    <link href='{{ asset('frontend/css/timber.scss.css') }}' rel='stylesheet' type='text/css'  media='all'  />
    <link href='{{ asset('frontend/css/css_main.css.css') }}' rel='stylesheet' type='text/css'  media='all'  />
    <link href='{{ asset('frontend/css/boxi-style.scss.css') }}' rel='stylesheet' type='text/css'  media='all'  />
    <link rel="stylesheet" href="{{ asset('frontend/css/amazingslider-1.css') }}">
    <!-- <link href='https://theme.hstatic.net/1000180294/1000232400/14/animate.css?v=1595' rel='stylesheet' type='text/css'  media='all'  /> -->
    <!-- <link href='https://hstatic.net/0/0/global/design/css/owl.carousel.css' rel='stylesheet' type='text/css'  media='all'  /> -->

    <link rel="stylesheet" type="text/css" href="{{ asset('frontend/css/style.css') }}">
</head>
<body>
    <div id="NavDrawer" class="drawer drawer--left" tabindex="-1">
        <div class="drawer__header">
            <div class="drawer__title h3">
                <a href="/" class="web-logo logo-nav-mb01">
                    <img src="{{ url('uploads/image/'.$setting->logo) }}" alt="Sao lưu và khôi phục dữ liệu">
                </a>
            </div>
            <div class="drawer__close js-drawer-close">
                <button type="button" class="icon-fallback-text">
                    <span class="fa fa-close" aria-hidden="true"></span>
                </button>
            </div>
        </div>
        <div class="mg-bottom-15">
            <form action="{{ url('search') }}" method="get" class="ta input-group search-bar" role="search">
                <input type="text" name="search_text"  placeholder="Tìm kiếm..." class="input-group-field" aria-label="Tìm kiếm...">
                <span class="input-group-btn">
                    <button type="submit" class="btn icon-fallback-text">
                        <span class="fa fa-search" aria-hidden="true"></span>
                    </button>
                </span>
            </form>
        </div>
        <ul class="mobile-nav">
            <li class="mobile-nav__item mobile-nav__item--active">
                <a href="/" class="mobile-nav__link">Trang chủ</a>
            </li>
            @foreach($cate as $item)
            <li class="mobile-nav__item" aria-haspopup="true">
                <div class="mobile-nav__has-sublist">
                    <a href="{{ url($item->pc_alias) }}.html" class="mobile-nav__link">{{ $item->pc_name }}</a>
                    <div class="mobile-nav__toggle">
                        <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                            <span class="fa fa-plus" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                            <span class="fa fa-minus" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
                <ul class="mobile-nav__sublist">
                    @php
                        $type = DB::table('product_type')->where('pt_cate',$item->pc_id)->get();
                    @endphp
                    @foreach($type as $val)
                    <li class="mobile-nav__item" aria-haspopup="true">
                        <div class="mobile-nav__has-sublist">
                            <a href="{{ url($val->pt_alias) }}.html" class="mobile-nav__link">{{ $val->pt_name }} </a>
                            <div class="mobile-nav__toggle">
                                <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                                    <span class="fa fa-plus" aria-hidden="true"></span>
                                </button>
                                <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                                    <span class="fa fa-minus" aria-hidden="true"></span>
                                </button>
                            </div>
                        </div>
                        <ul class="mobile-nav__sublist">
                            @php
                                $pro = DB::table('products')->where('pro_type',$val->pt_id)->get();
                            @endphp
                            @foreach($pro as $pr)
                            <li class="mobile-nav__item ">
                                <a href="{{ url('san-pham/'.$pr->pro_alias) }}.html" class="mobile-nav__link"> {{ $pr->pro_name }} </a>
                            </li>
                           @endforeach
                        </ul>
                    </li>
                    @endforeach
                </ul>
            </li>
            @endforeach
            @foreach($cate_blog as $item)
            <li class="mobile-nav__item" aria-haspopup="true">
                <div class="mobile-nav__has-sublist">
                    <a href="{{ url('/dich-vu') }}.html" class="mobile-nav__link"> {{ $item->bc_name }} </a>
                    <div class="mobile-nav__toggle">
                        <button type="button" class="icon-fallback-text mobile-nav__toggle-open">
                            <span class="fa fa-plus" aria-hidden="true"></span>
                        </button>
                        <button type="button" class="icon-fallback-text mobile-nav__toggle-close">
                            <span class="fa fa-minus" aria-hidden="true"></span>
                        </button>
                    </div>
                </div>
                <ul class="mobile-nav__sublist">
                    @php
                        $blog = DB::table('blogs')->where('bl_cate',$item->bc_id)->get();
                    @endphp
                    @foreach($blog as $bl)
                    <li class="mobile-nav__item" aria-haspopup="true">
                        <a href="{{ url('dich-vu/'.$bl->bl_id.'-'.$bl->bl_alias) }}.html" class="mobile-nav__link"> {{ $bl->bl_title }} </a>
                    </li>
                    @endforeach
                </ul>
            </li>
            @endforeach
            @foreach($menu as $item)
            <li class="mobile-nav__item">
                <a href="{{ url($item->mn_alias) }}.html" class="mobile-nav__link">{{ $item->mn_name  }} </a>
            </li>
            @endforeach
        </ul>
    </div>
    @include('frontend.layouts.header')
    <div id="amazingslider-wrapper-1">
        <div id="amazingslider-1">
            <ul class="amazingslider-slides" style="display:none;">
                @foreach($banner as $item)
                <li>
                    <img src="{{ url('uploads/imageBanner/'.$item->bn_images) }}" alt="slider1"  title="slider1" />
                </li>
                @endforeach
            </ul>
        </div>
    </div>
    <main>
        @yield('content')
        @include('frontend.layouts.footer')
    </main>
    

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="{{ asset('frontend/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('frontend/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('frontend/js/timber.js') }}"></script>
    <script src="{{ asset('frontend/js/fastclick.min.js') }}"></script>
    <script src="{{ asset('frontend/js/amazingslider.js') }}"></script>
    <script src="{{ asset('frontend/js/initslider-1.js') }}"></script>
    <script src="{{ asset('frontend/js/myjs.js') }}"></script>
</body>
</html>