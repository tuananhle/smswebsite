
<div class="grid__item large--one-quarter">
            <div class="collection-sidebar-block">
               <div class="col-title">
                  <h2 class="h3">Sản phẩm nổi bật</h2>
               </div>
               <div class="panel">
                  <ul class="no-bullets">
                     @foreach($pro as $item)
                     <li>
                        <a href="{{ url('san-pham/'.$item->pro_alias) }}.html" class="grid seenPro">
                           <div class="grid__item large--four-twelfths medium--four-twelfths small--six-twelfths">
                              <div class="seenPro-img">
                                 <img src="{{ url('uploads/imageProduct/'.$item->pro_images) }}" alt="{{ $item->pro_name }}">
                              </div>
                           </div>
                           <div class="grid__item large--eight-twelfths medium--eight-twelfths small--six-twelfths">
                              <div class="seenPro-info">
                                 <p class="seenPro-title">
                                    {{ $item->pro_name }}
                                 </p>
                                 {{-- <p class="seenPro-price">
                                    <span class="current-price">5,000,000₫</span>
                                    <span class="old-price">
                                    <s>6,000,000₫</s>
                                    </span>
                                 </p> --}}
                              </div>
                           </div>
                        </a>
                     </li>
                     @endforeach
                  </ul>
               </div>
            </div>
         </div>