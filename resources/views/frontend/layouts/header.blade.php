<header id="header" class="">
        <div class="header-top">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item large--six-twelfths">
                            <div class="header-actions">
                                <a href="tel:0983 899 111"><i class="fa fa-phone-square" aria-hidden="true"></i> {{ $setting->phone1 }}</a>
                                <a href="mailto:support@mixone.vn"><i class="fa fa-envelope" aria-hidden="true"></i> {{ $setting->email }}
                            </div>
                        </div>
                        <div class="grid__item large--six-twelfths medium-down--hide">
                            <div class="header-contact text-right">
                                <a href="{{$setting->facebook}}" target="_blank" class="social-network">
                                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                                </a>
                                <a href="https://twitter.com/" target="_blank" class="social-network">
                                    <i class="fa fa-twitter-square" aria-hidden="true"></i>
                                </a>
                                <a href="https://plus.google.com/" target="_blank" class="social-network">
                                    <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                                </a>
                                <a href="https://www.youtube.com/" target="_blank" class="social-network">
                                    <i class="fa fa-youtube-square" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <nav id="navbar" class="nav-bar text-center medium-down--hide" role="navigation">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item large--two-twelfths">
                            <div class="site-logo">
                                <h1>
                                    <a href="/" class="web-logo">
                                        <img src="{{ url('uploads/image/'.$setting->logo) }}" alt="Sao lưu và khôi phục dữ liệu">
                                    </a>
                                </h1>
                            </div>
                        </div>
                        <div class="grid__item large--ten-twelfths">
                            <ul class="site-nav">
                                <li class="active ">
                                    <a href="{{ url('/') }}">Trang chủ </a>
                                </li>
                                @foreach($cate as $item)
                                <li class=" dropdown-menu">
                                    <a href="{{ url($item->pc_alias) }}.html"> {{ $item->pc_name }} <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul>
                                        @php
                                            $type = DB::table('product_type')->where('pt_cate',$item->pc_id)->get();
                                        @endphp
                                        @foreach($type as $val)
                                        <li class="dropdown-menu  dropdown-menu--lv2">
                                            <a href="{{ url($val->pt_alias) }}.html">{{ $val->pt_name }}<i class="fa fa-caret-right" aria-hidden="true"></i></a>
                                            <ul >
                                                @php
                                                    $pro = DB::table('products')->where('pro_type',$val->pt_id)->get();
                                                @endphp
                                                @foreach($pro as $pr)
                                                <li>
                                                    <a href="{{ url('san-pham/'.$pr->pro_alias) }}.html" > {{ $pr->pro_name }} </a>
                                                </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                                @foreach($cate_blog as $item)
                                <li class=" dropdown-menu">
                                    <a href="{{ url('/dich-vu') }}.html"> {{ $item->bc_name }} <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    <ul>
                                        @php
                                            $blog = DB::table('blogs')->where('bl_cate',$item->bc_id)->get();
                                        @endphp
                                        @foreach($blog as $bl)
                                        <li>
                                            <a href="{{ url('dich-vu/'.$bl->bl_id.'-'.$bl->bl_alias) }}.html"> {{ $bl->bl_title }} </a>
                                        </li>
                                        @endforeach
                                        
                                    </ul>
                                </li>
                                @endforeach
                                @foreach($menu as $item)
                                <li class="active ">
                                    <a href="{{ url($item->mn_alias) }}.html">{{ $item->mn_name  }}</a>
                                </li>
                                @endforeach
                                <li class="active ">
                                    <a href="{{ url('lien-he') }}.html">Liên hệ</a>
                                </li>
                                
                               
                                <a class="search-hover" href="javascript:void(0)">
                                    <span class="fa fa-search" aria-hidden="true"></span>
                                    <div class="search-bar-header">
                                        <form action="{{ url('search') }}" method="get" class="input-group search-bar" role="search">
                                            
                                            <input type="search" name="search_text"  placeholder="Tìm kiếm..." class="input-group-field" aria-label="Tìm kiếm...">
                                            <span class="input-group-btn">
                                                <button type="submit" class="btn icon-fallback-text">
                                                    <span class="fa fa-search" aria-hidden="true"></span>
                                                </button>
                                            </span>
                                        </form>
                                    </div>
                                </a>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <nav id="mobile-navbar" class="large--hide" role="navigation">
            <div class="wrapper">
                <div class="inner">
                    <div class="grid">
                        <div class="grid__item medium--one-whole small--one-whole">
                            <div class="mobile-logo">
                                <a href="/" class="mobile-logo">
                                    <img src="{{ url('frontend/img/logo.png') }}" alt="Sao lưu và khôi phục dữ liệu">
                                </a>
                                <button type="button" class="mobile-bars icon-fallback-text site-nav__link js-drawer-open-left" aria-controls="NavDrawer" aria-expanded="true">
                                    <i class="fa fa-bars" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>