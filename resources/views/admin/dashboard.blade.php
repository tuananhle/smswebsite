@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div id="dashboard" class="container-fluid-md" style="height: 100vh;background:#fff;padding-top:50px;text-align:center">
                    <h1> Chào mừng bạn đến với hệ thống quản trị website </h1>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection