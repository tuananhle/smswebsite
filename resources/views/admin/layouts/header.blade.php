<header class="ui-app-frame__header">
    <div class="ui-top-bar">
        <div class="ui-top-bar__branding">
            <a href="/admin" style="display:block;padding-top: 12px;">
                <img src="/img/logo-2.png" alt="mixone" title="mixone">
            </a>
            <div class="icon-view-website">
                <a href="/" target="_blank" rel="noreferrer noopener" title="Xem website của bạn">
                    <svg class="next-icon next-icon--size-20 action-bar__link-icon next-icon--no-nudge">
                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-view3"></use>
                    </svg>
                </a>
            </div>
        </div>
        <div class="ui-top-bar__list">
            <div class="ui-top-bar__item ui-top-bar__item--desktop-hidden ui-top-bar-open-sidebar" style="padding-top: 15px">
                <div class="ui-app-frame__aside-opener">
                    <button name="button" type="button" class="btn-open-nvmb top-bar-button">
                        <svg class="next-icon next-icon--size-20 next-icon--no-nudge" role="img" aria-labelledby="menu-c4072f7f35f301f1ee17b27ad93ae0e9-title" style="width:30px;height:25px">
                            <title id="menu-c4072f7f35f301f1ee17b27ad93ae0e9-title">Open navigation</title> 
                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#menu"></use> 
                        </svg>
                    </button>
                </div>
            </div>
            <div class="ui-top-bar__item ui-top-bar__item--fill">
                <!-- Thông báo ( chữ chạy )-->
            </div>
            <div class="ui-top-bar__item ui-top-bar__item--separated ui-top-bar__item--bleed ui-top-bar__item--mobile-hidden ui-top-bar-acc" style="padding-right:38px;padding-top:10px">
                <div class="ui-popover__container ui-popover__container--full-width-container">
                    <div class="top-bar-profile">
                        <div class="top-bar-profile__avatar">
                            <span class="user-avatar">
                                <img class="gravatar gravatar--size-thumb" src="https://innmind.com/assets/placeholders/no_avatar-3d6725770296b6a1cce653a203d8f85dcc5298945b71fa7360e3d9aa4a3fc054.svg">
                            </span>
                        </div>
                        <div class="top-bar-profile__summary">
                            <p class="top-bar-profile__title">{{-- {{ Auth::guard('admin')->user()->name }} --}}</p>
                        </div>
                        <div>
                            <a href="{{ url('admin/logout') }}" class="ui-action-list-action" data-no-turbolink="true" data-method="post">
                                <span class="ui-action-list-action__text">
                                    <div class="ui-stack ui-stack--wrap ui-stack--alignment-center ui-stack--spacing-tight">
                                        <div class="ui-stack-item">
                                            <svg role="img" class="next-icon next-icon--size-16" aria-labelledby="minor-log-out-title">
                                                <title id="minor-log-out-title">Log out icon</title>
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#minor-log-out"></use>
                                            </svg>
                                        </div>
                                        <div class="ui-stack-item ui-stack-item--fill">
                                            <span>Đăng xuất</span>
                                        </div>
                                    </div>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>