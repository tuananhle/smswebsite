@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Thêm mới sản phẩm')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
   <meta name="csrf-token" content="{{ csrf_token() }}">
   <div class="wrapper" id="wrapper">
      <div id="body" class="page-content clearfix" data-tg-refresh="body">
         <div id="content">
            <style type="text/css">
               .note {
               color: #798c9c;
               font-weight: normal;
               text-transform: none;
               }
               @media (max-width:768px) {
               .bulk-actions-mobile {
               left: 22px !important;
               }
               }
               @media (min-width:419px) {
               .enter-text-select {
               display: none;
               }
               }
               @media (max-width:418px) and (min-width:365px) {
               .enter-text-select {
               display: initial;
               }
               .selection-count-mobile {
               font-size: 12px;
               padding: 1px 12px 1px 30px !important;
               line-height: 14px;
               }
               .btn-edit-product-mobile {
               font-size: 12px !important;
               padding: 7px 20px 7px 20px;
               padding-left: 20px !important;
               }
               .btn-dropdown-mobile {
               font-size: 12px !important;
               padding: 7px 10px 7px 10px;
               padding-left: 10px !important;
               }
               .checkbox-bulkactions-mobile {
               top: 3px;
               }
               .dropdown-menu-product-mobile {
               left: -90px;
               }
               .dropdown-menu-product-mobile.arrow-style:not(.pull-right):before, .dropdown-menu-product-mobile.arrow-style:not(.pull-right):after {
               left: 150px;
               }
               }
               @media (max-width:364px) {
               .enter-text-select {
               display: initial;
               }
               .selection-count-mobile {
               font-size: 12px;
               padding: 1px 8px 1px 25px !important;
               line-height: 14px;
               }
               .btn-edit-product-mobile {
               font-size: 12px !important;
               padding: 7px 15px 7px 15px;
               padding-left: 15px !important;
               }
               .btn-dropdown-mobile {
               font-size: 12px !important;
               padding: 7px 5px 7px 5px;
               padding-left: 5px !important;
               }
               .checkbox-bulkactions-mobile {
               top: 3px;
               }
               .dropdown-menu-product-mobile {
               left: -90px;
               }
               .dropdown-menu-product-mobile.arrow-style:not(.pull-right):before, .dropdown-menu-product-mobile.arrow-style:not(.pull-right):after {
               left: 150px;
               }
               }
               ul, li {
               list-style-type: none;
               }
               .single-query {
               border: none;
               padding: 4px 4px 4px 11px;
               height: 28px;
               border-bottom: 1px solid #e6e6e6;
               border-color: #c0c0c0 #c0c0c0 #e6e6e6;
               border-radius: 3px 3px 0 0;
               width: 100%;
               }
               .single-suggest-result {
               width: 320px;
               }
               .single-suggest-result li.single-suggest-select {
               padding: 3px;
               }
               .modal .modal-dialog.change-notice-modal {
               padding:3.5px;
               background:transparent;
               box-shadow: unset;
               }
            </style>
            <form id="frmAction" method="post">
               <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCcFgU7SyhGzT/hXHWP89/vG5vustGttUBrpJMgp0159sEKjf2wc+TdZThRoyFyCqOA==">
               <input type="hidden" name="returnUrl" value="/admin/products">
               <input type="hidden" name="id" value="">
            </form>
            <div class="modal fade" id="bizweb-modal" data-width="" tabindex="-1" role="dialog"></div>
            <div class="modal" data-tg-refresh="modal" id="modal_container" style="display: none;" aria-hidden="true" aria-labelledby="ModalTitle" tabindex="-1"></div>
            <div class="modal-bg" data-tg-refresh="modal" id="modal_backdrop"></div>
            <form id="frmRemoveSavedSearch" method="post">
               <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCcFgU7SyhGzT/hXHWP89/vG5vustGttUBrpJMgp0159sEKjf2wc+TdZThRoyFyCqOA==">
               <input type="hidden" name="returnUrl" value="/admin/products">
               <input type="hidden" name="savedSearchId" value="">
            </form>
            <form id="frmFilter" method="get">
               <input type="hidden" name="Query" value="">
            </form>
            <form id="frmBulkActions" method="get"></form>
            <div id="pages-index" class="page default has-contextual-help discounts-page" define="{productsImportAndExport:new Bizweb.ProductsImportAndExport(this, {'products_match_current_search':20, show_change_notice: false})}" context="productsImportAndExport">
               <header class="ui-title-bar-container   ui-title-bar-container--full-width">
                  <div class="ui-title-bar ">
                     <div class="ui-title-bar__main-group">
                        <div class="ui-title-bar__heading-group">
                           <span class="ui-title-bar__icon">
                              <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                 <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-discounts"></use>
                              </svg>
                           </span>
                           <h1 class="ui-title-bar__title">Tiêu đề</h1>
                        </div>
                        <div define="{titleBarActions: new Bizweb.TitleBarActions(this)}" class="action-bar">
                           <div class="action-bar__item action-bar__item--link-container">
                              {{-- <div class="action-bar__top-links">
                                 <button class="ui-button ui-button--transparent action-bar__link" bind-event-click="showExportProducts()" data-popover-index="1" type="button" name="button">
                                    <svg class="next-icon next-icon--size-20 action-bar__link-icon next-icon--no-nudge">
                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-export-minor"></use>
                                    </svg>
                                    Xuất file
                                 </button>
                                 <button class="ui-button ui-button--transparent action-bar__link" bind-event-click="openFileImportProducts()" data-popover-index="2" type="button" name="button">
                                    <svg class="next-icon next-icon--size-20 action-bar__link-icon next-icon--no-nudge">
                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-import-minor"></use>
                                    </svg>
                                    Nhập danh sách
                                 </button>
                              </div> --}}
                              {{-- <div class="action-bar__more hide">
                                 <div class="ui-popover__container">
                                    <button class="ui-button ui-button--transparent" type="button" name="button">
                                       <span data-singular-label="Thao tác" data-multiple-label="Chọn thao tác" class="action-bar__more-label">Chọn thao tác</span>
                                       <svg class="next-icon next-icon--size-20">
                                          <use xlink:href="#next-disclosure"></use>
                                       </svg>
                                    </button>
                                    <div class="ui-popover ui-popover--align-edge">
                                       <div class="ui-popover__tooltip"></div>
                                       <div class="ui-popover__content-wrapper">
                                          <div class="ui-popover__content">
                                             <div class="ui-popover__pane">
                                                <ul class="action-bar__popover-wrapper">
                                                   <li>
                                                      <ul class="ui-action-list">
                                                         <li class="ui-action-list__item">
                                                            <button class="ui-action-list-action action-bar__popover-hidden-item" data-popover-index="1" bind-event-click="showExportProducts()" type="button" name="button">
                                                               <span class="ui-action-list-action__text">
                                                                  <span class="action-bar__popover-icon-wrapper">
                                                                     <svg class="next-icon next-icon--color-blue next-icon--size-16 action-bar__popover-icon">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-export-minor"></use>
                                                                     </svg>
                                                                  </span>
                                                                  Xuất file
                                                               </span>
                                                            </button>
                                                         </li>
                                                         <li class="ui-action-list__item">
                                                            <button class="ui-action-list-action action-bar__popover-hidden-item" data-popover-index="2" bind-event-click="openFileImportProducts()" type="button" name="button">
                                                               <span class="ui-action-list-action__text">
                                                                  <span class="action-bar__popover-icon-wrapper">
                                                                     <svg class="next-icon next-icon--color-blue next-icon--size-16 action-bar__popover-icon">
                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-import-minor"></use>
                                                                     </svg>
                                                                  </span>
                                                                  Nhập danh sách
                                                               </span>
                                                            </button>
                                                         </li>
                                                      </ul>
                                                   </li>
                                                </ul>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div> --}}
                           </div>
                           <div class="ui-title-bar__mobile-primary-actions">
                              <div class="ui-title-bar__actions">
                                 <a href="{{ url('/admin/pagefooter/create') }}" class="ui-button ui-button--primary ui-title-bar__action">Thêm Page</a>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="ui-title-bar__actions-group">
                        <div class="ui-title-bar__actions">
                           <a href="{{ url('/admin/pagefooter/create') }}" class="ui-button ui-button--primary ui-title-bar__action">Thêm Page</a>
                        </div>
                     </div>
                  </div>
                  <div class="collapsible-header">
                     <div class="collapsible-header__heading"></div>
                  </div>
               </header>
               <div class="ui-layout ui-layout--full-width">
                  <div class="ui-layout__sections">
                     <div class="ui-layout__section">
                        <div class="ui-layout__item">
                           <section class="ui-card">
                              <div id="filterAndSavedSearch" context="filterandsavedsearch" define="{filterandsavedsearch: new Bizweb.ProductFilterAndSavedSearch(this,{&quot;type_filter&quot;:&quot;products&quot;})}">
                                 <div class="next-tab__container ">
                                    <ul class="next-tab__list filter-tab-list" id="filter-tab-list" role="tablist" data-has-next-tab-controller="true">
                                       <li class="filter-tab-item" data-tab-index="1">
                                          <a href="/admin/products" class="filter-tab filter-tab-active show-all-items next-tab next-tab--is-active">Tất cả bài viết</a>
                                       </li>
                                       <li class="next-tab__list__disclosure-item dropdown-container" id="hidden-search" style="display:none">
                                          <span class="next-tab next-tab--disclosure filter-tab" id="more-savedsearch" tabindex="-1" aria-selected="true" aria-label="dropdown item" aria-expanded="false" role="button">
                                             <svg class="next-icon next-icon--size-16 next-icon--no-nudge" id="svg-msaved">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-ellipsis"></use>
                                             </svg>
                                          </span>
                                          <ul class="dropdown-menu arrow-style dropdown-hidden-search dropdown-menu-right pull-right" role="tablist" id="dropdown-hidden-search"></ul>
                                       </li>
                                    </ul>
                                 </div>
                                 <div class="next-card__section next-card__section--no-bottom-spacing">
                                    <div class="obj-filter hide-when-printing table-filter-container">
                                       <div class="next-input-wrapper">
                                          <div class="next-field__connected-wrapper">
                                             <div class="next-field--connected--no-flex">
                                                <div class="ui-popover__container">
                                                   <button class="ui-button ui-btn-filter" type="button" name="button" id="ui-btn-filter">
                                                      Lọc <span class="btn__text--deprioritize" id="btn__text--deprioritize-filter">Bài viết</span>
                                                      <svg class="next-icon next-icon--size-20" id="svg-filter">
                                                         <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-disclosure"></use>
                                                      </svg>
                                                   </button>
                                                   <div class="ui-popover ui-popover--align-edge dropdown-menu margin-10 dropdown-filter">
                                                      <div class="ui-popover__tooltip" style="left: 75px;"></div>
                                                      <div class="ui-popover__content-wrapper">
                                                         <div class="ui-popover__content" style="max-height: 272px; width: 205px;">
                                                            <div class="ui-popover__section">
                                                               <div class="add-filters">
                                                                  <form bind-event-submit="submit(event)" class="clearfix">
                                                                     <p class="filter-heading">Hiển thị sản phẩm theo:</p>
                                                                     <div class="filter-builder">
                                                                        <div class="next-input-wrapper--half-spacing">
                                                                           <div class="next-input-wrapper">
                                                                              <label class="next-label helper--visually-hidden" for="filter-select-1">Chọn điều kiện lọc</label>
                                                                              <div class="ui-select__wrapper">
                                                                                 <select name="filter-select-1" bind="filterKey" bind-event-change="resetFilter()" id="filter-conditions" class="ui-select">
                                                                                    <option value="">Chọn điều kiện lọc...</option>
                                                                                    <option value="visibility">Hiển thị</option>
                                                                                    <option value="productType">Loại sản phẩm</option>
                                                                                    <option value="vendor">Nhà cung cấp</option>
                                                                                    <option value="tag">Đã được tag với</option>
                                                                                    <option value="collectionId">Danh mục sản phẩm</option>
                                                                                 </select>
                                                                                 <svg class="next-icon next-icon--size-16">
                                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
                                                                                 </svg>
                                                                              </div>
                                                                           </div>
                                                                        </div>
                                                                        <div bind-show="filterKey == 'visibility'" class="hide">
                                                                           <div class="next-input-wrapper--half-spacing">
                                                                              <div class="next-input-wrapper">
                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-2">Chọn điều kiện lọc</label>
                                                                                 <div class="ui-select__wrapper">
                                                                                    <select bind="option" class="ui-select filter-select" name="filter-select-2">
                                                                                       <option value="">Chọn điều kiện lọc...</option>
                                                                                       <option value="visibility">Hiển thị</option>
                                                                                       <option value="hidden">Ẩn</option>
                                                                                    </select>
                                                                                    <svg class="next-icon next-icon--size-16">
                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
                                                                                    </svg>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="next-input-wrapper--vertical-block">
                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
                                                                           </div>
                                                                        </div>
                                                                        <div bind-show="filterKey == 'productType'" class="hide">
                                                                           <div class="next-input-wrapper--half-spacing">
                                                                              <div class="next-input-wrapper">
                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-4">Chọn điều kiện lọc</label>
                                                                                 <div class="ui-select__wrapper">
                                                                                    <select bind="option" class="ui-select filter-select" name="filter-select-4">
                                                                                       <option value="">Chọn điều kiện lọc...</option>
                                                                                       <option value="Chocolate">Chocolate</option>
                                                                                       <option value="Vali">Vali</option>
                                                                                       <option value="Nhẫn">Nhẫn</option>
                                                                                       <option value="Nước hoa">Nước hoa</option>
                                                                                       <option value="Gấu bông">Gấu bông</option>
                                                                                       <option value="Quần áo">Quần áo</option>
                                                                                       <option value="Hoa">Hoa</option>
                                                                                       <option value="Hộp quà tặng">Hộp quà tặng</option>
                                                                                       <option value="Túi xách">Túi xách</option>
                                                                                       <option value="Balo">Balo</option>
                                                                                    </select>
                                                                                    <svg class="next-icon next-icon--size-16">
                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
                                                                                    </svg>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="next-input-wrapper--vertical-block">
                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
                                                                           </div>
                                                                        </div>
                                                                        <div bind-show="filterKey == 'vendor'" class="hide">
                                                                           <div class="next-input-wrapper--half-spacing">
                                                                              <div class="next-input-wrapper">
                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-5">Chọn điều kiện lọc</label>
                                                                                 <div class="ui-select__wrapper">
                                                                                    <select bind="option" class="ui-select filter-select" name="filter-select-5">
                                                                                       <option value="">Chọn điều kiện lọc...</option>
                                                                                       <option value="Barvo">Barvo</option>
                                                                                       <option value="DG">DG</option>
                                                                                       <option value="DTH">DTH</option>
                                                                                       <option value="Flower">Flower</option>
                                                                                       <option value="Her">Her</option>
                                                                                       <option value="ISCO">ISCO</option>
                                                                                       <option value="PRADA">PRADA</option>
                                                                                       <option value="Teddy">Teddy</option>
                                                                                       <option value="Vimax">Vimax</option>
                                                                                    </select>
                                                                                    <svg class="next-icon next-icon--size-16">
                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
                                                                                    </svg>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="next-input-wrapper--vertical-block">
                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
                                                                           </div>
                                                                        </div>
                                                                        <div bind-show="filterKey == 'tag'" class="hide">
                                                                           <div class="next-input-wrapper--half-spacing">
                                                                              <div class="next-input-wrapper">
                                                                                 <label class="next-label helper--visually-hidden" for="filter-input-4">Filter-input-4</label>
                                                                                 <input type="text" name="filter-input-4" id="filter-input-4" data-bind="value" bind-class="{error: isValid}" class="next-input">
                                                                              </div>
                                                                           </div>
                                                                           <div class="next-input-wrapper--vertical-block">
                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
                                                                           </div>
                                                                        </div>
                                                                        <div bind-show="filterKey == 'collectionId'" class="hide">
                                                                           <div class="next-input-wrapper--half-spacing">
                                                                              <div class="next-input-wrapper result-dropdown">
                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-4">Chọn điều kiện lọc</label>
                                                                                 <div class="ui-select__wrapper">
                                                                                    <input type="hidden" name="" class="form-control form-control smaller choosed-single-id" placeholder="Nhập Id">
                                                                                    <a class="ui-select dropdown-toggle btn-choose-product" href="#" bind-event-click="dropdown()">
                                                                                    <span class="choosed-single" style="margin-top: -4px;">
                                                                                    Chọn danh mục
                                                                                    </span>
                                                                                    </a>
                                                                                    <svg class="next-icon next-icon--size-16">
                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
                                                                                    </svg>
                                                                                 </div>
                                                                              </div>
                                                                           </div>
                                                                           <div class="next-input-wrapper--vertical-block">
                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
                                                                           </div>
                                                                        </div>
                                                                     </div>
                                                                  </form>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             
                                             <form action="/admin/products" bind-event-submit="submitQuery()" class="next-form next-form--full-width next-field--connected--no-border-radius" method="get">
                                                <label class="next-label helper--visually-hidden" for="query">Query</label>
                                                <div class="next-input--stylized next-field--connected">
                                                   <span class="next-input__add-on next-input__add-on--before">
                                                      <svg class="next-icon next-icon--color-slate-lightest next-icon--size-16">
                                                         <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-search-reverse"></use>
                                                      </svg>
                                                   </span>
                                                   <input type="text" name="search_text" id="search_text_blog" placeholder="Tìm kiếm sản phẩm"  value="" class="next-input next-input--invisible" onkeyup="retrieveBlog()">

                                                </div>
                                             </form>
                                             <div id="saved-search-actions-next" class="saved-search-actions-next" data-tg-refresh="saved-search-actions-next">
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="ui-card__section has-bulk-actions products" refresh="products" id="products-refresh">
                                 <div class="ui-type-container clearfix">
                                    <div class="table-wrapper" define="{bulkActions: new Bizweb.ProductBulkActions(this,{&quot;type&quot;:&quot;sản phẩm&quot;})}" context="bulkActions">
                                       <table id="price-rule-table" class="table-hover bulk-action-context expanded">
                                          <thead>
                                             <tr>
                                                <th class="select">
                                                   STT
                                                </th>
                                                
                                                <th><span>Tiêu đề</span></th>
                                                <th><span>Ngày đăng</span></th>
                                             </tr>
                                          </thead>
                                          <tbody >
                                             @php
                                                $stt= 0;
                                             @endphp
                                             @foreach($ftPage as $item)
                                             @php
                                                $stt = $stt +1;
                                             @endphp
                                             <tr data-define="{nestedLinkContainer: new Bizweb.NestedLinkContainer(this)}">
                                                <td class="select">
                                                   {!! $stt !!}
                                                </td>
                                                
                                                <td class="name">
                                                   <div class="ui-stack ui-stack--wrap">
                                                      <div class="ui-stack-item">
                                                         <a href="{{ url('admin/pagefooter/edit/'.$item->id) }}" data-nested-link-target="true">{{ $item->title }}</a>
                                                        
                                                      </div>
                                                   </div>
                                                </td>
                                                
                                                <td class="vendor">
                                                   
                                                   <p>{{ $item->created_at }}</p>
                                                </td>
                                             </tr>
                                             @endforeach
                                          </tbody>
                                       </table>
                                      
                                      
                                     
                                    </div>
                                    <div class="t-grid-pager-boder">
                                       <div class="t-pager t-reset clearfix fix-margin-pager">
                                          <div class="col-md-6 col-lg-6 hidden-xs hidden-sm no-padding">
                                             {{-- {{ $products->link() }} --}}
                                          </div>
                                       </div>
                                    </div>
                                    <div define="{products_match_current_search: 20}"></div>

                                 </div>
                              </div>
                           </section>
                        </div>
                        
                     </div>
                  </div>
               </div>
         </div>
      </div>
   </div>
</main>
@endsection
@section('script')
<script>
   $(document).ready(function(){
      $(window).scroll(function () {
          scrollPaggingMobile('.products');
         });
      ////////////
   });

   
</script>
@endsection
