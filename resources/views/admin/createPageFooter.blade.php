@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Thêm mới sản phẩm')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div id="products-new" class="product-detail page">
                    <form autocomplete="off" data-context="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <header class="ui-title-bar-container">
                            <div class="ui-title-bar">
                                <div class="ui-title-bar__navigation">
                                    <div class="ui-breadcrumbs">
                                        <a href="/admin/products" class="ui-button ui-button--transparent ui-breadcrumb">
                                            <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left-thinner"></use>
                                            </svg>
                                            <span class="ui-breadcrumb__item"> Page Footer</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="ui-title-bar__main-group">
                                    <div class="ui-title-bar__heading-group">
                                        <span class="ui-title-bar__icon">
                                            <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-products"></use>
                                            </svg>
                                        </span>
                                        <h1 class="ui-title-bar__title"> Đăng Page Footer mới </h1>
                                    </div>
                                </div>
                            </div>
                        </header>
                        
                        <div class="ui-layout">
                            <div class="ui-layout__sections">
                                <div class="ui-layout__section ui-layout__section--primary">
                                    <div class="ui-layout__item">
                                        <section class="ui-card" id="product-form-container">
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="product-name"> Tiêu đề</label>
                                                        <input required name="ftName" autofocus="autofocus" id="product-name" onkeyup="metaTitle(this)" placeholder="Nhập tên sản phẩm" class="next-input" size="30" type="text" required="">
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="Content"> Nội dung page viết </label>
                                                        <textarea id="noidung" required class="next-input ckeditor" name="ftContent"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="ui-page-actions">
                            <div class="ui-page-actions__container">
                                <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                                    <div class="ui-page-actions__button-group">
                                        <a class="btn" data-allow-default="1" href=" "> Hủy </a>
                                        <button name="button" type="submit" class="btn js-btn-primary js-btn-loadable btn-primary has-loading"> Lưu </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
            CKEDITOR.replace( 'noidung');
            CKEDITOR.replace( 'noidung2');
   </script>
   <script type="text/javascript">
                    function changeImg(input){
                        if(input.files && input.files[0]){
                            var reader = new FileReader();
                            reader.onload = function(e){
                                $(input).prev().attr('src',e.target.result);
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                </script>
</main>
@endsection