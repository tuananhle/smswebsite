<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> Đăng nhập hệ thống quản trị </title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ url('css/login.css') }}">
    </head>
    <body>
        <div class="bgImg"></div>
        <div class="content">
            <div class="d-flex h-100 align-items-center justify-content-center">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 ml-auto mr-auto">
                            <div class="card">
                                <div class="card-body">
                                        <div class="glowBox">
                                            <h4 class="title">Hệ thống quản trị</h4>
                                        </div>
                                    <div class="signUp">
                                        @if(Session::has('success'))
                                            <span style="color: #05d500; margin-top: 20px; display: block;">{{ Session::get('success') }}</span>
                                        @endif
                                        @if(Session::has('error'))
                                            <span style="color: #f33; margin-top: 20px; display: block;">{{ Session::get('error') }}</span>
                                        @endif
                                        <form method="post">
                                             {{ csrf_field() }}
                                            <span>
                                                <input type="text" class="form" name="email" placeholder="Email">
                                                <span class="underline"></span>
                                            </span>
                                            <span>
                                                <input type="password" class="form" name="password" placeholder="Password">
                                                <span class="underline"></span>
                                            </span>
                                            <span>
                                                <input type="checkbox" name="Remember"> Nhớ mật khẩu
                                            </span>
                                            <div class="text-center" style="padding: 30px 0 0;">
                                                <button type="submit" class="btn btn-primary"> Đăng nhập </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
