$('.has-submenu a').click(function()
{
	if($(this).hasClass('active-temp')){}
	else{
		$(this).addClass('active-temp')
		$('.has-submenu a').not($(this)).removeClass('active-temp')
	}
	var elm = $(this).attr('data-name');
	$("ol").not("[data-nav-section='" + elm + "']").removeClass('next-nav__list--is-open');
	var ol  = $("ol[data-nav-section='" + elm + "']");
	var parDiv = $('ol').parent('.next-nav__panel');
	if (ol.hasClass('next-nav__list--is-open')) {
		ol.removeClass('next-nav__list--is-open');
		$('#sidebar--nav').removeClass('next-nav--is-expanded');
		parDiv.removeClass('is-transitioning').removeClass('next-nav__panel--secondary-open');
	}
	else{
		ol.addClass('next-nav__list--is-open');
		$('#sidebar--nav').addClass('next-nav--is-expanded');
		if(parDiv.hasClass( "next-nav__panel--secondary-open" )){
			parDiv.addClass('is-transitioning');
		}
		else{
			parDiv.addClass('next-nav__panel--secondary-open');
		}
	}
})

function closeSubMenu() {
	var parDiv = $('ol').parent('.next-nav__panel');
	$('ol').removeClass('next-nav__list--is-open');
	$('#sidebar--nav').removeClass('next-nav--is-expanded');
	parDiv.removeClass('is-transitioning').removeClass('next-nav__panel--secondary-open');
}

function addProductImage() {
	var Elm = $('#product-images');
	var imgItem = 	'<li class="js-product-photo-grid-item product-photo-grid__item ui-sortable-handle" id="product-image-27506810">\
                        <div class="aspect-ratio aspect-ratio--square">\
                            <img class="aspect-ratio__content" src="/img/no-image.png" style="max-width: 60px;">\
                            <div class="product-photo-hover-overlay drag">\
                                <a href="javascript::void(0)" onclick="removeImgProduct(this)" style="position:absolute;top:0;right:0;color:#fff;margin:10px;text-decoration:none">\
                                    <i class="fa fa-remove"></i> <span> Xóa </span>\
                                </a>\
                                <div style="position:absolute;top:40%;width:100%;text-align:center;">\
                                    <span class="btn btn-sm btn-info" onclick="$(this).next().click()"> Chọn ảnh </span>\
                                    <input type="file" name="product[image][]" multiple class="hide" onchange="fileInputChanged(this)">\
                                </div>\
                            </div>\
                        </div>\
                    </li>';
    Elm.append(imgItem);
}

function fileInputChanged(input){
	if(input.files && input.files[0]){
        var reader = new FileReader();
        reader.onload = function(e){
        	var srcImg  =  e.target.result;
        	var itemImg = $(input).parents('.product-photo-grid__item').find('img').attr({ 'src': srcImg, 'style':'width:100%' });
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function removeImgProduct(that) {
	$(that).parents('.product-photo-grid__item').remove();
}

function metaTitle(that) {
	var product_name = $(that).val();
	var pr_slug = convertToSlug(product_name);
	var lengthTitle = product_name.length;
	$('#nva__google__title').text(product_name);
	$('#seo-title-tag').val(product_name);
	$('#nva__google__alias').text(pr_slug);
	$('#object-handle').val(pr_slug);
	$('#ltn__title').text(lengthTitle);
}
function metaMenu(that){
    var menu_name = $(that).val();
    var menu_slug = convertToSlug(menu_name);
    var lengthTitle = menu_name.length;
    $('#alias-handle').val(menu_slug);

}
function onMetaDescription(that) {
	var text_des = $(that).val();
	var length_text_des = text_des.length;
	$('.nva__google__description').text(text_des);
	$('#seo_description_lnt').text(length_text_des);
}

function onAliasSeo(that) {
	var text_alias = $(that).val();
	$('#nva__google__alias').text(convertToSlug(text_alias));
}

function convertToSlug(title){
     slug = title.toLowerCase();
     slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
     slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
     slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
     slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
     slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
     slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
     slug = slug.replace(/đ/gi, 'd');
     slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
     slug = slug.replace(/ /gi, "-");
     slug = slug.replace(/\-\-\-\-\-/gi, '-');
     slug = slug.replace(/\-\-\-\-/gi, '-');
     slug = slug.replace(/\-\-\-/gi, '-');
     slug = slug.replace(/\-\-/gi, '-');
     slug = '@' + slug + '@';
     slug = slug.replace(/\@\-|\-\@|\@/gi, '');
	 return slug;
};

function fileInputAvatarImage(input)
{
    if(input.files && input.files[0]){
        var reader = new FileReader();
        reader.onload = function(e)
        {
            var srcImg  =  e.target.result;
            $('#avatar__member__nva').attr({ 'src': srcImg, 'style':'width:100%' });
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function editPassWordUser() {
    $(".btn-open-edtp__nva02").click();
}
function updatePassword(user_id) {
    var password_new = $('input[name=password_new]').val();
    var repassword_new = $('input[name=repassword_new]').val();
    var password_old = $('input[name=password_old]').val();
    var formData = new FormData();
    formData.append('pass_old', password_old);
    formData.append('re_pass_new', repassword_new);
    formData.append('pass_new', password_new);
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        type: "POST",
        url: "/admin/members/editpass/"+user_id,
    	dataType: 'json',
        contentType: false,
        processData: false,
        data: formData,
        success: function(data){
			$('.err__ip').removeClass('input-error');
			$('.spn__err').text('');
            switch(data.code){
                case 403:
                    $('input[name=password_old]').addClass('input-error').next('.mes_err__ads01').text(data.messages);
                    break;
                case 400:
                	console.log('lỗi 400');
                    if (data.error == 'pass_new') {
                        $('.mes_err__pns').text(data.messages).prev().addClass('input-error');
                    }
                    else if (data.error == 're_pass_new'){
                        $('.mes_err__rpns').text(data.messages).prev().addClass('input-error');
                    }
                    break;
                case 200:
                    $('#edtp__nva02').modal('hide'); alert(data.messages);
                    window.location.href = '/admin/members/list';
                    break;
                default:
                    console.log(data.messages);
                    break;
            }
		}
	});
}
function retrieve(){
    var country = $('#search_text').val();
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });
    $.ajax({
      type: "POST",
      url: '/admin/products/search',
      data: {country:country},
      dataType: 'html',
      success: function(result){
        $('#ajaxtable').html(result);
      }
    });

  }