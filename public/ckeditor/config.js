/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//http://[tên miền của bạn]/ckfinder/ckfinder.html’
	config.filebrowserBrowseUrl = 'http://mixone.ta/ckfinder/ckfinder.html';
	config.filebrowserImageBrowseUrl = 'http://mixone.ta/ckfinder/ckfinder.html?type=Images';
	config.filebrowserFlashBrowseUrl = 'http://mixone.ta/ckfinder/ckfinder.html?type=Flash';
	config.filebrowserUploadUrl = 'http://mixone.ta/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
	config.filebrowserImageUploadUrl = 'http://mixone.ta/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
	config.filebrowserFlashUploadUrl = 'http://mixone.ta/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
	
};
