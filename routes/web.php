<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('login-admin', 'LoginController@renderview')->middleware('CheckLogin');
Route::post('login-admin', 'LoginController@postLogin');
Route::get('admin/logout',['as'=>'logout','uses'=>'LoginController@logout']);

Route::get('admin', function() {
	return redirect('admin/dashboard');
});

Route::group(['prefix' => 'admin/dashboard', 'middleware' => 'CheckAdmin'], function(){
	Route::get('/', 'DashAdminController@dashboard');
});
Route::group(['prefix' => 'admin/pagefooter', 'middleware' => 'CheckAdmin'], function(){
	Route::get('/create','FooterController@create');
	Route::post('/create','FooterController@postCreate');
	Route::get('/list','FooterController@getList');
	Route::get('/edit/{id}','FooterController@getEdit');
	Route::post('/edit/{id}','FooterController@postEdit');
	Route::get('/del/{id}','FooterController@delete');
});

Route::group(['prefix'=>'/'],function()
{
	Route::get('lien-he.html','PageController@lienhe');
	Route::get('/',['as'=>'getIndex','uses'=>'PageController@index']);
	Route::get('dich-vu.html','PageController@listBlog');
	Route::get('dich-vu/{id}-{alias}.html','PageController@detailBlog');
	Route::get('{alias}.html',['as'=>'getPro','uses'=>'PageController@listPro']);
	Route::get('san-pham/{alias}.html',['as'=>'getdetailPro','uses'=>'PageController@detaiPro']); //detailproduct
	Route::get('search','PageController@search');
});

