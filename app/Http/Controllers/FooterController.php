<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Footerpage;
use Illuminate\Support\Str;


class FooterController extends Controller
{
    public function create()
    {
    	return view('admin.createPageFooter');
    }
    public function postCreate(Request $request)
    {
    	$ftpage = new Footerpage();
    	$ftpage->title = $request->ftName;
    	$ftpage->alias = Str::slug($request->ftName);
    	$ftpage->content = $request->ftContent;
    	$ftpage->save();
    	if ($ftpage->id) {
    		return redirect('admin/pagefooter/list')->with('success','Thêm page thành công');
    	}
    	else
        {
            return back()->with('error', 'Đăng page không thành công');
        }
    }
    public function getList()
    {
    	$data['ftPage'] = Footerpage::get();
    	return view('admin.listPageFooter',$data);
    }
    public function getEdit($id)
    {
    	$data['ftPage'] = Footerpage::where('id', $id)->first();
    	return view('admin.editPageFooter',$data);
    }
    public function postEdit(Request $request,$id)
    {
    	$ftpage = Footerpage::find($id);
    	$ftpage->title = $request->ftName;
    	$ftpage->alias = Str::slug($request->ftName);
    	$ftpage->content = $request->ftContent;
    	$ftpage->save();
    	if ($ftpage->id) {
    		return redirect('admin/pagefooter/list')->with('success','Cập nhật page thành công');
    	}
    	else
        {
            return back()->with('error', 'Cập nhật page không thành công');
        }
    }
    public function delete($id)
    {
    	$ftPage = Footerpage::where('id',$id)->first();
    	$ftPage->delete($id);
    	return redirect('admin/pagefooter/list');
    }
}
