<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductType;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Entities\ProductVendor;
use Modules\Blog\Entities\Blog;
use Modules\Menu\Entities\Menu;

class PageController extends Controller
{
    public function index()
    {
    	return view('frontend.home');
    }
    public function lienhe()
    {
        return view('frontend.pages.lienhe');
    }
    public function listPro($alias)
    {
        $checkMenu = Menu::where('mn_alias', $alias)->first();
        if ($checkMenu != null)
        {
            $data['menu2'] =  Menu::where('mn_alias',$alias)->first();
            return view('frontend.pages.pageMenu',$data);
        }
    	$cateId = ProductCategory::where('pc_alias', $alias)->first();

    	if($cateId != null)
    	{
    		$where = 'pro_cate';
    		$id = $cateId->pc_id;
    		$data['title'] = $cateId->pc_name;
    	}
    	else
    	{
    		$typeId = ProductType::where('pt_alias', $alias)->first();
    		if($typeId != null)
    		{
    			$where = 'pro_type';
    			$id = $typeId->pt_id;
    			$data['title'] = $typeId->pt_name;
    		}
    	}
    	$product = Product::where($where, $id)->orderby('id', 'desc')->paginate(10);
    	if(count($product) > 0) {
    		$data['product'] = $product;
    		return view('frontend.pages.listProduct',$data);
    	}
    	else {
    		return redirect('403');
    	}
    }
    public function detaiPro($alias)
    {
    
    	$data['products'] = Product::where('pro_alias', $alias)->first();
    	return view('frontend.pages.detailProduct',$data);
    }
    public function listBlog()
    {
        $data['blog'] = Blog::get();
        return view('frontend.pages.listBlog',$data);
    }
    public function detailBlog($id)
    {
        $data['detailblog'] = Blog::where('bl_id',$id)->first();
        return view('frontend.pages.detailBlog',$data);
    }
    // public function pagemenu($alias)
    // {
    //     $data['menu2'] =  Menu::where('mn_alias',$alias)->first();
    //     return view('frontend.pages.pageMenu',$data);
    // }
    public function search(Request $request)
    {
        $data['keyword'] = $request->search_text;
        $data['search'] = Product::where('pro_name', 'like', "%".$data['keyword']."%")->orWhere('pro_code', 'like', "%".$data['keyword']."%")->paginate(10);
        return view('frontend.pages.search',$data);
    }
}
