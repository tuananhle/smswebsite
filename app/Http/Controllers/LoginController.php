<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth,Session;

class LoginController extends Controller
{
    public function renderview()
    {
    	return view('admin.login');
    }
    public function postLogin(Request $request)
    {
    	$arr = [
            'email'	=>	$request->email,
            'password'	=>	$request->password
        ];
        if($request->remember == 'Remember') {
            $remember = true;
        }
        else {
            $remember = false;
        }
        if(Auth::guard('admin')->attempt($arr, $remember))
        {
            return redirect('admin/dashboard')->with('success', 'Đăng nhập thành công');
        }
        else
        {
            return back()->with('error', 'Tài khoản hoặc mật khẩu không tồn tại');
        }
    }
    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/login-admin');
    }
}
