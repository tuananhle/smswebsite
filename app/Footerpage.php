<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Footerpage extends Model
{
    protected $table = 'footerpages';
    protected $guarded = [];
}
