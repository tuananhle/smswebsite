<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use DB,View;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $menu = DB::table('menus')->get();
        $address = DB::table('address_setting')->get();
        $suppliers = DB::table('suppliers')->limit(7)->get();
        $setting = DB::table('settings')->first();
        $banner = DB::table('banner')->get();
        $pro = DB::table('products')->limit(7)->get();
        $cate = DB::table('product_categoies')->get();
        $cate_blog = DB::table('blog_cates')->get();
        View::share([
            'address'        => $address,
            'suppliers'        => $suppliers,
            'pro'        => $pro,
            'setting'        => $setting,
            'banner'        => $banner,
            'menu'        => $menu,
            'cate'        => $cate,
            'cate_blog'        => $cate_blog,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
