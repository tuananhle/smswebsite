@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
	<main id="AppFrameMain" class="ui-app-frame__main">
	   <div class="wrapper" id="wrapper">
	      <div id="body" class="page-content clearfix" data-tg-refresh="body">
	         <div id="content">
	            <div id="customer-show" class="page">
	               <script type="text/javascript">
	                  Page(function () {
	                      return {
	                          ordersCount: 0,
	                          state: 'disabled',
	                      }
	                  });
	               </script>
	               <form accept-charset="UTF-8" action="/admin/customers/8535637" autocomplete="off" class="edit_customer" data-id="edit_customer" data-tg-refresh="edit_customer" data-tg-refresh-on-error="edit_customer" data-tg-remote="true" define="{form: new Bizweb.Forms(this, {disabledUntilDirty: true, alertUnsavedChanges: true})}" id="edit_customer_8535637" method="post">
	                  <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCTENgLZOTcYuqakF+qXYqbmiLGY9B9VrHJ6ElQ3aDj9JZDW8tMGkoKDhamIWBkiIPg==">        
	                  <header class="ui-title-bar-container" id="customer-header" data-tg-refresh="customer-header">
	                     <div class="ui-title-bar">
	                        <div class="ui-title-bar__navigation">
	                           <div class="ui-breadcrumbs">
	                              <a href="/admin/customers" class="ui-button ui-button--transparent ui-breadcrumb">
	                                 <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
	                                    <use xlink:href="#chevron-left-thinner"></use>
	                                 </svg>
	                                 <span class="ui-breadcrumb__item">Khách hàng</span>
	                              </a>
	                           </div>
	                        </div>
	                        <div class="ui-title-bar__main-group">
	                           <div class="ui-title-bar__heading-group">
	                              <span class="ui-title-bar__icon">
	                                 <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
	                                    <use xlink:href="#next-customers"></use>
	                                 </svg>
	                              </span>
	                              <h1 class="ui-title-bar__title">lê anh</h1>
	                           </div>
	                           <script class="modal_source" define="{activeAccountModal: new Bizweb.Modal(this)}" type="text/html">
	                              <div class="modal-dialog">
	                              <div class="modal-content">
	                              <form method="post" action="/admin/customers/activeaccount" class="form-horizontal" data-define="{ activeAccountForm: new Bizweb.Forms(this, { disabledUntilDirty: true, disableContextBar: true }) }" data-tg-refresh="active_account_form" data-tg-refresh-on-error="active_account_form" data-tg-refresh-on-success="customer-show customer-header customer-card modal" data-tg-remote="true" id="active_account_form" accept-charset="UTF-8">
	                              <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCTENgLZOTcYuqakF+qXYqbmiLGY9B9VrHJ6ElQ3aDj9JZDW8tMGkoKDhamIWBkiIPg==" />
	                              <input type="hidden" name="returnUrl" value="/admin/customers/8535637" />
	                              <input type="hidden" name="id" value="8535637" />
	                              <div class="ui-modal__header">
	                              <div class="ui-modal__header-title">
	                              <h2 class="ui-title">Kích hoạt tài khoản</h2>
	                              </div>
	                              <div class="ui-modal__header-actions">
	                              <div class="ui-modal__close-button">
	                              <button class="ui-button ui-button--transparent close-modal" data-dismiss="modal" aria-label="Close modal" type="button" name="button">
	                                  <svg class="next-icon next-icon--color-ink-lighter next-icon--size-20">
	                                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cancel-small-minor"></use>
	                                  </svg>
	                              </button>
	                              </div>
	                              </div>
	                              </div>
	                              <div class="ui-modal__body">
	                              <div class="ui-modal__section">
	                              <div class="ui-type-container">
	                              <div class="ui-form__section">
	                                  <div class="ui-form__group">
	                                      <div class="next-input-wrapper">
	                                          <label class="next-label" for="NewPassword">Mật khẩu</label>
	                                          <input class="next-input" data-val="true" data-val-length="Mật khẩu dài từ 6 đến 50 ký tự" data-val-length-max="50" data-val-length-min="6" data-val-required="Nhập vào mật khẩu" id="NewPassword" name="NewPassword" placeholder="Nhập Mật khẩu" type="password">
	                                          <span class="field-validation-valid help-block" data-valmsg-for="NewPassword" data-valmsg-replace="true"></span>
	                                      </div>
	                                      <div class="next-input-wrapper">
	                                          <label class="next-label" for="ConfirmPassword">Xác nhận mật khẩu</label>
	                                          <input class="next-input" data-val="true" data-val-equalto="Mật khẩu mới không khớp nhau" data-val-equalto-other="*.NewPassword" id="ConfirmPassword" name="ConfirmPassword" placeholder="Nhập lại Mật khẩu" type="password">
	                                          <span class="field-validation-valid help-block" data-valmsg-for="ConfirmPassword" data-valmsg-replace="true"></span>
	                                      </div>
	                                  </div>
	                              </div>
	                              </div>
	                              </div>
	                              </div>
	                              <div class="ui-modal__footer">
	                              <div class="ui-modal__footer-actions">
	                              <div class="ui-modal__tertiary-actions">
	                              <div class="ui-type-container">
	                                  <div class="ui-form__section">
	                                      <div class="next-input-wrapper next-input-small-margin-top">
	                                          
	                                          <label class="next-label next-label--switch inline fw-normal type--subdued note" for="send-email">
	                                              Gửi
	                                              <a href="/admin/settings/notifications" target="_blank" rel="noreferrer noopener">Email thông báo</a>
	                                              tới khách hàng
	                                          </label>
	                                          <input checked="checked" class="next-checkbox" id="SendNotificationEmail" name="SendNotificationEmail" type="checkbox" value="true" /><input name="SendNotificationEmail" type="hidden" value="false" />
	                                          <span class="next-checkbox--styled">
	                                              <svg class="next-icon next-icon--size-10 checkmark">
	                                                  <use xlink:href="#next-checkmark-thick" />
	                                              </svg>
	                                          </span>
	                                      </div>
	                                  </div>
	                              </div>
	                              </div>
	                              <div class="ui-modal__secondary-actions">
	                              <div class="button-group">
	                                  <button class="ui-button close-modal" data-dismiss="modal" type="button" name="button">Hủy</button>
	                              </div>
	                              </div>
	                              <div class="ui-modal__primary-actions">
	                              <div class="button-group button-group--right-aligned">
	                                  <button class="ui-button btn-primary js-btn-loadable has-loading" type="submit" name="commit">Kích hoạt</button>
	                              </div>
	                              </div>
	                              </div>
	                              </div>
	                              </form>
	                              </div>
	                              </div>
	                              
	                           </script>
	                           <div define="{titleBarActions: new Bizweb.TitleBarActions(this)}" class="action-bar">
	                              <div class="action-bar__item action-bar__item--link-container">
	                                 <div class="action-bar__top-links">
	                                    <button class="ui-button ui-button--transparent action-bar__link" bind-event-click="activeAccountModal.show()" data-popover-index="1" type="button" name="button">
	                                       <svg class="next-icon next-icon--size-20 action-bar__link-icon next-icon--no-nudge">
	                                          <use xlink:href="#next-account"></use>
	                                       </svg>
	                                       Kích hoạt tài khoản
	                                    </button>
	                                 </div>
	                                 <div class="action-bar__more hide">
	                                    <div class="ui-popover__container">
	                                       <button class="ui-button ui-button--transparent" type="button" name="button">
	                                          <span data-singular-label="Chọn thao tác" data-multiple-label="Chọn thao tác" class="action-bar__more-label">Chọn thao tác</span>
	                                          <svg class="next-icon next-icon--size-20">
	                                             <use xlink:href="#next-disclosure"></use>
	                                          </svg>
	                                       </button>
	                                       <div class="ui-popover ui-popover--align-edge">
	                                          <div class="ui-popover__tooltip"></div>
	                                          <div class="ui-popover__content-wrapper">
	                                             <div class="ui-popover__content">
	                                                <div class="ui-popover__pane">
	                                                   <ul class="action-bar__popover-wrapper">
	                                                      <li>
	                                                         <ul class="ui-action-list">
	                                                            <li class="ui-action-list__item">
	                                                               <button class="ui-action-list-action action-bar__popover-hidden-item" data-popover-index="1" bind-event-click="activeAccountModal.show()" type="button" name="button">
	                                                                  <span class="ui-action-list-action__text">
	                                                                     <span class="action-bar__popover-icon-wrapper">
	                                                                        <svg class="next-icon next-icon--color-blue next-icon--size-16 action-bar__popover-icon">
	                                                                           <use xlink:href="#next-account"></use>
	                                                                        </svg>
	                                                                     </span>
	                                                                     Kích hoạt tài khoản
	                                                                  </span>
	                                                               </button>
	                                                            </li>
	                                                         </ul>
	                                                      </li>
	                                                   </ul>
	                                                </div>
	                                             </div>
	                                          </div>
	                                       </div>
	                                    </div>
	                                 </div>
	                              </div>
	                           </div>
	                        </div>
	                        <div class="ui-title-bar__actions-group">
	                           <div class="ui-title-bar__actions">
	                              <button class="ui-button ui-button--primary has-loading js-btn-loadable js-btn-primary ui-title-bar__action btn-primary" type="submit" name="button" disabled="disabled">
	                              Lưu
	                              </button>
	                           </div>
	                        </div>
	                     </div>
	                     <div class="collapsible-header">
	                        <div class="collapsible-header__heading">
	                        </div>
	                     </div>
	                  </header>
	                  <div class="ui-layout">
	                     <div class="ui-layout__sections">
	                        <div class="ui-layout__section ui-layout__section--primary">
	                           <div class="ui-layout__item">
	                              <div class="next-card">
	                                 <header class="next-card__header" id="customer-profile" refresh="customer-profile">
	                                    <div class="next-grid next-grid--no-outside-padding next-grid--vertically-centered">
	                                       <div class="next-grid__cell next-grid__cell--no-flex">
	                                          <span class="user-avatar user-avatar--style-custom-1">
	                                          <span class="user-avatar__initials">
	                                          <img class="profile-pic img-circle" data-name="anh lê" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHBvaW50ZXItZXZlbnRzPSJub25lIiB3aWR0aD0iNDYiIGhlaWdodD0iNDYiIHN0eWxlPSJiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTQyLCA2OCwgMTczKTsgd2lkdGg6IDQ2cHg7IGhlaWdodDogNDZweDsgYm9yZGVyLXJhZGl1czogMHB4OyI+PHRleHQgdGV4dC1hbmNob3I9Im1pZGRsZSIgeT0iNTAlIiB4PSI1MCUiIGR5PSIwLjM1ZW0iIHBvaW50ZXItZXZlbnRzPSJhdXRvIiBmaWxsPSIjZmZmZmZmIiBmb250LWZhbWlseT0iSGVsdmV0aWNhTmV1ZS1MaWdodCxIZWx2ZXRpY2EgTmV1ZSBMaWdodCxIZWx2ZXRpY2EgTmV1ZSxIZWx2ZXRpY2EsIEFyaWFsLEx1Y2lkYSBHcmFuZGUsIHNhbnMtc2VyaWYiIHN0eWxlPSJmb250LXdlaWdodDogNDAwOyBmb250LXNpemU6IDIwcHg7Ij5BPC90ZXh0Pjwvc3ZnPg==">
	                                          </span>
	                                          <img class="gravatar gravatar--size-medium" srcset="https://secure.gravatar.com/avatar/e08f8887c1018aa90da479413e282d9a.jpg?s=80&amp;d=blank 2x" src="https://secure.gravatar.com/avatar/e08f8887c1018aa90da479413e282d9a.jpg?s=80&amp;d=blank" alt="Avatar 80x80">
	                                          </span>
	                                       </div>
	                                       <div class="next-grid__cell">
	                                          <h3 class="next-heading next-heading--no-margin">
	                                             lê anh
	                                          </h3>
	                                          <p class="type--subdued">
	                                             Quận 1, TP Hồ Chí Minh, Việt Nam
	                                          </p>
	                                       </div>
	                                    </div>
	                                 </header>
	                                 <section class="next-card__section">
	                                    <div class="ppt" id="customer-notes" refresh="notes">
	                                       <div class="next-input-wrapper">
	                                          <label class="next-label" for="Note">Ghi chú</label>
	                                          <textarea class="next-input next-resize-vertical" cols="20" data-val="true" data-val-length="Ghi chú tối đa 255 ký tự" data-val-length-max="255" id="Note" name="Note" placeholder="Nhập Ghi chú về khách hàng" rows="2"></textarea>
	                                       </div>
	                                    </div>
	                                 </section>
	                              </div>
	                           </div>
	                           <div class="ui-layout__item" id="recent-orders" refresh="recent-orders">
	                              <section class="ui-card">
	                                 <header class="ui-card__header">
	                                    <div class="ui-type-container">
	                                       <div class="ui-stack ui-stack--wrap ui-stack--distribution-trailing ui-stack--alignment-center">
	                                          <div class="ui-stack-item ui-stack-item--fill">
	                                             <h2 class="ui-heading">Đơn hàng gần đây</h2>
	                                          </div>
	                                       </div>
	                                    </div>
	                                 </header>
	                                 <div class="ui-card__section">
	                                    <div class="ui-type-container">
	                                       <div class="ui-stack ui-stack--wrap ui-stack--vertical ui-stack--alignment-center ui-stack--spacing-loose">
	                                          <svg class="next-icon next-icon--color-sky-darker next-icon--size-80">
	                                             <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-order"></use>
	                                          </svg>
	                                          <h2>Khách hàng chưa có đơn đặt hàng</h2>
	                                       </div>
	                                    </div>
	                                 </div>
	                              </section>
	                           </div>
	                        </div>
	                        <div class="ui-layout__section ui-layout__section--secondary">
	                           <div class="ui-layout__item">
	                              <aside class="next-card" id="customer-card" refresh="customer-card">
	                                 <header class="next-card__header">
	                                    <div class="next-grid next-grid--no-outside-padding next-grid--vertically-centered">
	                                       <div class="next-grid__cell">
	                                          <h3 class="next-heading">Liên hệ</h3>
	                                       </div>
	                                       <div class="next-grid__cell next-grid__cell--no-flex">
	                                          <button class="ui-button btn--link" bind-event-click="editOverviewModal.show()" type="button" name="button">Sửa</button>
	                                       </div>
	                                    </div>
	                                 </header>
	                                 <section class="next-card__section">
	                                    <a href="mailto:tuana@gmail.com" class="text-gray">
	                                    tuana@gmail.com
	                                    </a>
	                                    <ul class="unstyled type--subdued">
	                                       <li>
	                                          Không đồng ý nhận tiếp thị
	                                       </li>
	                                       <li>
	                                          Chưa có tài khoản
	                                       </li>
	                                    </ul>
	                                 </section>
	                                 <section class="next-card__section">
	                                    <div class="next-grid next-grid--no-outside-padding">
	                                       <div class="next-grid__cell next-heading next-heading--small ">
	                                          Sổ địa chỉ
	                                       </div>
	                                       <div class="next-grid__cell next-grid__cell--no-flex">
	                                          <div class="ui-popover__container ui-popover__container--contains-active-popover">
	                                             <button class="ui-button btn--link" bind-event-click="false" type="button" name="button" id="ui-popover-activator--8" aria-expanded="true" aria-haspopup="true" aria-owns="ui-popover--8" aria-controls="ui-popover--8">Sửa</button>
	                                             <div class="ui-popover ui-popover--full-height">
	                                                <div class="ui-popover__tooltip"></div>
	                                                <div class="ui-popover__content-wrapper">
	                                                   <div class="ui-popover__content">
	                                                      <div class="ui-popover__pane ui-popover__pane--fixed">
	                                                         <div class="next-grid next-grid--no-outside-padding next-grid--vertically-centered">
	                                                            <div class="next-grid__cell">
	                                                               <h3 class="next-heading next-heading--no-margin">
	                                                                  Thay đổi địa chỉ
	                                                               </h3>
	                                                            </div>
	                                                            <div class="next-grid__cell next-grid__cell--no-flex">
	                                                               <button class="ui-button btn--link" bind-event-click="addAddressModal.show()" type="button" name="button">Thêm địa chỉ mới</button>
	                                                            </div>
	                                                         </div>
	                                                      </div>
	                                                      <div class="ui-popover__pane">
	                                                         <div class="ui-popover__section">
	                                                            <div class="next-grid next-grid--no-outside-padding">
	                                                               <div class="next-grid__cell">
	                                                                  <h6 class="next-heading--tiny next-heading--semi-bold next-heading--half-margin">
	                                                                     Địa chỉ mặc định
	                                                                  </h6>
	                                                                  <p class="type--subdued word_break__content">lê anh</p>
	                                                                  <p class="type--subdued word_break__content"></p>
	                                                                  <p class="type--subdued">Quận 1</p>
	                                                                  <p class="type--subdued word_break__content">TP Hồ Chí Minh<span> </span></p>
	                                                                  <p class="type--subdued">Việt Nam</p>
	                                                                  <div class="st">
	                                                                     <script class="modal_source" define="{editAddressModal_10398624: new Bizweb.Modal(this)}" type="text/html">
	                                                                        <form define="{ form10398624: new Bizweb.Forms(this, { disabledUntilDirty: true, alertUnsavedChanges: true, disableContextBar: true })}" action="/admin/customers/editaddress/10398624?customerId=8535637&amp;currentDefault=True" method="post" accept-charset="UTF-8" autocomplete="off" tg-remote="true" refresh-on-error="edit_customer_address" refresh-on-success="modal customer-address customer-card" refresh="edit_customer_address" id="edit_customer_address">
	                                                                            <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCTENgLZOTcYuqakF+qXYqbmiLGY9B9VrHJ6ElQ3aDj9JZDW8tMGkoKDhamIWBkiIPg==" />
	                                                                            <div define="{customerAddress: new Bizweb.CustomerAddress(this,{&quot;district_slt&quot;:&quot;#districtSlt&quot;,&quot;province_slt&quot;:&quot;#provinceSlt&quot;,&quot;have_city&quot;:true})}" context="customerAddress" class="modal-dialog">
	                                                                                <div class="modal-content">
	                                                                                    <div class="ui-modal__header">
	                                                                                        <div class="ui-modal__header-title">
	                                                                                            <h2 class="ui-title">Sửa địa chỉ</h2>
	                                                                                        </div>
	                                                                                        <div class="ui-modal__header-actions">
	                                                                                            <div class="ui-modal__close-button">
	                                                                                                <button class="ui-button ui-button--transparent close-modal" data-dismiss="modal" aria-label="Close modal" type="button" name="button">
	                                                                                                    <svg class="next-icon next-icon--color-ink-lighter next-icon--size-20">
	                                                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cancel-small-minor" />
	                                                                                                    </svg>
	                                                                                                </button>
	                                                                                            </div>
	                                                                                        </div>
	                                                                                    </div>
	                                                                                    <div class="ui-modal__body">
	                                                                                        <div class="ui-modal__section">
	                                                                                            <div class="ui-type-container">
	                                                                                                <div class="ui-form__section">
	                                                                                                    <div class="ui-form__group">
	                                                                                                        <div class="next-input-wrapper">
	                                                                                                            <label class="next-label" for="LastName">Họ</label>
	                                                                                                            <input class="next-input" id="LastName" name="LastName" placeholder="Nhập Họ" type="text" value="l&#234;" aria-invalid="false">
	                                                                                                        </div>
	                                                                                                        <div class="next-input-wrapper">
	                                                                                                            <label class="next-label" for="FirstName">T&#234;n</label>
	                                                                                                            <input class="next-input" id="FirstName" name="FirstName" placeholder="Nhập Tên" type="text" value="anh" aria-invalid="false">
	                                                                                                        </div>
	                                                                                                    </div>
	                                                                                                    <div class="ui-form__group">
	                                                                                                        <div class="next-input-wrapper">
	                                                                                                            <label class="next-label" for="Company">C&#244;ng ty</label>
	                                                                                                            <input class="next-input" id="Company" name="Company" placeholder="Nhập Công ty" type="text" value="" />
	                                                                                                        </div>
	                                                                                                        <div class="next-input-wrapper">
	                                                                                                            <label class="next-label" for="Phone">Số điện thoại</label>
	                                                                                                            <input class="next-input" id="Phone" name="Phone" placeholder="Nhập số điện thoại" type="text" value="" />
	                                                                                                        </div>
	                                                                                                    </div>
	                                                                                                    <div class="next-input-wrapper">
	                                                                                                        <label class="next-label" for="Address1">Địa chỉ</label>
	                                                                                                        <input class="next-input" id="Address1" name="Address1" placeholder="Nhập Địa chỉ" type="text" value="" />
	                                                                                                    </div>
	                                                                                                    <div class="ui-form__group">
	                                                                                                        <div class="ui-form__element">
	                                                                                                            <div class="next-input-wrapper">
	                                                                                                                <label class="next-label" for="CountryId">Quốc gia</label>
	                                                                        
	                                                                                                                <div class="ui-select__wrapper">
	                                                                                                                    <select bind="country_id" bind-event-change="changeCountry()" class="ui-select js-country-select" id="CountryId" name="CountryId" style="width: 100%;"><option value="86">Afghanistan</option>
	                                                                        <option value="87">Albania</option>
	                                                                        <option value="88">Algeria</option>
	                                                                        <option value="89">American Samoa</option>
	                                                                        <option value="90">Andorra</option>
	                                                                        <option value="91">Angola</option>
	                                                                        <option value="92">Anguilla</option>
	                                                                        <option value="93">Antarctica</option>
	                                                                        <option value="94">Antigua and Barbuda</option>
	                                                                        <option value="3">Argentina</option>
	                                                                        <option value="4">Armenia</option>
	                                                                        <option value="5">Aruba</option>
	                                                                        <option value="6">Australia</option>
	                                                                        <option value="7">Austria</option>
	                                                                        <option value="8">Azerbaijan</option>
	                                                                        <option value="9">Bahamas</option>
	                                                                        <option value="95">Bahrain</option>
	                                                                        <option value="10">Bangladesh</option>
	                                                                        <option value="96">Barbados</option>
	                                                                        <option value="11">Belarus</option>
	                                                                        <option value="12">Belgium</option>
	                                                                        <option value="13">Belize</option>
	                                                                        <option value="97">Benin</option>
	                                                                        <option value="14">Bermuda</option>
	                                                                        <option value="98">Bhutan</option>
	                                                                        <option value="15">Bolivia</option>
	                                                                        <option value="16">Bosnia and Herzegovina</option>
	                                                                        <option value="99">Botswana</option>
	                                                                        <option value="100">Bouvet Island</option>
	                                                                        <option value="17">Brazil</option>
	                                                                        <option value="101">British Indian Ocean Territory</option>
	                                                                        <option value="102">Brunei Darussalam</option>
	                                                                        <option value="18">Bulgaria</option>
	                                                                        <option value="103">Burkina Faso</option>
	                                                                        <option value="104">Burundi</option>
	                                                                        <option value="105">Cambodia</option>
	                                                                        <option value="106">Cameroon</option>
	                                                                        <option value="2">Canada</option>
	                                                                        <option value="107">Cape Verde</option>
	                                                                        <option value="19">Cayman Islands</option>
	                                                                        <option value="108">Central African Republic</option>
	                                                                        <option value="109">Chad</option>
	                                                                        <option value="20">Chile</option>
	                                                                        <option value="21">China</option>
	                                                                        <option value="110">Christmas Island</option>
	                                                                        <option value="111">Cocos (Keeling) Islands</option>
	                                                                        <option value="22">Colombia</option>
	                                                                        <option value="112">Comoros</option>
	                                                                        <option value="113">Congo</option>
	                                                                        <option value="114">Cook Islands</option>
	                                                                        <option value="23">Costa Rica</option>
	                                                                        <option value="24">Croatia</option>
	                                                                        <option value="25">Cuba</option>
	                                                                        <option value="26">Cyprus</option>
	                                                                        <option value="27">Czech Republic</option>
	                                                                        <option value="28">Denmark</option>
	                                                                        <option value="116">Djibouti</option>
	                                                                        <option value="117">Dominica</option>
	                                                                        <option value="29">Dominican Republic</option>
	                                                                        <option value="30">Ecuador</option>
	                                                                        <option value="31">Egypt</option>
	                                                                        <option value="118">El Salvador</option>
	                                                                        <option value="119">Equatorial Guinea</option>
	                                                                        <option value="120">Eritrea</option>
	                                                                        <option value="121">Estonia</option>
	                                                                        <option value="122">Ethiopia</option>
	                                                                        <option value="123">Falkland Islands (Malvinas)</option>
	                                                                        <option value="124">Faroe Islands</option>
	                                                                        <option value="125">Fiji</option>
	                                                                        <option value="32">Finland</option>
	                                                                        <option value="33">France</option>
	                                                                        <option value="126">French Guiana</option>
	                                                                        <option value="127">French Polynesia</option>
	                                                                        <option value="128">French Southern Territories</option>
	                                                                        <option value="129">Gabon</option>
	                                                                        <option value="130">Gambia</option>
	                                                                        <option value="34">Georgia</option>
	                                                                        <option value="35">Germany</option>
	                                                                        <option value="131">Ghana</option>
	                                                                        <option value="36">Gibraltar</option>
	                                                                        <option value="37">Greece</option>
	                                                                        <option value="132">Greenland</option>
	                                                                        <option value="133">Grenada</option>
	                                                                        <option value="134">Guadeloupe</option>
	                                                                        <option value="135">Guam</option>
	                                                                        <option value="38">Guatemala</option>
	                                                                        <option value="136">Guinea</option>
	                                                                        <option value="137">Guinea-bissau</option>
	                                                                        <option value="138">Guyana</option>
	                                                                        <option value="139">Haiti</option>
	                                                                        <option value="140">Heard and Mc Donald Islands</option>
	                                                                        <option value="141">Honduras</option>
	                                                                        <option value="39">Hong Kong</option>
	                                                                        <option value="40">Hungary</option>
	                                                                        <option value="142">Iceland</option>
	                                                                        <option value="41">India</option>
	                                                                        <option value="42">Indonesia</option>
	                                                                        <option value="143">Iran (Islamic Republic of)</option>
	                                                                        <option value="144">Iraq</option>
	                                                                        <option value="43">Ireland</option>
	                                                                        <option value="44">Israel</option>
	                                                                        <option value="45">Italy</option>
	                                                                        <option value="115">Ivory Coast</option>
	                                                                        <option value="46">Jamaica</option>
	                                                                        <option value="47">Japan</option>
	                                                                        <option value="48">Jordan</option>
	                                                                        <option value="49">Kazakhstan</option>
	                                                                        <option value="145">Kenya</option>
	                                                                        <option value="146">Kiribati</option>
	                                                                        <option value="147">Korea</option>
	                                                                        <option value="50">Korea, Democratic People&#39;s Republic of</option>
	                                                                        <option value="51">Kuwait</option>
	                                                                        <option value="148">Kyrgyzstan</option>
	                                                                        <option value="149">Lao People&#39;s Democratic Republic</option>
	                                                                        <option value="150">Latvia</option>
	                                                                        <option value="151">Lebanon</option>
	                                                                        <option value="152">Lesotho</option>
	                                                                        <option value="153">Liberia</option>
	                                                                        <option value="154">Libya</option>
	                                                                        <option value="155">Liechtenstein</option>
	                                                                        <option value="156">Lithuania</option>
	                                                                        <option value="157">Luxembourg</option>
	                                                                        <option value="158">Macau</option>
	                                                                        <option value="159">Macedonia</option>
	                                                                        <option value="160">Madagascar</option>
	                                                                        <option value="161">Malawi</option>
	                                                                        <option value="52">Malaysia</option>
	                                                                        <option value="162">Maldives</option>
	                                                                        <option value="163">Mali</option>
	                                                                        <option value="164">Malta</option>
	                                                                        <option value="165">Marshall Islands</option>
	                                                                        <option value="166">Martinique</option>
	                                                                        <option value="167">Mauritania</option>
	                                                                        <option value="168">Mauritius</option>
	                                                                        <option value="169">Mayotte</option>
	                                                                        <option value="53">Mexico</option>
	                                                                        <option value="170">Micronesia</option>
	                                                                        <option value="171">Moldova</option>
	                                                                        <option value="172">Monaco</option>
	                                                                        <option value="173">Mongolia</option>
	                                                                        <option value="174">Montenegro</option>
	                                                                        <option value="175">Montserrat</option>
	                                                                        <option value="176">Morocco</option>
	                                                                        <option value="177">Mozambique</option>
	                                                                        <option value="178">Myanmar</option>
	                                                                        <option value="179">Namibia</option>
	                                                                        <option value="180">Nauru</option>
	                                                                        <option value="181">Nepal</option>
	                                                                        <option value="54">Netherlands</option>
	                                                                        <option value="182">Netherlands Antilles</option>
	                                                                        <option value="183">New Caledonia</option>
	                                                                        <option value="55">New Zealand</option>
	                                                                        <option value="184">Nicaragua</option>
	                                                                        <option value="185">Niger</option>
	                                                                        <option value="186">Nigeria</option>
	                                                                        <option value="187">Niue</option>
	                                                                        <option value="188">Norfolk Island</option>
	                                                                        <option value="189">Northern Mariana Islands</option>
	                                                                        <option value="56">Norway</option>
	                                                                        <option value="190">Oman</option>
	                                                                        <option value="57">Pakistan</option>
	                                                                        <option value="191">Palau</option>
	                                                                        <option value="192">Panama</option>
	                                                                        <option value="193">Papua New Guinea</option>
	                                                                        <option value="58">Paraguay</option>
	                                                                        <option value="59">Peru</option>
	                                                                        <option value="60">Philippines</option>
	                                                                        <option value="194">Pitcairn</option>
	                                                                        <option value="61">Poland</option>
	                                                                        <option value="62">Portugal</option>
	                                                                        <option value="63">Puerto Rico</option>
	                                                                        <option value="64">Qatar</option>
	                                                                        <option value="195">Reunion</option>
	                                                                        <option value="65">Romania</option>
	                                                                        <option value="66">Russia</option>
	                                                                        <option value="196">Rwanda</option>
	                                                                        <option value="197">Saint Kitts and Nevis</option>
	                                                                        <option value="198">Saint Lucia</option>
	                                                                        <option value="199">Saint Vincent and the Grenadines</option>
	                                                                        <option value="200">Samoa</option>
	                                                                        <option value="201">San Marino</option>
	                                                                        <option value="202">Sao Tome and Principe</option>
	                                                                        <option value="67">Saudi Arabia</option>
	                                                                        <option value="203">Senegal</option>
	                                                                        <option value="85">Serbia</option>
	                                                                        <option value="204">Seychelles</option>
	                                                                        <option value="205">Sierra Leone</option>
	                                                                        <option value="68">Singapore</option>
	                                                                        <option value="69">Slovakia (Slovak Republic)</option>
	                                                                        <option value="70">Slovenia</option>
	                                                                        <option value="206">Solomon Islands</option>
	                                                                        <option value="207">Somalia</option>
	                                                                        <option value="71">South Africa</option>
	                                                                        <option value="208">South Georgia &amp; South Sandwich Islands</option>
	                                                                        <option value="72">Spain</option>
	                                                                        <option value="209">Sri Lanka</option>
	                                                                        <option value="210">St. Helena</option>
	                                                                        <option value="211">St. Pierre and Miquelon</option>
	                                                                        <option value="212">Sudan</option>
	                                                                        <option value="213">Suriname</option>
	                                                                        <option value="214">Svalbard and Jan Mayen</option>
	                                                                        <option value="215">Swaziland</option>
	                                                                        <option value="73">Sweden</option>
	                                                                        <option value="74">Switzerland</option>
	                                                                        <option value="216">Syrian Arab Republic</option>
	                                                                        <option value="75">Taiwan</option>
	                                                                        <option value="217">Tajikistan</option>
	                                                                        <option value="218">Tanzania</option>
	                                                                        <option value="76">Thailand</option>
	                                                                        <option value="219">Togo</option>
	                                                                        <option value="220">Tokelau</option>
	                                                                        <option value="221">Tonga</option>
	                                                                        <option value="222">Trinidad and Tobago</option>
	                                                                        <option value="223">Tunisia</option>
	                                                                        <option value="77">Turkey</option>
	                                                                        <option value="224">Turkmenistan</option>
	                                                                        <option value="225">Turks and Caicos Islands</option>
	                                                                        <option value="226">Tuvalu</option>
	                                                                        <option value="227">Uganda</option>
	                                                                        <option value="78">Ukraine</option>
	                                                                        <option value="79">United Arab Emirates</option>
	                                                                        <option value="80">United Kingdom</option>
	                                                                        <option value="1">United States</option>
	                                                                        <option value="81">United States minor outlying islands</option>
	                                                                        <option value="82">Uruguay</option>
	                                                                        <option value="83">Uzbekistan</option>
	                                                                        <option value="228">Vanuatu</option>
	                                                                        <option value="229">Vatican City State (Holy See)</option>
	                                                                        <option value="84">Venezuela</option>
	                                                                        <option selected="selected" value="230">Việt Nam</option>
	                                                                        <option value="231">Virgin Islands (British)</option>
	                                                                        <option value="232">Virgin Islands (U.S.)</option>
	                                                                        <option value="233">Wallis and Futuna Islands</option>
	                                                                        <option value="234">Western Sahara</option>
	                                                                        <option value="235">Yemen</option>
	                                                                        <option value="236">Zambia</option>
	                                                                        <option value="237">Zimbabwe</option>
	                                                                        </select>
	                                                                                                                    <svg class="next-icon next-icon--size-16">
	                                                                                                                        <use xlink:href="#select-chevron" />
	                                                                                                                    </svg>
	                                                                                                                </div>
	                                                                                                            </div>
	                                                                                                        </div>
	                                                                                                        <div class="ui-form__element">
	                                                                                                            <div class="next-input-wrapper">
	                                                                                                                <label class="next-label" for="Zip">Postal / Zip Code</label>
	                                                                                                                <input class="next-input" id="Zip" name="Zip" placeholder="Nhập Postal / Zip Code" type="text" value="" />
	                                                                                                            </div>
	                                                                                                        </div>
	                                                                                                    </div>
	                                                                                                    <div class="ui-form__group">
	                                                                                                        <div class="ui-form__element">
	                                                                                                            <div class="next-input-wrapper">
	                                                                                                                <label class="next-label" for="City">Tỉnh / Th&#224;nh phố</label>
	                                                                        
	                                                                                                                <div class="ui-select__wrapper" bind-show="have_city">
	                                                                                                                    <select bind="province_id" bind-disabled="!have_city" bind-event-change="changeProvince()" bind-show="have_city" class="ui-select js-province-select" id="provinceSlt" name="ProvinceId" style="width: 100%;"><option value="1">H&#224; Nội</option>
	                                                                        <option selected="selected" value="2">TP Hồ Ch&#237; Minh</option>
	                                                                        <option value="3">An Giang</option>
	                                                                        <option value="4">B&#224; Rịa-Vũng T&#224;u</option>
	                                                                        <option value="5">Bắc Giang</option>
	                                                                        <option value="6">Bắc Kạn</option>
	                                                                        <option value="7">Bạc Li&#234;u</option>
	                                                                        <option value="8">Bắc Ninh</option>
	                                                                        <option value="9">Bến Tre</option>
	                                                                        <option value="10">B&#236;nh Định</option>
	                                                                        <option value="11">B&#236;nh Dương</option>
	                                                                        <option value="12">B&#236;nh Phước</option>
	                                                                        <option value="13">B&#236;nh Thuận</option>
	                                                                        <option value="14">C&#224; Mau</option>
	                                                                        <option value="15">Cần Thơ</option>
	                                                                        <option value="16">Cao Bằng</option>
	                                                                        <option value="17">Đ&#224; Nẵng</option>
	                                                                        <option value="18">Đắk Lắk</option>
	                                                                        <option value="19">Đắk N&#244;ng</option>
	                                                                        <option value="20">Điện Bi&#234;n</option>
	                                                                        <option value="21">Đồng Nai</option>
	                                                                        <option value="22">Đồng Th&#225;p</option>
	                                                                        <option value="23">Gia Lai</option>
	                                                                        <option value="24">H&#224; Giang</option>
	                                                                        <option value="25">H&#224; Nam</option>
	                                                                        <option value="26">H&#224; Tĩnh</option>
	                                                                        <option value="27">Hải Dương</option>
	                                                                        <option value="28">Hải Ph&#242;ng</option>
	                                                                        <option value="29">Hậu Giang</option>
	                                                                        <option value="30">H&#242;a B&#236;nh</option>
	                                                                        <option value="31">Hưng Y&#234;n</option>
	                                                                        <option value="32">Kh&#225;nh H&#242;a</option>
	                                                                        <option value="33">Ki&#234;n Giang</option>
	                                                                        <option value="34">Kon Tum</option>
	                                                                        <option value="35">Lai Ch&#226;u</option>
	                                                                        <option value="36">L&#226;m Đồng</option>
	                                                                        <option value="37">Lạng Sơn</option>
	                                                                        <option value="38">L&#224;o Cai</option>
	                                                                        <option value="39">Long An</option>
	                                                                        <option value="40">Nam Định</option>
	                                                                        <option value="41">Nghệ An</option>
	                                                                        <option value="42">Ninh B&#236;nh</option>
	                                                                        <option value="43">Ninh Thuận</option>
	                                                                        <option value="44">Ph&#250; Thọ</option>
	                                                                        <option value="45">Ph&#250; Y&#234;n</option>
	                                                                        <option value="46">Quảng B&#236;nh</option>
	                                                                        <option value="47">Quảng Nam</option>
	                                                                        <option value="48">Quảng Ng&#227;i</option>
	                                                                        <option value="49">Quảng Ninh</option>
	                                                                        <option value="50">Quảng Trị</option>
	                                                                        <option value="51">S&#243;c Trăng</option>
	                                                                        <option value="52">Sơn La</option>
	                                                                        <option value="53">T&#226;y Ninh</option>
	                                                                        <option value="54">Th&#225;i B&#236;nh</option>
	                                                                        <option value="55">Th&#225;i Nguy&#234;n</option>
	                                                                        <option value="56">Thanh H&#243;a</option>
	                                                                        <option value="57">Thừa Thi&#234;n Huế</option>
	                                                                        <option value="58">Tiền Giang</option>
	                                                                        <option value="59">Tr&#224; Vinh</option>
	                                                                        <option value="60">Tuy&#234;n Quang</option>
	                                                                        <option value="61">Vĩnh Long</option>
	                                                                        <option value="62">Vĩnh Ph&#250;c</option>
	                                                                        <option value="63">Y&#234;n B&#225;i</option>
	                                                                        </select>
	                                                                                                                    <svg class="next-icon next-icon--size-16" bind-show="have_city">
	                                                                                                                        <use xlink:href="#select-chevron" />
	                                                                                                                    </svg>
	                                                                                                                </div>
	                                                                                                                <input bind-disabled="have_city" bind-show="!have_city" class="next-input hide" id="City" name="City" placeholder="Nhập Tỉnh / Thành phố" type="text" value="TP Hồ Chí Minh" />
	                                                                                                            </div>
	                                                                                                        </div>
	                                                                                                        <div class="ui-form__element">
	                                                                                                            <div class="next-input-wrapper">
	                                                                                                                <div bind-show="have_city">
	                                                                                                                    <label class="next-label" for="DistrictId">Quận huyện</label>
	                                                                                                                    <div class="ui-select__wrapper">
	                                                                                                                        <select bind-disabled="!have_city" class="ui-select js-district-select" id="districtSlt" name="DistrictId" style="width: 100%;"><option selected="selected" value="30">Quận 1</option>
	                                                                        <option value="31">Quận 2</option>
	                                                                        <option value="32">Quận 3</option>
	                                                                        <option value="33">Quận 4</option>
	                                                                        <option value="34">Quận 5</option>
	                                                                        <option value="35">Quận 6</option>
	                                                                        <option value="36">Quận 7</option>
	                                                                        <option value="37">Quận 8</option>
	                                                                        <option value="38">Quận 9</option>
	                                                                        <option value="39">Quận 10</option>
	                                                                        <option value="40">Quận 11</option>
	                                                                        <option value="41">Quận 12</option>
	                                                                        <option value="42">Quận G&#242; Vấp</option>
	                                                                        <option value="43">Quận T&#226;n B&#236;nh</option>
	                                                                        <option value="44">Quận T&#226;n Ph&#250;</option>
	                                                                        <option value="45">Quận B&#236;nh Thạnh</option>
	                                                                        <option value="46">Quận Ph&#250; Nhuận</option>
	                                                                        <option value="47">Quận Thủ Đức</option>
	                                                                        <option value="48">Quận B&#236;nh T&#226;n</option>
	                                                                        <option value="49">Huyện Củ Chi</option>
	                                                                        <option value="50">Huyện H&#243;c M&#244;n</option>
	                                                                        <option value="51">Huyện B&#236;nh Ch&#225;nh</option>
	                                                                        <option value="52">Huyện Nh&#224; B&#232;</option>
	                                                                        <option value="53">Huyện Cần Giờ</option>
	                                                                        </select>
	                                                                                                                        <svg class="next-icon next-icon--size-16">
	                                                                                                                            <use xlink:href="#select-chevron" />
	                                                                                                                        </svg>
	                                                                                                                    </div>
	                                                                                                                </div>
	                                                                                                            </div>
	                                                                                                        </div>
	                                                                                                    </div>
	                                                                                                </div>
	                                                                                            </div>
	                                                                                        </div>
	                                                                                    </div>
	                                                                                    <div class="ui-modal__footer">
	                                                                                        <div class="ui-modal__footer-actions">
	                                                                                            <div class="ui-modal__secondary-actions">
	                                                                                                <div class="button-group">
	                                                                                                    <button class="ui-button close-modal" data-dismiss="modal" type="button" name="button">Hủy</button>
	                                                                                                </div>
	                                                                                            </div>
	                                                                                            <div class="ui-modal__primary-actions">
	                                                                                                <div class="button-group button-group--right-aligned">
	                                                                                                    <button class="btn js-btn-loadable js-btn-primary has-loading" type="submit" name="button">Lưu</button>
	                                                                                                </div>
	                                                                                            </div>
	                                                                                        </div>
	                                                                                    </div>
	                                                                                </div>
	                                                                            </div>
	                                                                        </form>
	                                                                     </script>
	                                                                     <button class="ui-button btn--link" bind-event-click="editAddressModal_10398624.show()" type="button" name="button">Sửa</button>
	                                                                  </div>
	                                                               </div>
	                                                            </div>
	                                                         </div>
	                                                      </div>
	                                                   </div>
	                                                </div>
	                                             </div>
	                                          </div>
	                                       </div>
	                                    </div>
	                                    <p class="type--subdued word_break__content">lê anh</p>
	                                    <p class="type--subdued word_break__content"></p>
	                                    <p class="type--subdued">Quận 1</p>
	                                    <p class="type--subdued word_break__content">TP Hồ Chí Minh<span> </span></p>
	                                    <p class="type--subdued">Việt Nam</p>
	                                 </section>
	                              </aside>
	                           </div>
	                           <div class="ui-layout__item">
	                              <div class="next-card next-card--aside">
	                                 <header class="next-card__header">
	                                    <div class="wrappable wrappable--half-spacing wrappable--vertically-centered">
	                                       <div class="wrappable__item">
	                                          <h3 class="ui-heading">
	                                             Tag
	                                          </h3>
	                                       </div>
	                                       <div class="wrappable__item wrappable__item--no-flex">
	                                          <button class="ui-button btn--link" bind-event-click="tagDisplay.showAllTagsModal.show()" type="button" name="button">Toàn bộ tag</button>
	                                       </div>
	                                    </div>
	                                 </header>
	                                {{--  <script type="text/template" id="display-one-tag" data-input-name="Tags[]">
	                                    <li class="next-token">
	                                        <span class="next-token__label">{{= tag }}</span>
	                                    
	                                        <a bind-event-click="removeTag(this)" class="next-token__remove">
	                                            <input type="hidden" name="{{= inputName }}" value="{{= tag }}">
	                                            <span class="next-token__remove__icon">
	                                                <svg class="next-icon next-icon--size-10 next-icon--no-nudge">
	                                                    <use xlink:href="#next-remove"></use>
	                                                </svg>
	                                            </span>
	                                        </a>
	                                    </li>
	                                 </script> --}}
	                               {{--   <section class="next-card__section">
	                                    <div define="{tagDisplay: new Bizweb.TagDisplay(this)}" context="tagDisplay" id="tags-event-bus">
	                                       <a href="/admin/customers/tags" data-define="{showAllTagsModal: new Bizweb.Modal(this)}" type="text/html" class="modal_source" style="display: none"></a>
	                                       <script type="text/template" id="autocomplete-header-suggestion-template">
	                                          <li>
	                                              <a class="next-list__item next-list__item--promoted-action"
	                                                 selectable
	                                                 data-bind-event-click="selectHeader(event)">
	                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
	                                                      <div class="next-grid__cell next-grid__cell--no-flex next-grid__cell--vertically-centered">
	                                                          <svg style="overflow: visible; /* fixes clipping in Chrome */" class="next-icon next-icon--size-12 next-icon next-icon--12 block"> <use xlink:href="#next-add-circle" /> </svg>
	                                                      </div>
	                                                      <div class="next-grid__cell">
	                                                          <strong>Add</strong> {{= input }}
	                                                      </div>
	                                                  </div>
	                                              </a>
	                                          </li>
	                                       </script>
	                                       <script type="text/template" id="autocomplete-suggestions-list-template">
	                                          {{ _.each(suggestions, function(suggestion, index) { }}
	                                          <li>
	                                              <a class="next-list__item"
	                                                 selectable
	                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(index + indexStart) }}, event)"
	                                                 data-bind-class="{ 'next-list__item--is-applied': isApplied({{= JSON.stringify(index + indexStart) }}) }">
	                                                  {{= suggestion }}
	                                              </a>
	                                          </li>
	                                          {{ }) }}
	                                       </script>
	                                       <script type="text/template" id="autocomplete-no-suggestions-template">
	                                          <li>
	                                              <div class="ui-popover__section">
	                                                  <p class="type--subdued tc">No results found</p>
	                                              </div>
	                                          </li>
	                                       </script>
	                                       <script type="text/template" id="country-autocomplete-list-template">
	                                          {{ _.each(suggestions, function(suggestion, index) { }}
	                                          <li>
	                                              <a class="next-list__item"
	                                                 selectable
	                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(index + indexStart) }}, event)"
	                                                 data-bind-class="{ 'next-list__item--is-applied': isApplied({{= JSON.stringify(index + indexStart) }}) }">
	                                                  {{= suggestion.name }}
	                                              </a>
	                                          </li>
	                                          {{ }) }}
	                                       </script>
	                                       <script type="text/template" id="autocomplete-v2-suggestions-list-with-toggle-template">
	                                          {{ _.each(suggestions, function(suggestion, index) { }}
	                                          <li>
	                                              <a class="next-list__item"
	                                                 selectable
	                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(_.pick(suggestion, [displayKey, detailsKey, secondaryDetailsKey, 'id'])) }}, event)"
	                                                 data-bind-class="{ 'next-list__item--is-applied': isApplied({{= JSON.stringify(suggestion.id) }}) }">
	                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
	                                                      <div class="next-grid__cell next-grid__cell--no-flex">
	                                                          <svg class="next-icon next-icon--size-12"> <use xlink:href="#next-checkmark" /> </svg>
	                                                      </div>
	                                                      <div class="next-grid__cell">
	                                                          {{= suggestion[displayKey] }}
	                                          
	                                                          {{ if(detailsKey || secondaryDetailsKey) { }}
	                                                          <span class="type--subdued"> {{= suggestion[detailsKey] || suggestion[secondaryDetailsKey]}}</span>
	                                                          {{ } }}
	                                                      </div>
	                                                  </div>
	                                              </a>
	                                          </li>
	                                          {{ }) }}
	                                       </script>
	                                       <script type="text/template" id="autocomplete-v2-no-results-template">
	                                          {{ if (input) { }}
	                                          <li>
	                                              <a class="next-list__item next-list__item--promoted-action"
	                                                 selectable
	                                                 data-bind-event-click="selectHeader(event)">
	                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
	                                                      <div class="next-grid__cell next-grid__cell--no-flex next-grid__cell--vertically-centered">
	                                                          <svg style="overflow: visible; /* fixes clipping in Chrome */" class="next-icon next-icon--size-12 next-icon next-icon--12 block"> <use xlink:href="#next-add-circle" /> </svg>
	                                                      </div>
	                                                      <div class="next-grid__cell">
	                                                          <strong>Add</strong> {{= input }}
	                                                      </div>
	                                                  </div>
	                                              </a>
	                                          </li>
	                                          
	                                          {{ } }}
	                                       </script>
	                                       <script type="text/template" id="autocomplete-v2-results-template">
	                                          {{ if (firstPage && !hasMatchingSuggestion && input) { }}
	                                          <li>
	                                              <a class="next-list__item next-list__item--promoted-action"
	                                                 selectable
	                                                 data-bind-event-click="selectHeader(event)">
	                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
	                                                      <div class="next-grid__cell next-grid__cell--no-flex next-grid__cell--vertically-centered">
	                                                          <svg style="overflow: visible; "class="next-icon next-icon--size-12 next-icon next-icon--12 block"> <use xlink:href="#next-add-circle" /> </svg>
	                                                      </div>
	                                                      <div class="next-grid__cell">
	                                                          <strong>Add</strong> {{= input }}
	                                                      </div>
	                                                  </div>
	                                              </a>
	                                          </li>
	                                          
	                                          {{ } }}
	                                          
	                                          {{ _.each(suggestions, function(suggestion) { }}
	                                          <li>
	                                              <a class="next-list__item"
	                                                 selectable
	                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(suggestion) }}, event)">
	                                                  {{= suggestion }}
	                                              </a>
	                                          </li>
	                                          {{ }) }}
	                                       </script>
	                                       <script type="text/template" id="autocomplete-v2-tag-results-template">
	                                          {{ if (firstPage && !input) { }}
	                                          <li>
	                                              <h3 class="next-heading next-heading--micro-uppercase-bordered">Tag gần đây</h3>
	                                          </li>
	                                          {{ } }}
	                                          {{ if (firstPage && !hasMatchingSuggestion && input) { }}
	                                          <li>
	                                              <a class="next-list__item next-list__item--promoted-action"
	                                                 selectable
	                                                 data-bind-event-click="selectHeader(event)">
	                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
	                                                      <div class="next-grid__cell next-grid__cell--no-flex next-grid__cell--vertically-centered">
	                                                          <svg style="overflow: visible; "class="next-icon next-icon--size-12 next-icon next-icon--12 block"> <use xlink:href="#next-add-circle" /> </svg>
	                                                      </div>
	                                                      <div class="next-grid__cell">
	                                                          <strong>Add</strong> {{= input }}
	                                                      </div>
	                                                  </div>
	                                              </a>
	                                          </li>
	                                          
	                                          {{ } }}
	                                          
	                                          {{ _.each(suggestions, function(suggestion) { }}
	                                          <li>
	                                              <a class="next-list__item"
	                                                 selectable
	                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(suggestion) }}, event)">
	                                                  {{= suggestion }}
	                                              </a>
	                                          </li>
	                                          {{ }) }}
	                                       </script>
	                                       <div class="ui-popover__container ui-popover__container--full-width-container" data-context="tagsAutocomplete" data-define="{ 'tagsAutocomplete': new Bizweb.AutocompleteV2.TagsAutocomplete(this, &quot;\/admin\/customers\/tags&quot;, $node, {&quot;values&quot;:[],&quot;maxLengthError&quot;:&quot;Tag vượt quá 249 kí tự&quot;,&quot;maxLength&quot;:249,&quot;duplicateError&quot;:&quot;Bạn đã sử dụng tag \&quot;%s\&quot; rồi&quot;}) }">
	                                          <div>
	                                             <div class="">
	                                                <input class="next-input js-no-dirty" placeholder="VIP, thân thiết, quay lại" id="tags" data-bind="input" data-bind-event-keyup="inputChanged()" size="30" type="text" aria-expanded="false" aria-haspopup="false" aria-owns="popover-dropdown-14" aria-controls="popover-announce-selected" role="combobox" aria-autocomplete="list" aria-activedescendant="selected-option-5">
	                                             </div>
	                                             <div class="tooltip-error attached-to-bottom hide" data-bind-show="errorMessage">
	                                                <ul class="error-list">
	                                                   <li data-bind="errorMessage"></li>
	                                                </ul>
	                                             </div>
	                                          </div>
	                                          <div class="ui-popover ui-popover--full-width ui-popover--no-focus" data-popover-horizontally-relative-to-closest=".next-card" data-popover-css-vertical-margin="13" data-popover-css-horizontal-margin="0" data-popover-css-max-height="300" data-popover-css-max-width="10000" id="ui-popover--7" aria-labelledby="tags" aria-expanded="false" role="dialog" data-popover-preferred-position="top" style="margin-right: 0px; margin-left: 0px;">
	                                             <div class="ui-popover__tooltip"></div>
	                                             <div class="ui-popover__content-wrapper">
	                                                <div class="ui-popover__content">
	                                                   <div class="ui-popover__pane">
	                                                      <ul class="js-autocomplete-suggestions next-list next-list--compact next-list--toggles" role="listbox" id="popover-dropdown-14"></ul>
	                                                      <div class="ui-popover__section hide" data-bind-show="loadingResults">
	                                                         <p class="type--subdued tc">
	                                                            <span class="next-spinner">
	                                                               <svg class="next-icon next-icon--size-16">
	                                                                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-spinner"></use>
	                                                               </svg>
	                                                            </span>
	                                                            Loading…
	                                                         </p>
	                                                      </div>
	                                                   </div>
	                                                </div>
	                                             </div>
	                                          </div>
	                                       </div>
	                                       <input type="hidden" name="Tags[]" bind-disabled="hasTags()">
	                                       <ul class="js-tag-list next-token-list st"></ul>
	                                    </div>
	                                 </section> --}}
	                              </div>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	                  <div class="ui-page-actions  ui-page-actions--has-secondary">
	                     <div class="ui-page-actions__container">
	                        <div class="ui-page-actions__actions ui-page-actions__actions--secondary">
	                           <div class="ui-page-actions__button-group">
	                              <button class="ui-button btn-destroy" bind-event-click="deleteCustomerModal.show()" href="#" type="button" name="button">Xóa khách hàng</button>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	                  <script class="modal_source" define="{deleteCustomerModal: new Bizweb.Modal(this)}" type="text/html">
	                     <form method="post" define="{deleteCustomer: new Bizweb.Forms(this, {disabledUntilDirty: true, ajax: true, disableContextBar: true })}" action="/admin/customers/delete" tg-remote="delete">
	                         <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCTENgLZOTcYuqakF+qXYqbmiLGY9B9VrHJ6ElQ3aDj9JAEmOqFS96B4MrV4vdqY6Pg==" />
	                         <input type="hidden" name="returnUrl" value="/admin/customers/8535637" />
	                         <input type="hidden" name="id" value="8535637" />
	                         <div class="modal-dialog">
	                             <div class="modal-content">
	                                 <div class="ui-modal__header">
	                                     <div class="ui-modal__header-title">
	                                         <h2 class="ui-title">Xóa khách hàng</h2>
	                                     </div>
	                                     <div class="ui-modal__header-actions">
	                                         <div class="ui-modal__close-button">
	                                             <button class="ui-button ui-button--transparent close-modal" data-dismiss="modal" aria-label="Close modal" type="button" name="button">
	                                                 <svg class="next-icon next-icon--color-ink-lighter next-icon--size-20">
	                                                     <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cancel-small-minor" />
	                                                 </svg>
	                                             </button>
	                                         </div>
	                                     </div>
	                                 </div>
	                                 <div class="ui-modal__body">
	                                     <div class="ui-modal__section">
	                                         <p>Bạn có chắc muốn xóa khách hàng <strong>l&#234; anh</strong>? Thao tác này không thể phục hồi!</p>
	                                     </div>
	                                 </div>
	                                 <div class="ui-modal__footer">
	                                     <div class="ui-modal__footer-actions">
	                                         <div class="ui-modal__secondary-actions">
	                                             <div class="button-group">
	                                                 <button class="ui-button close-modal" data-dismiss="modal" type="button" name="button">Hủy</button>
	                                             </div>
	                                         </div>
	                                         <div class="ui-modal__primary-actions">
	                                             <div class="button-group button-group--right-aligned">
	                                                 <button class="ui-button btn-destroy-no-hover js-btn-loadable has-loading btn-delete-bulk-action" type="submit" name="commit">Xóa</button>
	                                             </div>
	                                         </div>
	                                     </div>
	                                 </div>
	                             </div>
	                         </div>
	                     </form>
	                  </script>
	               </form>
	               <div class="ui-footer-help">
	                  <div class="ui-footer-help__content">
	                     <div class="ui-footer-help__icon">
	                        <svg class="next-icon next-icon--size-24 next-icon--no-nudge" role="img" aria-labelledby="next-help-circle-title">
	                           <title id="next-help-circle-title">Help</title>
	                           <use xlink:href="#next-help-circle"></use>
	                        </svg>
	                     </div>
	                     <div>
	                        <p>
	                           Bạn có thể xem thêm hướng dẫn
	                           <a rel="noreferrer noopener" target="_blank" href="https://web-docs.sapo.vn/thong-tin-khach-hang-33.html?utm_source=admin&amp;utm_medium=customers&amp;utm_campaign=docs">tại đây</a>
	                        </p>
	                     </div>
	                  </div>
	               </div>
	               <script class="modal_source" define="{addAddressModal: new Bizweb.Modal(this)}" type="text/html">
	                  <form define="{ formCreateAddress: new Bizweb.Forms(this, {disabledUntilDirty: true, alertUnsavedChanges: false, disableContextBar: true })}" action="/admin/customers/addaddress?customerId=8535637" method="post" autocomplete="off" accept-charset="UTF-8" tg-remote="true" refresh-on-error="edit_customer_address" refresh-on-success="modal customer-address customer-card" refresh="edit_customer_address" id="edit_customer_address">
	                  <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCTENgLZOTcYuqakF+qXYqbmiLGY9B9VrHJ6ElQ3aDj9JAEmOqFS96B4MrV4vdqY6Pg==" />
	                  <div define="{customerAddressCreate: new Bizweb.CustomerAddress(this,{&quot;district_slt&quot;:&quot;#districtSlt&quot;,&quot;province_slt&quot;:&quot;#provinceSlt&quot;,&quot;have_city&quot;:true})}" context="customerAddressCreate" class="modal-dialog">
	                  <div class="modal-content">
	                      <div class="ui-modal__header">
	                          <div class="ui-modal__header-title">
	                              <h2 class="ui-title">Thêm địa chỉ</h2>
	                          </div>
	                          <div class="ui-modal__header-actions">
	                              <div class="ui-modal__close-button">
	                                  <button class="ui-button ui-button--transparent close-modal" data-dismiss="modal" aria-label="Close modal" type="button" name="button">
	                                      <svg class="next-icon next-icon--color-ink-lighter next-icon--size-20">
	                                          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cancel-small-minor" />
	                                      </svg>
	                                  </button>
	                              </div>
	                          </div>
	                      </div>
	                      <div class="ui-modal__body">
	                          <div class="ui-modal__section">
	                              <div class="ui-type-container">
	                                  <div class="ui-form__section">
	                                      <div class="ui-form__group">
	                                          <div class="next-input-wrapper">
	                                              <label class="next-label" for="LastName">Họ</label>
	                                              <input class="next-input" data-val="true" data-val-length="Họ tối đa 50 ký tự" data-val-length-max="50" id="LastName" name="LastName" placeholder="Nhập Họ" type="text" value="" />
	                                          </div>
	                                          <div class="next-input-wrapper">
	                                              <label class="next-label" for="FirstName">T&#234;n</label>
	                                              <input class="next-input" data-val="true" data-val-length="Tên tối đa 50 ký tự" data-val-length-max="50" id="FirstName" name="FirstName" placeholder="Nhập Tên" type="text" value="" />
	                                          </div>
	                                      </div>
	                                      <div class="ui-form__group">
	                                          <div class="next-input-wrapper">
	                                              <label class="next-label" for="Company">C&#244;ng ty</label>
	                                              <input class="next-input" data-val="true" data-val-length="Công ty tối đa 50 ký tự" data-val-length-max="50" id="Company" name="Company" placeholder="Nhập Công ty" type="text" value="" />
	                                          </div>
	                                          <div class="next-input-wrapper">
	                                              <label class="next-label" for="Phone">Số điện thoại</label>
	                                              <input class="next-input" data-val="true" data-val-length="Điện thoại tối đa 15 ký tự" data-val-length-max="15" data-val-phone="Điện thoại không đúng định dạng" id="Phone" name="Phone" placeholder="Nhập số điện thoại" type="text" value="" />
	                                          </div>
	                                      </div>
	                                      <div class="next-input-wrapper">
	                                          <label class="next-label" for="Address1">Địa chỉ</label>
	                                          <input class="next-input" data-val="true" data-val-length="Địa chỉ tối đa 255 ký tự" data-val-length-max="255" id="Address1" name="Address1" placeholder="Nhập Địa chỉ" type="text" value="" />
	                                      </div>
	                                      <div class="ui-form__group">
	                                          <div class="ui-form__element">
	                                              <div class="next-input-wrapper">
	                                                  <label class="next-label" for="CountryId">Quốc gia</label>
	                  
	                                                  <div class="ui-select__wrapper">
	                                                      <select bind="country_id" bind-event-change="changeCountry()" class="ui-select js-country-select" data-val="true" data-val-number="The field Quốc gia must be a number." id="CountryId" name="CountryId" style="width: 100%;"><option value="86">Afghanistan</option>
	                  <option value="87">Albania</option>
	                  <option value="88">Algeria</option>
	                  <option value="89">American Samoa</option>
	                  <option value="90">Andorra</option>
	                  <option value="91">Angola</option>
	                  <option value="92">Anguilla</option>
	                  <option value="93">Antarctica</option>
	                  <option value="94">Antigua and Barbuda</option>
	                  <option value="3">Argentina</option>
	                  <option value="4">Armenia</option>
	                  <option value="5">Aruba</option>
	                  <option value="6">Australia</option>
	                  <option value="7">Austria</option>
	                  <option value="8">Azerbaijan</option>
	                  <option value="9">Bahamas</option>
	                  <option value="95">Bahrain</option>
	                  <option value="10">Bangladesh</option>
	                  <option value="96">Barbados</option>
	                  <option value="11">Belarus</option>
	                  <option value="12">Belgium</option>
	                  <option value="13">Belize</option>
	                  <option value="97">Benin</option>
	                  <option value="14">Bermuda</option>
	                  <option value="98">Bhutan</option>
	                  <option value="15">Bolivia</option>
	                  <option value="16">Bosnia and Herzegovina</option>
	                  <option value="99">Botswana</option>
	                  <option value="100">Bouvet Island</option>
	                  <option value="17">Brazil</option>
	                  <option value="101">British Indian Ocean Territory</option>
	                  <option value="102">Brunei Darussalam</option>
	                  <option value="18">Bulgaria</option>
	                  <option value="103">Burkina Faso</option>
	                  <option value="104">Burundi</option>
	                  <option value="105">Cambodia</option>
	                  <option value="106">Cameroon</option>
	                  <option value="2">Canada</option>
	                  <option value="107">Cape Verde</option>
	                  <option value="19">Cayman Islands</option>
	                  <option value="108">Central African Republic</option>
	                  <option value="109">Chad</option>
	                  <option value="20">Chile</option>
	                  <option value="21">China</option>
	                  <option value="110">Christmas Island</option>
	                  <option value="111">Cocos (Keeling) Islands</option>
	                  <option value="22">Colombia</option>
	                  <option value="112">Comoros</option>
	                  <option value="113">Congo</option>
	                  <option value="114">Cook Islands</option>
	                  <option value="23">Costa Rica</option>
	                  <option value="24">Croatia</option>
	                  <option value="25">Cuba</option>
	                  <option value="26">Cyprus</option>
	                  <option value="27">Czech Republic</option>
	                  <option value="28">Denmark</option>
	                  <option value="116">Djibouti</option>
	                  <option value="117">Dominica</option>
	                  <option value="29">Dominican Republic</option>
	                  <option value="30">Ecuador</option>
	                  <option value="31">Egypt</option>
	                  <option value="118">El Salvador</option>
	                  <option value="119">Equatorial Guinea</option>
	                  <option value="120">Eritrea</option>
	                  <option value="121">Estonia</option>
	                  <option value="122">Ethiopia</option>
	                  <option value="123">Falkland Islands (Malvinas)</option>
	                  <option value="124">Faroe Islands</option>
	                  <option value="125">Fiji</option>
	                  <option value="32">Finland</option>
	                  <option value="33">France</option>
	                  <option value="126">French Guiana</option>
	                  <option value="127">French Polynesia</option>
	                  <option value="128">French Southern Territories</option>
	                  <option value="129">Gabon</option>
	                  <option value="130">Gambia</option>
	                  <option value="34">Georgia</option>
	                  <option value="35">Germany</option>
	                  <option value="131">Ghana</option>
	                  <option value="36">Gibraltar</option>
	                  <option value="37">Greece</option>
	                  <option value="132">Greenland</option>
	                  <option value="133">Grenada</option>
	                  <option value="134">Guadeloupe</option>
	                  <option value="135">Guam</option>
	                  <option value="38">Guatemala</option>
	                  <option value="136">Guinea</option>
	                  <option value="137">Guinea-bissau</option>
	                  <option value="138">Guyana</option>
	                  <option value="139">Haiti</option>
	                  <option value="140">Heard and Mc Donald Islands</option>
	                  <option value="141">Honduras</option>
	                  <option value="39">Hong Kong</option>
	                  <option value="40">Hungary</option>
	                  <option value="142">Iceland</option>
	                  <option value="41">India</option>
	                  <option value="42">Indonesia</option>
	                  <option value="143">Iran (Islamic Republic of)</option>
	                  <option value="144">Iraq</option>
	                  <option value="43">Ireland</option>
	                  <option value="44">Israel</option>
	                  <option value="45">Italy</option>
	                  <option value="115">Ivory Coast</option>
	                  <option value="46">Jamaica</option>
	                  <option value="47">Japan</option>
	                  <option value="48">Jordan</option>
	                  <option value="49">Kazakhstan</option>
	                  <option value="145">Kenya</option>
	                  <option value="146">Kiribati</option>
	                  <option value="147">Korea</option>
	                  <option value="50">Korea, Democratic People&#39;s Republic of</option>
	                  <option value="51">Kuwait</option>
	                  <option value="148">Kyrgyzstan</option>
	                  <option value="149">Lao People&#39;s Democratic Republic</option>
	                  <option value="150">Latvia</option>
	                  <option value="151">Lebanon</option>
	                  <option value="152">Lesotho</option>
	                  <option value="153">Liberia</option>
	                  <option value="154">Libya</option>
	                  <option value="155">Liechtenstein</option>
	                  <option value="156">Lithuania</option>
	                  <option value="157">Luxembourg</option>
	                  <option value="158">Macau</option>
	                  <option value="159">Macedonia</option>
	                  <option value="160">Madagascar</option>
	                  <option value="161">Malawi</option>
	                  <option value="52">Malaysia</option>
	                  <option value="162">Maldives</option>
	                  <option value="163">Mali</option>
	                  <option value="164">Malta</option>
	                  <option value="165">Marshall Islands</option>
	                  <option value="166">Martinique</option>
	                  <option value="167">Mauritania</option>
	                  <option value="168">Mauritius</option>
	                  <option value="169">Mayotte</option>
	                  <option value="53">Mexico</option>
	                  <option value="170">Micronesia</option>
	                  <option value="171">Moldova</option>
	                  <option value="172">Monaco</option>
	                  <option value="173">Mongolia</option>
	                  <option value="174">Montenegro</option>
	                  <option value="175">Montserrat</option>
	                  <option value="176">Morocco</option>
	                  <option value="177">Mozambique</option>
	                  <option value="178">Myanmar</option>
	                  <option value="179">Namibia</option>
	                  <option value="180">Nauru</option>
	                  <option value="181">Nepal</option>
	                  <option value="54">Netherlands</option>
	                  <option value="182">Netherlands Antilles</option>
	                  <option value="183">New Caledonia</option>
	                  <option value="55">New Zealand</option>
	                  <option value="184">Nicaragua</option>
	                  <option value="185">Niger</option>
	                  <option value="186">Nigeria</option>
	                  <option value="187">Niue</option>
	                  <option value="188">Norfolk Island</option>
	                  <option value="189">Northern Mariana Islands</option>
	                  <option value="56">Norway</option>
	                  <option value="190">Oman</option>
	                  <option value="57">Pakistan</option>
	                  <option value="191">Palau</option>
	                  <option value="192">Panama</option>
	                  <option value="193">Papua New Guinea</option>
	                  <option value="58">Paraguay</option>
	                  <option value="59">Peru</option>
	                  <option value="60">Philippines</option>
	                  <option value="194">Pitcairn</option>
	                  <option value="61">Poland</option>
	                  <option value="62">Portugal</option>
	                  <option value="63">Puerto Rico</option>
	                  <option value="64">Qatar</option>
	                  <option value="195">Reunion</option>
	                  <option value="65">Romania</option>
	                  <option value="66">Russia</option>
	                  <option value="196">Rwanda</option>
	                  <option value="197">Saint Kitts and Nevis</option>
	                  <option value="198">Saint Lucia</option>
	                  <option value="199">Saint Vincent and the Grenadines</option>
	                  <option value="200">Samoa</option>
	                  <option value="201">San Marino</option>
	                  <option value="202">Sao Tome and Principe</option>
	                  <option value="67">Saudi Arabia</option>
	                  <option value="203">Senegal</option>
	                  <option value="85">Serbia</option>
	                  <option value="204">Seychelles</option>
	                  <option value="205">Sierra Leone</option>
	                  <option value="68">Singapore</option>
	                  <option value="69">Slovakia (Slovak Republic)</option>
	                  <option value="70">Slovenia</option>
	                  <option value="206">Solomon Islands</option>
	                  <option value="207">Somalia</option>
	                  <option value="71">South Africa</option>
	                  <option value="208">South Georgia &amp; South Sandwich Islands</option>
	                  <option value="72">Spain</option>
	                  <option value="209">Sri Lanka</option>
	                  <option value="210">St. Helena</option>
	                  <option value="211">St. Pierre and Miquelon</option>
	                  <option value="212">Sudan</option>
	                  <option value="213">Suriname</option>
	                  <option value="214">Svalbard and Jan Mayen</option>
	                  <option value="215">Swaziland</option>
	                  <option value="73">Sweden</option>
	                  <option value="74">Switzerland</option>
	                  <option value="216">Syrian Arab Republic</option>
	                  <option value="75">Taiwan</option>
	                  <option value="217">Tajikistan</option>
	                  <option value="218">Tanzania</option>
	                  <option value="76">Thailand</option>
	                  <option value="219">Togo</option>
	                  <option value="220">Tokelau</option>
	                  <option value="221">Tonga</option>
	                  <option value="222">Trinidad and Tobago</option>
	                  <option value="223">Tunisia</option>
	                  <option value="77">Turkey</option>
	                  <option value="224">Turkmenistan</option>
	                  <option value="225">Turks and Caicos Islands</option>
	                  <option value="226">Tuvalu</option>
	                  <option value="227">Uganda</option>
	                  <option value="78">Ukraine</option>
	                  <option value="79">United Arab Emirates</option>
	                  <option value="80">United Kingdom</option>
	                  <option value="1">United States</option>
	                  <option value="81">United States minor outlying islands</option>
	                  <option value="82">Uruguay</option>
	                  <option value="83">Uzbekistan</option>
	                  <option value="228">Vanuatu</option>
	                  <option value="229">Vatican City State (Holy See)</option>
	                  <option value="84">Venezuela</option>
	                  <option selected="selected" value="230">Việt Nam</option>
	                  <option value="231">Virgin Islands (British)</option>
	                  <option value="232">Virgin Islands (U.S.)</option>
	                  <option value="233">Wallis and Futuna Islands</option>
	                  <option value="234">Western Sahara</option>
	                  <option value="235">Yemen</option>
	                  <option value="236">Zambia</option>
	                  <option value="237">Zimbabwe</option>
	                  </select>
	                                                      <svg class="next-icon next-icon--size-16">
	                                                          <use xlink:href="#select-chevron" />
	                                                      </svg>
	                                                  </div>
	                                              </div>
	                                          </div>
	                                          <div class="ui-form__element">
	                                              <div class="next-input-wrapper">
	                                                  <label class="next-label" for="Zip">Postal / Zip Code</label>
	                                                  <input class="next-input" data-val="true" data-val-length="Postal / Zip Code tối đa 20 ký tự" data-val-length-max="20" id="Zip" name="Zip" placeholder="Nhập Postal / Zip Code" type="text" value="" />
	                                              </div>
	                                          </div>
	                                      </div>
	                                      <div class="ui-form__group">
	                                          <div class="ui-form__element">
	                                              <div class="next-input-wrapper">
	                                                  <label class="next-label" for="City">Tỉnh / Th&#224;nh phố</label>
	                  
	                                                  <div class="ui-select__wrapper" bind-show="have_city">
	                                                      <select bind="province_id" bind-disabled="!have_city" bind-event-change="changeProvince()" bind-show="have_city" class="ui-select js-province-select" data-val="true" data-val-number="The field ProvinceId must be a number." id="provinceSlt" name="ProvinceId" style="width: 100%;"><option value="1">H&#224; Nội</option>
	                  <option selected="selected" value="2">TP Hồ Ch&#237; Minh</option>
	                  <option value="3">An Giang</option>
	                  <option value="4">B&#224; Rịa-Vũng T&#224;u</option>
	                  <option value="5">Bắc Giang</option>
	                  <option value="6">Bắc Kạn</option>
	                  <option value="7">Bạc Li&#234;u</option>
	                  <option value="8">Bắc Ninh</option>
	                  <option value="9">Bến Tre</option>
	                  <option value="10">B&#236;nh Định</option>
	                  <option value="11">B&#236;nh Dương</option>
	                  <option value="12">B&#236;nh Phước</option>
	                  <option value="13">B&#236;nh Thuận</option>
	                  <option value="14">C&#224; Mau</option>
	                  <option value="15">Cần Thơ</option>
	                  <option value="16">Cao Bằng</option>
	                  <option value="17">Đ&#224; Nẵng</option>
	                  <option value="18">Đắk Lắk</option>
	                  <option value="19">Đắk N&#244;ng</option>
	                  <option value="20">Điện Bi&#234;n</option>
	                  <option value="21">Đồng Nai</option>
	                  <option value="22">Đồng Th&#225;p</option>
	                  <option value="23">Gia Lai</option>
	                  <option value="24">H&#224; Giang</option>
	                  <option value="25">H&#224; Nam</option>
	                  <option value="26">H&#224; Tĩnh</option>
	                  <option value="27">Hải Dương</option>
	                  <option value="28">Hải Ph&#242;ng</option>
	                  <option value="29">Hậu Giang</option>
	                  <option value="30">H&#242;a B&#236;nh</option>
	                  <option value="31">Hưng Y&#234;n</option>
	                  <option value="32">Kh&#225;nh H&#242;a</option>
	                  <option value="33">Ki&#234;n Giang</option>
	                  <option value="34">Kon Tum</option>
	                  <option value="35">Lai Ch&#226;u</option>
	                  <option value="36">L&#226;m Đồng</option>
	                  <option value="37">Lạng Sơn</option>
	                  <option value="38">L&#224;o Cai</option>
	                  <option value="39">Long An</option>
	                  <option value="40">Nam Định</option>
	                  <option value="41">Nghệ An</option>
	                  <option value="42">Ninh B&#236;nh</option>
	                  <option value="43">Ninh Thuận</option>
	                  <option value="44">Ph&#250; Thọ</option>
	                  <option value="45">Ph&#250; Y&#234;n</option>
	                  <option value="46">Quảng B&#236;nh</option>
	                  <option value="47">Quảng Nam</option>
	                  <option value="48">Quảng Ng&#227;i</option>
	                  <option value="49">Quảng Ninh</option>
	                  <option value="50">Quảng Trị</option>
	                  <option value="51">S&#243;c Trăng</option>
	                  <option value="52">Sơn La</option>
	                  <option value="53">T&#226;y Ninh</option>
	                  <option value="54">Th&#225;i B&#236;nh</option>
	                  <option value="55">Th&#225;i Nguy&#234;n</option>
	                  <option value="56">Thanh H&#243;a</option>
	                  <option value="57">Thừa Thi&#234;n Huế</option>
	                  <option value="58">Tiền Giang</option>
	                  <option value="59">Tr&#224; Vinh</option>
	                  <option value="60">Tuy&#234;n Quang</option>
	                  <option value="61">Vĩnh Long</option>
	                  <option value="62">Vĩnh Ph&#250;c</option>
	                  <option value="63">Y&#234;n B&#225;i</option>
	                  </select>
	                                                      <svg class="next-icon next-icon--size-16" bind-show="have_city">
	                                                          <use xlink:href="#select-chevron" />
	                                                      </svg>
	                                                  </div>
	                                                  <input bind-disabled="have_city" bind-show="!have_city" class="next-input hide" data-val="true" data-val-length="Tỉnh / Thành phố tối đa 50 ký tự" data-val-length-max="50" id="City" name="City" placeholder="Nhập Tỉnh / Thành phố" type="text" value="" />
	                                              </div>
	                                          </div>
	                                          <div class="ui-form__element">
	                                              <div class="next-input-wrapper">
	                                                  <div bind-show="have_city">
	                                                      <label class="next-label" for="DistrictId">Quận huyện</label>
	                                                      <div class="ui-select__wrapper">
	                                                          <select bind-disabled="!have_city" class="ui-select js-district-select" data-val="true" data-val-number="The field Quận huyện must be a number." id="districtSlt" name="DistrictId" style="width: 100%;"><option value="30">Quận 1</option>
	                  <option value="31">Quận 2</option>
	                  <option value="32">Quận 3</option>
	                  <option value="33">Quận 4</option>
	                  <option value="34">Quận 5</option>
	                  <option value="35">Quận 6</option>
	                  <option value="36">Quận 7</option>
	                  <option value="37">Quận 8</option>
	                  <option value="38">Quận 9</option>
	                  <option value="39">Quận 10</option>
	                  <option value="40">Quận 11</option>
	                  <option value="41">Quận 12</option>
	                  <option value="42">Quận G&#242; Vấp</option>
	                  <option value="43">Quận T&#226;n B&#236;nh</option>
	                  <option value="44">Quận T&#226;n Ph&#250;</option>
	                  <option value="45">Quận B&#236;nh Thạnh</option>
	                  <option value="46">Quận Ph&#250; Nhuận</option>
	                  <option value="47">Quận Thủ Đức</option>
	                  <option value="48">Quận B&#236;nh T&#226;n</option>
	                  <option value="49">Huyện Củ Chi</option>
	                  <option value="50">Huyện H&#243;c M&#244;n</option>
	                  <option value="51">Huyện B&#236;nh Ch&#225;nh</option>
	                  <option value="52">Huyện Nh&#224; B&#232;</option>
	                  <option value="53">Huyện Cần Giờ</option>
	                  </select>
	                                                          <svg class="next-icon next-icon--size-16">
	                                                              <use xlink:href="#select-chevron" />
	                                                          </svg>
	                                                      </div>
	                                                  </div>
	                                              </div>
	                                          </div>
	                                      </div>
	                                  </div>
	                              </div>
	                          </div>
	                      </div>
	                      <div class="ui-modal__footer">
	                          <div class="ui-modal__footer-actions">
	                              <div class="ui-modal__secondary-actions">
	                                  <div class="button-group">
	                                      <button class="ui-button close-modal" data-dismiss="modal" type="button" name="button">Hủy</button>
	                                  </div>
	                              </div>
	                              <div class="ui-modal__primary-actions">
	                                  <div class="button-group button-group--right-aligned">
	                                      <button class="btn js-btn-loadable js-btn-primary has-loading" type="submit" name="button">Lưu</button>
	                                  </div>
	                              </div>
	                          </div>
	                      </div>
	                  </div>
	                  </div>
	                  </form>
	               </script>
	               <script class="modal_source" define="{editOverviewModal: new Bizweb.Modal(this)}" type="text/html">
	                  <form define="{ editCustomerOverview: new Bizweb.Forms(this, {disabledUntilDirty: true, alertUnsavedChanges: false, disableContextBar: true })}" action="/admin/customers/editoverview/8535637" method="post" autocomplete="off" tg-remote="true" refresh-on-error="edit_customer_address_form" refresh-on-success="modal customer-header customer-profile customer-card" refresh="edit_customer_address_form" id="edit_customer_address_form">
	                  <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCTENgLZOTcYuqakF+qXYqbmiLGY9B9VrHJ6ElQ3aDj9JAEmOqFS96B4MrV4vdqY6Pg==" />
	                  <div class="modal-dialog">
	                  <div class="modal-content">
	                      <div class="ui-modal__header">
	                          <div class="ui-modal__header-title">
	                              <h2 class="ui-title">Sửa thông tin khách hàng</h2>
	                          </div>
	                          <div class="ui-modal__header-actions">
	                              <div class="ui-modal__close-button">
	                                  <button class="ui-button ui-button--transparent close-modal" data-dismiss="modal" aria-label="Close modal" type="button" name="button">
	                                      <svg class="next-icon next-icon--color-ink-lighter next-icon--size-20">
	                                          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cancel-small-minor" />
	                                      </svg>
	                                  </button>
	                              </div>
	                          </div>
	                      </div>
	                      <div class="ui-modal__body">
	                          <div class="ui-modal__section">
	                              <div class="ui-type-container">
	                                  <div class="ui-form__section">
	                                      <div class="ui-form__group">
	                                          <div class="next-input-wrapper">
	                                              <label class="next-label" for="LastName">Họ</label>
	                                              <input class="next-input" data-val="true" data-val-atleastonerequired="Email hoặc Họ tên không được để trống" data-val-atleastonerequired-properties="FirstName,LastName,Email" id="LastName" name="LastName" placeholder="Nhập Họ" size="30" type="text" value="lê" />
	                                          </div>
	                                          <div class="next-input-wrapper">
	                                              <label class="next-label" for="FirstName">T&#234;n</label>
	                                              <input class="next-input" data-val="true" data-val-atleastonerequired="Email hoặc Họ tên không được để trống" data-val-atleastonerequired-properties="FirstName,LastName,Email" data-val-length="Tên tối đa 50 ký tự" data-val-length-max="50" id="FirstName" name="FirstName" placeholder="Nhập Tên" size="30" type="text" value="anh" />
	                                          </div>
	                                      </div>
	                                      <div class="next-input-wrapper">
	                                          <label class="next-label" for="Email">Email</label>
	                                          <input class="next-input" data-val="true" data-val-atleastonerequired="Email hoặc Họ tên không được để trống" data-val-atleastonerequired-properties="FirstName,LastName,Email" data-val-email="Email không đúng định dạng" data-val-length="Email tối đa 128 ký tự" data-val-length-max="128" id="Email" name="Email" placeholder="Nhập Email" size="30" type="text" value="tuana@gmail.com" />
	                                      </div>
	                                      <div class="next-input-wrapper">
	                                          <label class="next-label next-label--switch" for="AcceptsMarketing">Kh&#225;ch h&#224;ng muốn được tiếp thị</label>
	                                          <input class="next-checkbox" id="AcceptsMarketing" name="AcceptsMarketing" type="checkbox" value="true" /><input name="AcceptsMarketing" type="hidden" value="false" />
	                                          <span class="next-checkbox--styled">
	                                              <svg class="next-icon next-icon--size-10 checkmark">
	                                                  <use xlink:href="#next-checkmark-thick" />
	                                              </svg>
	                                          </span>
	                                      </div>
	                                  </div>
	                              </div>
	                          </div>
	                      </div>
	                      <div class="ui-modal__footer">
	                          <div class="ui-modal__footer-actions">
	                              <div class="ui-modal__secondary-actions">
	                                  <div class="button-group">
	                                      <button class="ui-button close-modal" data-dismiss="modal" type="button" name="button">Hủy</button>
	                                  </div>
	                              </div>
	                              <div class="ui-modal__primary-actions">
	                                  <div class="button-group button-group--right-aligned">
	                                      <button class="btn js-btn-loadable js-btn-primary has-loading btn-primary" type="submit" name="button">Lưu</button>
	                                  </div>
	                              </div>
	                          </div>
	                      </div>
	                  </div>
	                  </div>
	                  </form>
	               </script>
	               <div class="modal" data-tg-refresh="modal" id="modal_container" style="display: none;" aria-hidden="true" aria-labelledby="ModalTitle" tabindex="-1"></div>
	               <div class="modal-bg" data-tg-refresh="modal" id="modal_backdrop"></div>
	            </div>
	         </div>
	      </div>
	   </div>
	</main>
@stop