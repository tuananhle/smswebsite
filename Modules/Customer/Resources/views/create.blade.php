@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
	<main id="AppFrameMain" class="ui-app-frame__main">
   <div class="wrapper" id="wrapper">
      <div id="body" class="page-content clearfix" data-tg-refresh="body">
         <div id="content">
            <div id="customers-create" class="page">
               <script type="text/javascript">
                  Page();
               </script>
               <form accept-charset="UTF-8" action="/admin/customers/create" autocomplete="off" class="address-form" data-context-cancel-path="/admin/customers" data-context-create-name="Khách hàng" data-id="new_customer_form" data-tg-refresh="new_customer_form" data-tg-refresh-on-error="new_customer_form" data-tg-remote="true" define="{form: new Bizweb.Forms(this, { disabledUntilDirty: true, alertUnsavedChanges: true })}" id="new_customer" method="post">
                  <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCXu1y6XQgQgXusF55yd1z+dwuzNHDL5W/0tbQfPhoXOZIwJB+kGaH2g6au4ypy2kiw==">    
                  <header class="ui-title-bar-container">
                     <div class="ui-title-bar">
                        <div class="ui-title-bar__navigation">
                           <div class="ui-breadcrumbs">
                              <a href="/admin/customers" class="ui-button ui-button--transparent ui-breadcrumb">
                                 <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                    <use xlink:href="#chevron-left-thinner"></use>
                                 </svg>
                                 <span class="ui-breadcrumb__item">Khách hàng</span>
                              </a>
                           </div>
                        </div>
                        <div class="ui-title-bar__main-group">
                           <div class="ui-title-bar__heading-group">
                              <span class="ui-title-bar__icon">
                                 <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                    <use xlink:href="#next-customers"></use>
                                 </svg>
                              </span>
                              <h1 class="ui-title-bar__title">Thêm mới khách hàng</h1>
                           </div>
                           <div data-define="{titleBarActions: new Bizweb.TitleBarActions(this)}" class="action-bar">
                              <div class="ui-title-bar__mobile-primary-actions">
                                 <div class="ui-title-bar__actions">
                                    <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary ui-title-bar__action btn-primary has-loading" type="submit" name="save-customer" value="Submit" disabled="disabled">Lưu</button>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="ui-title-bar__actions-group">
                           <div class="ui-title-bar__actions">
                              <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary ui-title-bar__action btn-primary has-loading" type="submit" name="save-customer" value="Submit" disabled="disabled">Lưu</button>
                           </div>
                        </div>
                     </div>
                     <div class="collapsible-header">
                        <div class="collapsible-header__heading">
                        </div>
                     </div>
                  </header>
                  <div class="ui-layout">
                     <div class="ui-layout__sections">
                        <div class="ui-layout__section ui-layout__section--primary">
                           <div class="ui-layout__item" id="customer-overview">
                              <section class="ui-card">
                                 <header class="ui-card__header">
                                    <h2 class="ui-heading">Thông tin cơ bản</h2>
                                 </header>
                                 <div class="ui-card__section">
                                    <div class="ui-type-container">
                                       <div class="ui-form__section">
                                          <div class="ui-form__group">
                                             <div class="next-input-wrapper">
                                                <label class="next-label" for="LastName">Họ</label>
                                                <input bind="last_name" class="next-input" id="LastName" name="LastName" placeholder="Nhập Họ" size="30" type="text">
                                             </div>
                                             <div class="next-input-wrapper" id="ht-cus-name">
                                                <label class="next-label" for="FirstName">Tên</label>
                                                <input bind="first_name" class="next-input" id="FirstName" name="FirstName" placeholder="Nhập Tên" size="30" type="text">
                                             </div>
                                          </div>
                                          <div class="next-input-wrapper" id="ht-cus-email">
                                             <label class="next-label" for="Email">Email</label>
                                             <input class="next-input" id="Email" name="Email" placeholder="Nhập Email" type="text">
                                          </div>
                                          <div class="next-input-wrapper">
                                             <label class="next-label next-label--switch" for="AcceptsMarketing">Khách hàng muốn được tiếp thị</label>
                                             <input class="next-checkbox" id="AcceptsMarketing" name="AcceptsMarketing" type="checkbox" value="true">
                                             <span class="next-checkbox--styled">
                                                <svg class="next-icon next-icon--size-10 checkmark">
                                                   <use xlink:href="#next-checkmark-thick"></use>
                                                </svg>
                                             </span>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                           </div>
                           <div class="ui-layout__item">
                              <section class="ui-card">
                                 <header class="ui-card__header">
                                    <h2 class="ui-heading">Địa chỉ</h2>
                                 </header>
                                 <div class="ui-card__section">
                                    <div class="ui-type-container">
                                       <div class="ui-form__section">
                                          <div class="ui-form__group">
                                             <div class="next-input-wrapper">
                                                <label class="next-label" for="Address_LastName">Họ</label>
                                                <input bind="address_last_name" bind-placeholder="last_name" class="next-input" editable-placeholder="True" id="Address_LastName" name="Address.LastName" placeholder="" size="30" type="text">
                                             </div>
                                             <div class="next-input-wrapper">
                                                <label class="next-label" for="Address_FirstName">Tên</label>
                                                <input bind="address_first_name" bind-placeholder="first_name" class="next-input" editable-placeholder="True" id="Address_FirstName" name="Address.FirstName" placeholder="" size="30" type="text">
                                             </div>
                                          </div>
                                          <div class="ui-form__group">
                                             <div class="next-input-wrapper">
                                                <label class="next-label" for="Address_Company">Công ty</label>
                                                <input class="next-input" id="Address_Company" name="Address.Company" placeholder="Nhập Công ty" type="text">
                                             </div>
                                             <div class="next-input-wrapper">
                                                <label class="next-label" for="Address_Phone">Điện thoại</label>
                                                <input class="next-input" id="Address_Phone" name="Address.Phone" placeholder="Nhập số điện thoại" type="text">
                                             </div>
                                          </div>
                                          <div class="next-input-wrapper" id="ht-cus-address">
                                             <label class="next-label" for="Address_Address1">Địa chỉ</label>
                                             <input class="next-input" id="Address_Address1" name="Address.Address1" placeholder="Nhập Địa chỉ" type="text">
                                          </div>
                                          <div id="customer-address" define="{customer_address: new Bizweb.CustomerAddress(this,{&quot;district_slt&quot;:&quot;#districtSlt&quot;,&quot;province_slt&quot;:&quot;#provinceSlt&quot;,&quot;have_city&quot;:true})}">
                                             <div class="ui-form__group">
                                                <div class="ui-form__element">
                                                   <div class="next-input-wrapper">
                                                      <label class="next-label" for="Address_CountryId">Quốc gia</label>
                                                      <div class="ui-select__wrapper next-input--has-content">
                                                         <select bind="customer_address.country_id" bind-event-change="customer_address.changeCountry()" class="ui-select js-country-select" data-val="true" data-val-number="The field Quốc gia must be a number." id="Address_CountryId" name="Address.CountryId" style="width: 100%;">
                                                            <option value="86">Afghanistan</option>
                                                            <option value="87">Albania</option>
                                                            <option value="88">Algeria</option>
                                                            <option value="89">American Samoa</option>
                                                            <option value="90">Andorra</option>
                                                            <option value="91">Angola</option>
                                                            <option value="92">Anguilla</option>
                                                            <option value="93">Antarctica</option>
                                                            <option value="94">Antigua and Barbuda</option>
                                                            <option value="3">Argentina</option>
                                                            <option value="4">Armenia</option>
                                                            <option value="5">Aruba</option>
                                                            <option value="6">Australia</option>
                                                            <option value="7">Austria</option>
                                                            <option value="8">Azerbaijan</option>
                                                            <option value="9">Bahamas</option>
                                                            <option value="95">Bahrain</option>
                                                            <option value="10">Bangladesh</option>
                                                            <option value="96">Barbados</option>
                                                            <option value="11">Belarus</option>
                                                            <option value="12">Belgium</option>
                                                            <option value="13">Belize</option>
                                                            <option value="97">Benin</option>
                                                            <option value="14">Bermuda</option>
                                                            <option value="98">Bhutan</option>
                                                            <option value="15">Bolivia</option>
                                                            <option value="16">Bosnia and Herzegovina</option>
                                                            <option value="99">Botswana</option>
                                                            <option value="100">Bouvet Island</option>
                                                            <option value="17">Brazil</option>
                                                            <option value="101">British Indian Ocean Territory</option>
                                                            <option value="102">Brunei Darussalam</option>
                                                            <option value="18">Bulgaria</option>
                                                            <option value="103">Burkina Faso</option>
                                                            <option value="104">Burundi</option>
                                                            <option value="105">Cambodia</option>
                                                            <option value="106">Cameroon</option>
                                                            <option value="2">Canada</option>
                                                            <option value="107">Cape Verde</option>
                                                            <option value="19">Cayman Islands</option>
                                                            <option value="108">Central African Republic</option>
                                                            <option value="109">Chad</option>
                                                            <option value="20">Chile</option>
                                                            <option value="21">China</option>
                                                            <option value="110">Christmas Island</option>
                                                            <option value="111">Cocos (Keeling) Islands</option>
                                                            <option value="22">Colombia</option>
                                                            <option value="112">Comoros</option>
                                                            <option value="113">Congo</option>
                                                            <option value="114">Cook Islands</option>
                                                            <option value="23">Costa Rica</option>
                                                            <option value="24">Croatia</option>
                                                            <option value="25">Cuba</option>
                                                            <option value="26">Cyprus</option>
                                                            <option value="27">Czech Republic</option>
                                                            <option value="28">Denmark</option>
                                                            <option value="116">Djibouti</option>
                                                            <option value="117">Dominica</option>
                                                            <option value="29">Dominican Republic</option>
                                                            <option value="30">Ecuador</option>
                                                            <option value="31">Egypt</option>
                                                            <option value="118">El Salvador</option>
                                                            <option value="119">Equatorial Guinea</option>
                                                            <option value="120">Eritrea</option>
                                                            <option value="121">Estonia</option>
                                                            <option value="122">Ethiopia</option>
                                                            <option value="123">Falkland Islands (Malvinas)</option>
                                                            <option value="124">Faroe Islands</option>
                                                            <option value="125">Fiji</option>
                                                            <option value="32">Finland</option>
                                                            <option value="33">France</option>
                                                            <option value="126">French Guiana</option>
                                                            <option value="127">French Polynesia</option>
                                                            <option value="128">French Southern Territories</option>
                                                            <option value="129">Gabon</option>
                                                            <option value="130">Gambia</option>
                                                            <option value="34">Georgia</option>
                                                            <option value="35">Germany</option>
                                                            <option value="131">Ghana</option>
                                                            <option value="36">Gibraltar</option>
                                                            <option value="37">Greece</option>
                                                            <option value="132">Greenland</option>
                                                            <option value="133">Grenada</option>
                                                            <option value="134">Guadeloupe</option>
                                                            <option value="135">Guam</option>
                                                            <option value="38">Guatemala</option>
                                                            <option value="136">Guinea</option>
                                                            <option value="137">Guinea-bissau</option>
                                                            <option value="138">Guyana</option>
                                                            <option value="139">Haiti</option>
                                                            <option value="140">Heard and Mc Donald Islands</option>
                                                            <option value="141">Honduras</option>
                                                            <option value="39">Hong Kong</option>
                                                            <option value="40">Hungary</option>
                                                            <option value="142">Iceland</option>
                                                            <option value="41">India</option>
                                                            <option value="42">Indonesia</option>
                                                            <option value="143">Iran (Islamic Republic of)</option>
                                                            <option value="144">Iraq</option>
                                                            <option value="43">Ireland</option>
                                                            <option value="44">Israel</option>
                                                            <option value="45">Italy</option>
                                                            <option value="115">Ivory Coast</option>
                                                            <option value="46">Jamaica</option>
                                                            <option value="47">Japan</option>
                                                            <option value="48">Jordan</option>
                                                            <option value="49">Kazakhstan</option>
                                                            <option value="145">Kenya</option>
                                                            <option value="146">Kiribati</option>
                                                            <option value="147">Korea</option>
                                                            <option value="50">Korea, Democratic People's Republic of</option>
                                                            <option value="51">Kuwait</option>
                                                            <option value="148">Kyrgyzstan</option>
                                                            <option value="149">Lao People's Democratic Republic</option>
                                                            <option value="150">Latvia</option>
                                                            <option value="151">Lebanon</option>
                                                            <option value="152">Lesotho</option>
                                                            <option value="153">Liberia</option>
                                                            <option value="154">Libya</option>
                                                            <option value="155">Liechtenstein</option>
                                                            <option value="156">Lithuania</option>
                                                            <option value="157">Luxembourg</option>
                                                            <option value="158">Macau</option>
                                                            <option value="159">Macedonia</option>
                                                            <option value="160">Madagascar</option>
                                                            <option value="161">Malawi</option>
                                                            <option value="52">Malaysia</option>
                                                            <option value="162">Maldives</option>
                                                            <option value="163">Mali</option>
                                                            <option value="164">Malta</option>
                                                            <option value="165">Marshall Islands</option>
                                                            <option value="166">Martinique</option>
                                                            <option value="167">Mauritania</option>
                                                            <option value="168">Mauritius</option>
                                                            <option value="169">Mayotte</option>
                                                            <option value="53">Mexico</option>
                                                            <option value="170">Micronesia</option>
                                                            <option value="171">Moldova</option>
                                                            <option value="172">Monaco</option>
                                                            <option value="173">Mongolia</option>
                                                            <option value="174">Montenegro</option>
                                                            <option value="175">Montserrat</option>
                                                            <option value="176">Morocco</option>
                                                            <option value="177">Mozambique</option>
                                                            <option value="178">Myanmar</option>
                                                            <option value="179">Namibia</option>
                                                            <option value="180">Nauru</option>
                                                            <option value="181">Nepal</option>
                                                            <option value="54">Netherlands</option>
                                                            <option value="182">Netherlands Antilles</option>
                                                            <option value="183">New Caledonia</option>
                                                            <option value="55">New Zealand</option>
                                                            <option value="184">Nicaragua</option>
                                                            <option value="185">Niger</option>
                                                            <option value="186">Nigeria</option>
                                                            <option value="187">Niue</option>
                                                            <option value="188">Norfolk Island</option>
                                                            <option value="189">Northern Mariana Islands</option>
                                                            <option value="56">Norway</option>
                                                            <option value="190">Oman</option>
                                                            <option value="57">Pakistan</option>
                                                            <option value="191">Palau</option>
                                                            <option value="192">Panama</option>
                                                            <option value="193">Papua New Guinea</option>
                                                            <option value="58">Paraguay</option>
                                                            <option value="59">Peru</option>
                                                            <option value="60">Philippines</option>
                                                            <option value="194">Pitcairn</option>
                                                            <option value="61">Poland</option>
                                                            <option value="62">Portugal</option>
                                                            <option value="63">Puerto Rico</option>
                                                            <option value="64">Qatar</option>
                                                            <option value="195">Reunion</option>
                                                            <option value="65">Romania</option>
                                                            <option value="66">Russia</option>
                                                            <option value="196">Rwanda</option>
                                                            <option value="197">Saint Kitts and Nevis</option>
                                                            <option value="198">Saint Lucia</option>
                                                            <option value="199">Saint Vincent and the Grenadines</option>
                                                            <option value="200">Samoa</option>
                                                            <option value="201">San Marino</option>
                                                            <option value="202">Sao Tome and Principe</option>
                                                            <option value="67">Saudi Arabia</option>
                                                            <option value="203">Senegal</option>
                                                            <option value="85">Serbia</option>
                                                            <option value="204">Seychelles</option>
                                                            <option value="205">Sierra Leone</option>
                                                            <option value="68">Singapore</option>
                                                            <option value="69">Slovakia (Slovak Republic)</option>
                                                            <option value="70">Slovenia</option>
                                                            <option value="206">Solomon Islands</option>
                                                            <option value="207">Somalia</option>
                                                            <option value="71">South Africa</option>
                                                            <option value="208">South Georgia &amp; South Sandwich Islands</option>
                                                            <option value="72">Spain</option>
                                                            <option value="209">Sri Lanka</option>
                                                            <option value="210">St. Helena</option>
                                                            <option value="211">St. Pierre and Miquelon</option>
                                                            <option value="212">Sudan</option>
                                                            <option value="213">Suriname</option>
                                                            <option value="214">Svalbard and Jan Mayen</option>
                                                            <option value="215">Swaziland</option>
                                                            <option value="73">Sweden</option>
                                                            <option value="74">Switzerland</option>
                                                            <option value="216">Syrian Arab Republic</option>
                                                            <option value="75">Taiwan</option>
                                                            <option value="217">Tajikistan</option>
                                                            <option value="218">Tanzania</option>
                                                            <option value="76">Thailand</option>
                                                            <option value="219">Togo</option>
                                                            <option value="220">Tokelau</option>
                                                            <option value="221">Tonga</option>
                                                            <option value="222">Trinidad and Tobago</option>
                                                            <option value="223">Tunisia</option>
                                                            <option value="77">Turkey</option>
                                                            <option value="224">Turkmenistan</option>
                                                            <option value="225">Turks and Caicos Islands</option>
                                                            <option value="226">Tuvalu</option>
                                                            <option value="227">Uganda</option>
                                                            <option value="78">Ukraine</option>
                                                            <option value="79">United Arab Emirates</option>
                                                            <option value="80">United Kingdom</option>
                                                            <option value="1">United States</option>
                                                            <option value="81">United States minor outlying islands</option>
                                                            <option value="82">Uruguay</option>
                                                            <option value="83">Uzbekistan</option>
                                                            <option value="228">Vanuatu</option>
                                                            <option value="229">Vatican City State (Holy See)</option>
                                                            <option value="84">Venezuela</option>
                                                            <option selected="selected" value="230">Việt Nam</option>
                                                            <option value="231">Virgin Islands (British)</option>
                                                            <option value="232">Virgin Islands (U.S.)</option>
                                                            <option value="233">Wallis and Futuna Islands</option>
                                                            <option value="234">Western Sahara</option>
                                                            <option value="235">Yemen</option>
                                                            <option value="236">Zambia</option>
                                                            <option value="237">Zimbabwe</option>
                                                         </select>
                                                         <svg class="next-icon next-icon--size-16">
                                                            <use xlink:href="#select-chevron"></use>
                                                         </svg>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div class="ui-form__element">
                                                   <div class="next-input-wrapper">
                                                      <label class="next-label" for="Address_Zip">Postal / Zip Code</label>
                                                      <input class="next-input" id="Address_Zip" name="Address.Zip" placeholder="Nhập Postal / Zip Code" type="text">
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="ui-form__group">
                                                <div class="ui-form__element">
                                                   <div class="next-input-wrapper">
                                                      <label class="next-label" for="Address_City">Tỉnh / Thành phố</label>
                                                      <div class="ui-select__wrapper next-input--has-content" bind-show="customer_address.have_city">
                                                         <select bind="customer_address.province_id" bind-disabled="!customer_address.have_city" bind-event-change="customer_address.changeProvince()" bind-show="customer_address.have_city" class="ui-select js-province-select" data-val="true" data-val-number="The field ProvinceId must be a number." id="provinceSlt" name="Address.ProvinceId" style="width: 100%;">
                                                            <option value="1">Hà Nội</option>
                                                            <option selected="selected" value="2">TP Hồ Chí Minh</option>
                                                            <option value="3">An Giang</option>
                                                            <option value="4">Bà Rịa-Vũng Tàu</option>
                                                            <option value="5">Bắc Giang</option>
                                                            <option value="6">Bắc Kạn</option>
                                                            <option value="7">Bạc Liêu</option>
                                                            <option value="8">Bắc Ninh</option>
                                                            <option value="9">Bến Tre</option>
                                                            <option value="10">Bình Định</option>
                                                            <option value="11">Bình Dương</option>
                                                            <option value="12">Bình Phước</option>
                                                            <option value="13">Bình Thuận</option>
                                                            <option value="14">Cà Mau</option>
                                                            <option value="15">Cần Thơ</option>
                                                            <option value="16">Cao Bằng</option>
                                                            <option value="17">Đà Nẵng</option>
                                                            <option value="18">Đắk Lắk</option>
                                                            <option value="19">Đắk Nông</option>
                                                            <option value="20">Điện Biên</option>
                                                            <option value="21">Đồng Nai</option>
                                                            <option value="22">Đồng Tháp</option>
                                                            <option value="23">Gia Lai</option>
                                                            <option value="24">Hà Giang</option>
                                                            <option value="25">Hà Nam</option>
                                                            <option value="26">Hà Tĩnh</option>
                                                            <option value="27">Hải Dương</option>
                                                            <option value="28">Hải Phòng</option>
                                                            <option value="29">Hậu Giang</option>
                                                            <option value="30">Hòa Bình</option>
                                                            <option value="31">Hưng Yên</option>
                                                            <option value="32">Khánh Hòa</option>
                                                            <option value="33">Kiên Giang</option>
                                                            <option value="34">Kon Tum</option>
                                                            <option value="35">Lai Châu</option>
                                                            <option value="36">Lâm Đồng</option>
                                                            <option value="37">Lạng Sơn</option>
                                                            <option value="38">Lào Cai</option>
                                                            <option value="39">Long An</option>
                                                            <option value="40">Nam Định</option>
                                                            <option value="41">Nghệ An</option>
                                                            <option value="42">Ninh Bình</option>
                                                            <option value="43">Ninh Thuận</option>
                                                            <option value="44">Phú Thọ</option>
                                                            <option value="45">Phú Yên</option>
                                                            <option value="46">Quảng Bình</option>
                                                            <option value="47">Quảng Nam</option>
                                                            <option value="48">Quảng Ngãi</option>
                                                            <option value="49">Quảng Ninh</option>
                                                            <option value="50">Quảng Trị</option>
                                                            <option value="51">Sóc Trăng</option>
                                                            <option value="52">Sơn La</option>
                                                            <option value="53">Tây Ninh</option>
                                                            <option value="54">Thái Bình</option>
                                                            <option value="55">Thái Nguyên</option>
                                                            <option value="56">Thanh Hóa</option>
                                                            <option value="57">Thừa Thiên Huế</option>
                                                            <option value="58">Tiền Giang</option>
                                                            <option value="59">Trà Vinh</option>
                                                            <option value="60">Tuyên Quang</option>
                                                            <option value="61">Vĩnh Long</option>
                                                            <option value="62">Vĩnh Phúc</option>
                                                            <option value="63">Yên Bái</option>
                                                         </select>
                                                         <svg class="next-icon next-icon--size-16">
                                                            <use xlink:href="#select-chevron"></use>
                                                         </svg>
                                                      </div>
                                                      <input bind-disabled="customer_address.have_city" bind-show="!customer_address.have_city" class="next-input  hide" id="Address_City" name="Address.City" placeholder="Nhập Tỉnh / Thành phố" type="text" disabled="">
                                                   </div>
                                                </div>
                                                <div class="ui-form__element">
                                                   <div class="next-input-wrapper" bind-show="customer_address.have_city">
                                                      <label class="next-label" for="Address_DistrictId">Quận huyện</label>
                                                      <div class="ui-select__wrapper next-input--has-content">
                                                         <select bind-disabled="!customer_address.have_city" class="ui-select js-district-select" data-val="true" data-val-number="The field Quận huyện must be a number." id="districtSlt" name="Address.DistrictId" style="width: 100%;">
                                                            <option value="30">Quận 1</option>
                                                            <option value="31">Quận 2</option>
                                                            <option value="32">Quận 3</option>
                                                            <option value="33">Quận 4</option>
                                                            <option value="34">Quận 5</option>
                                                            <option value="35">Quận 6</option>
                                                            <option value="36">Quận 7</option>
                                                            <option value="37">Quận 8</option>
                                                            <option value="38">Quận 9</option>
                                                            <option value="39">Quận 10</option>
                                                            <option value="40">Quận 11</option>
                                                            <option value="41">Quận 12</option>
                                                            <option value="42">Quận Gò Vấp</option>
                                                            <option value="43">Quận Tân Bình</option>
                                                            <option value="44">Quận Tân Phú</option>
                                                            <option value="45">Quận Bình Thạnh</option>
                                                            <option value="46">Quận Phú Nhuận</option>
                                                            <option value="47">Quận Thủ Đức</option>
                                                            <option value="48">Quận Bình Tân</option>
                                                            <option value="49">Huyện Củ Chi</option>
                                                            <option value="50">Huyện Hóc Môn</option>
                                                            <option value="51">Huyện Bình Chánh</option>
                                                            <option value="52">Huyện Nhà Bè</option>
                                                            <option value="53">Huyện Cần Giờ</option>
                                                         </select>
                                                         <svg class="next-icon next-icon--size-16">
                                                            <use xlink:href="#select-chevron"></use>
                                                         </svg>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </section>
                           </div>
                        </div>
                        <div class="ui-layout__section ui-layout__section--secondary" id="customer-side-bar">
                           <div class="ui-layout__item">
                              <aside class="next-card">
                                 <section>
                                    <header class="next-card__header">
                                       <div class="next-grid next-grid--no-outside-padding">
                                          <div class="next-grid__cell">
                                             <h3 class="next-heading">Ghi chú</h3>
                                          </div>
                                       </div>
                                    </header>
                                    <div class="next-card__section">
                                       <div class="next-input-wrapper">
                                          <textarea class="next-input next-resize-vertical" cols="20" id="Note" name="Note" placeholder="Nhập Ghi chú về khách hàng" rows="2"></textarea>
                                       </div>
                                    </div>
                                 </section>
                              </aside>
                           </div>
                           <div class="ui-layout__item">
                              <div class="next-card next-card--aside">
                                 <header class="next-card__header">
                                    <div class="wrappable wrappable--half-spacing wrappable--vertically-centered">
                                       <div class="wrappable__item">
                                          <h3 class="ui-heading">
                                             Tag
                                          </h3>
                                       </div>
                                       <div class="wrappable__item wrappable__item--no-flex">
                                          <button class="ui-button btn--link" bind-event-click="tagDisplay.showAllTagsModal.show()" type="button" name="button">Toàn bộ tag</button>
                                       </div>
                                    </div>
                                 </header>
                                 <script type="text/template" id="display-one-tag" data-input-name="Tags[]">
                                    <li class="next-token">
                                        <span class="next-token__label"></span>
                                    
                                        <a bind-event-click="removeTag(this)" class="next-token__remove">
                                            <input type="hidden" name="" value="">
                                            <span class="next-token__remove__icon">
                                                <svg class="next-icon next-icon--size-10 next-icon--no-nudge">
                                                    <use xlink:href="#next-remove"></use>
                                                </svg>
                                            </span>
                                        </a>
                                    </li>
                                 </script>
                                 <section class="next-card__section">
                                    <div define="{tagDisplay: new Bizweb.TagDisplay(this)}" context="tagDisplay" id="tags-event-bus">
                                       <a href="/admin/customers/tags" data-define="{showAllTagsModal: new Bizweb.Modal(this)}" type="text/html" class="modal_source" style="display: none"></a>
                                       <script type="text/template" id="autocomplete-header-suggestion-template">
                                          <li>
                                              <a class="next-list__item next-list__item--promoted-action"
                                                 selectable
                                                 data-bind-event-click="selectHeader(event)">
                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
                                                      <div class="next-grid__cell next-grid__cell--no-flex next-grid__cell--vertically-centered">
                                                          <svg style="overflow: visible; /* fixes clipping in Chrome */" class="next-icon next-icon--size-12 next-icon next-icon--12 block"> <use xlink:href="#next-add-circle" /> </svg>
                                                      </div>
                                                      <div class="next-grid__cell">
                                                          <strong>Add</strong>
                                                      </div>
                                                  </div>
                                              </a>
                                          </li>
                                       </script>
                                      {{--  <script type="text/template" id="autocomplete-suggestions-list-template">
                                          {{ _.each(suggestions, function(suggestion, index) { }}
                                          <li>
                                              <a class="next-list__item"
                                                 selectable
                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(index + indexStart) }}, event)"
                                                 data-bind-class="{ 'next-list__item--is-applied': isApplied({{= JSON.stringify(index + indexStart) }}) }">
                                                  {{= suggestion }}
                                              </a>
                                          </li>
                                          {{ }) }}
                                       </script> --}}
                                       <script type="text/template" id="autocomplete-no-suggestions-template">
                                          <li>
                                              <div class="ui-popover__section">
                                                  <p class="type--subdued tc">No results found</p>
                                              </div>
                                          </li>
                                       </script>
                                      {{--  <script type="text/template" id="country-autocomplete-list-template">
                                          {{ _.each(suggestions, function(suggestion, index) { }}
                                          <li>
                                              <a class="next-list__item"
                                                 selectable
                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(index + indexStart) }}, event)"
                                                 data-bind-class="{ 'next-list__item--is-applied': isApplied({{= JSON.stringify(index + indexStart) }}) }">
                                                  {{= suggestion.name }}
                                              </a>
                                          </li>
                                          {{ }) }}
                                       </script>
                                       <script type="text/template" id="autocomplete-v2-suggestions-list-with-toggle-template">
                                          {{ _.each(suggestions, function(suggestion, index) { }}
                                          <li>
                                              <a class="next-list__item"
                                                 selectable
                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(_.pick(suggestion, [displayKey, detailsKey, secondaryDetailsKey, 'id'])) }}, event)"
                                                 data-bind-class="{ 'next-list__item--is-applied': isApplied({{= JSON.stringify(suggestion.id) }}) }">
                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
                                                      <div class="next-grid__cell next-grid__cell--no-flex">
                                                          <svg class="next-icon next-icon--size-12"> <use xlink:href="#next-checkmark" /> </svg>
                                                      </div>
                                                      <div class="next-grid__cell">
                                                          {{= suggestion[displayKey] }}
                                          
                                                          {{ if(detailsKey || secondaryDetailsKey) { }}
                                                          <span class="type--subdued"> {{= suggestion[detailsKey] || suggestion[secondaryDetailsKey]}}</span>
                                                          {{ } }}
                                                      </div>
                                                  </div>
                                              </a>
                                          </li>
                                          {{ }) }}
                                       </script>
                                       <script type="text/template" id="autocomplete-v2-no-results-template">
                                          {{ if (input) { }}
                                          <li>
                                              <a class="next-list__item next-list__item--promoted-action"
                                                 selectable
                                                 data-bind-event-click="selectHeader(event)">
                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
                                                      <div class="next-grid__cell next-grid__cell--no-flex next-grid__cell--vertically-centered">
                                                          <svg style="overflow: visible; /* fixes clipping in Chrome */" class="next-icon next-icon--size-12 next-icon next-icon--12 block"> <use xlink:href="#next-add-circle" /> </svg>
                                                      </div>
                                                      <div class="next-grid__cell">
                                                          <strong>Add</strong> {{= input }}
                                                      </div>
                                                  </div>
                                              </a>
                                          </li>
                                          
                                          {{ } }}
                                       </script>
                                       <script type="text/template" id="autocomplete-v2-results-template">
                                          {{ if (firstPage && !hasMatchingSuggestion && input) { }}
                                          <li>
                                              <a class="next-list__item next-list__item--promoted-action"
                                                 selectable
                                                 data-bind-event-click="selectHeader(event)">
                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
                                                      <div class="next-grid__cell next-grid__cell--no-flex next-grid__cell--vertically-centered">
                                                          <svg style="overflow: visible; "class="next-icon next-icon--size-12 next-icon next-icon--12 block"> <use xlink:href="#next-add-circle" /> </svg>
                                                      </div>
                                                      <div class="next-grid__cell">
                                                          <strong>Add</strong> {{= input }}
                                                      </div>
                                                  </div>
                                              </a>
                                          </li>
                                          
                                          {{ } }}
                                          
                                          {{ _.each(suggestions, function(suggestion) { }}
                                          <li>
                                              <a class="next-list__item"
                                                 selectable
                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(suggestion) }}, event)">
                                                  {{= suggestion }}
                                              </a>
                                          </li>
                                          {{ }) }}
                                       </script>
                                       <script type="text/template" id="autocomplete-v2-tag-results-template">
                                          {{ if (firstPage && !input) { }}
                                          <li>
                                              <h3 class="next-heading next-heading--micro-uppercase-bordered">Tag gần đây</h3>
                                          </li>
                                          {{ } }}
                                          {{ if (firstPage && !hasMatchingSuggestion && input) { }}
                                          <li>
                                              <a class="next-list__item next-list__item--promoted-action"
                                                 selectable
                                                 data-bind-event-click="selectHeader(event)">
                                                  <div class="next-grid next-grid--no-outside-padding next-grid--compact">
                                                      <div class="next-grid__cell next-grid__cell--no-flex next-grid__cell--vertically-centered">
                                                          <svg style="overflow: visible; "class="next-icon next-icon--size-12 next-icon next-icon--12 block"> <use xlink:href="#next-add-circle" /> </svg>
                                                      </div>
                                                      <div class="next-grid__cell">
                                                          <strong>Add</strong> {{= input }}
                                                      </div>
                                                  </div>
                                              </a>
                                          </li>
                                          
                                          {{ } }}
                                          
                                          {{ _.each(suggestions, function(suggestion) { }}
                                          <li>
                                              <a class="next-list__item"
                                                 selectable
                                                 data-bind-event-click="selectSuggestion({{= JSON.stringify(suggestion) }}, event)">
                                                  {{= suggestion }}
                                              </a>
                                          </li>
                                          {{ }) }}
                                       </script>
                                       <div class="ui-popover__container ui-popover__container--full-width-container" data-context="tagsAutocomplete" data-define="{ 'tagsAutocomplete': new Bizweb.AutocompleteV2.TagsAutocomplete(this, &quot;\/admin\/customers\/tags&quot;, $node, {&quot;values&quot;:[],&quot;maxLengthError&quot;:&quot;Tag vượt quá 249 kí tự&quot;,&quot;maxLength&quot;:249,&quot;duplicateError&quot;:&quot;Bạn đã sử dụng tag \&quot;%s\&quot; rồi&quot;}) }">
                                          <div>
                                             <div class="">
                                                <input class="next-input js-no-dirty" placeholder="VIP, thân thiết, quay lại" id="tags" data-bind="input" data-bind-event-keyup="inputChanged()" size="30" type="text" aria-expanded="false" aria-haspopup="false" aria-owns="popover-dropdown-14" aria-controls="popover-announce-selected" role="combobox" aria-autocomplete="list" aria-activedescendant="selected-option-4">
                                             </div>
                                             <div class="tooltip-error attached-to-bottom hide" data-bind-show="errorMessage">
                                                <ul class="error-list">
                                                   <li data-bind="errorMessage"></li>
                                                </ul>
                                             </div>
                                          </div>
                                          <div class="ui-popover ui-popover--full-width ui-popover--no-focus" data-popover-horizontally-relative-to-closest=".next-card" data-popover-css-vertical-margin="13" data-popover-css-horizontal-margin="0" data-popover-css-max-height="300" data-popover-css-max-width="10000" id="ui-popover--7" aria-labelledby="tags" aria-expanded="false" role="dialog" data-popover-preferred-position="top" style="margin-right: 0px; margin-left: 0px;">
                                             <div class="ui-popover__tooltip"></div>
                                             <div class="ui-popover__content-wrapper">
                                                <div class="ui-popover__content">
                                                   <div class="ui-popover__pane">
                                                      <ul class="js-autocomplete-suggestions next-list next-list--compact next-list--toggles" role="listbox" id="popover-dropdown-14"></ul>
                                                      <div class="ui-popover__section hide" data-bind-show="loadingResults">
                                                         <p class="type--subdued tc">
                                                            <span class="next-spinner">
                                                               <svg class="next-icon next-icon--size-16">
                                                                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-spinner"></use>
                                                               </svg>
                                                            </span>
                                                            Loading…
                                                         </p>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div> --}}
                                       <input type="hidden" name="Tags[]" bind-disabled="hasTags()">
                                       <ul class="js-tag-list next-token-list st"></ul>
                                    </div>
                                 </section>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="ui-page-actions">
                     <div class="ui-page-actions__container">
                        <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                           <div class="ui-page-actions__button-group">
                              <a class="btn" href="/admin/customers">Hủy</a>
                              <button name="commit" type="submit" value="Submit" class="btn js-btn-primary js-btn-loadable btn-primary has-loading" disabled="disabled">
                              Lưu
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </form>
               <div class="ui-footer-help">
                  <div class="ui-footer-help__content">
                     <div class="ui-footer-help__icon">
                        <svg class="next-icon next-icon--size-24 next-icon--no-nudge" role="img" aria-labelledby="next-help-circle-title">
                           <title id="next-help-circle-title">Help</title>
                           <use xlink:href="#next-help-circle"></use>
                        </svg>
                     </div>
                     <div>
                        <p>
                           Bạn có thể xem thêm hướng dẫn
                           <a rel="noreferrer noopener" target="_blank" href="https://web-docs.sapo.vn/them-moi-khach-hang-32.html?utm_source=admin&amp;utm_medium=customers&amp;utm_campaign=docs">tại đây</a>
                        </p>
                     </div>
                  </div>
               </div>
               <div class="modal" data-tg-refresh="modal" id="modal_container" style="display: none;" aria-hidden="true" aria-labelledby="ModalTitle" tabindex="-1"></div>
               <div class="modal-bg" data-tg-refresh="modal" id="modal_backdrop"></div>
            </div>
         </div>
      </div>
   </div>
</main>
@stop