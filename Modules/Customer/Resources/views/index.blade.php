@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
	<main id="AppFrameMain" class="ui-app-frame__main">
	   <div class="wrapper" id="wrapper">
	      <div id="body" class="page-content clearfix" data-tg-refresh="body">
	         <div id="content">
	            <style type="text/css">
	               .dropdown--customer.display-from-bottom, .next-dropdown.display-from-bottom, .display-from-bottom.sp-container {
	               top: auto;
	               bottom: 100%;
	               margin-bottom: 8px;
	               }
	               .dropdown--customer.left-aligned, .dropdown.sp-container, .next-dropdown.left-aligned, .sp-container {
	               right: auto;
	               left: 0px;
	               }
	               .dropdown--customer {
	               width: 300px;
	               z-index: 225;
	               color: #31373d;
	               }
	               .dropdown--customer {
	               line-height: 18px;
	               right: 0;
	               text-align: left;
	               min-width: 175px;
	               font-size: 0;
	               }
	               .dropdown--customer, .next-dropdown, .sp-container {
	               z-index: 225;
	               display: none;
	               position: absolute;
	               top: 100%;
	               margin-top: 11px;
	               border: 1px solid #c0c0c0;
	               background: white;
	               box-shadow: 0px 2px 6px rgba(0,0,0,0.25);
	               border-radius: 3px;
	               }
	               .has-dropdown .dropdown-header {
	               background: #fafcfc;
	               border-bottom: solid 1px #e9eff3;
	               border-radius: 3px 3px 0 0;
	               padding: 5px 10px 20px;
	               font-size: 13px;
	               }
	               .dropdown-header-title {
	               float: left;
	               font-weight: 700;
	               }
	               .dropdown-header-title a {
	               text-decoration: none;
	               }
	               .dropdown--customer .ico-list {
	               margin: 10px 0;
	               padding-left: 10px;
	               }
	               .ico-list {
	               list-style: none;
	               margin: 0;
	               }
	               .dropdown--customer li {
	               text-transform: none;
	               list-style-type: none;
	               margin: 0;
	               padding: 0;
	               font-size: 13px;
	               font-weight: 400;
	               }
	               .ico-list-14 > li, .ico-list-14 .ico-list-item {
	               min-height: 14px;
	               margin: 0 0 4.66667px 0;
	               }
	               .ico-list > li, .ico-list .ico-list-item {
	               position: relative;
	               }
	               .ico-list-14 .ico-list-content {
	               margin-left: 28px;
	               }
	               .has-dropdown {
	               display: inline-block;
	               position: relative;
	               }
	               .dropdown--customer.is-visible, .next-dropdown.is-visible, .is-visible.sp-container {
	               -webkit-animation: reveal 0.2s ease-out;
	               animation: reveal 0.2s ease-out;
	               }
	               .dropdown--customer.is-visible, .next-dropdown.is-visible, .is-visible.sp-container {
	               -webkit-animation: reveal 0.2s ease-out;
	               animation: reveal 0.2s ease-out;
	               }
	               .dropdown--customer.is-visible, .next-dropdown.is-visible, .is-visible.sp-container {
	               display: block;
	               }
	               .ico-list li > .ico, .ico-list li > .store-setup__ico--checkbox, .ico-list .ico-list-item > .ico, .ico-list .ico-list-item > .store-setup__ico--checkbox {
	               position: absolute;
	               top: 1px;
	               left: 0;
	               line-height: unset;
	               text-indent: unset;
	               }
	               .dropdown--customer:after, .dropdown--customer:before, .next-dropdown:after, .sp-container:after, .next-dropdown:before, .sp-container:before {
	               position: absolute;
	               right: 10px;
	               content: "";
	               width: 0;
	               height: 0;
	               border-top: none;
	               border-right: 8px solid transparent;
	               border-left: 8px solid transparent;
	               }
	               .dropdown--customer.display-from-bottom:after, .next-dropdown.display-from-bottom:after, .display-from-bottom.sp-container:after {
	               margin-top: -1px;
	               border-top: 7px solid white;
	               }
	               .dropdown--customer.display-from-bottom:before, .dropdown--customer.display-from-bottom:after, .next-dropdown.display-from-bottom:before, .display-from-bottom.sp-container:before, .next-dropdown.display-from-bottom:after, .display-from-bottom.sp-container:after {
	               top: 100%;
	               left: 10px;
	               border-bottom: none;
	               border-right: 8px solid transparent;
	               border-left: 8px solid transparent;
	               }
	               .dropdown--customer.display-from-bottom:before, .next-dropdown.display-from-bottom:before, .display-from-bottom.sp-container:before {
	               border-top: 7px solid #c2c2c2;
	               }
	            </style>
	            <script type="text/javascript">
	               Page();
	               
	               $(window).scroll(function () {
	                   scrollPaggingMobile('.customers');
	               });
	            </script>
	            <form id="frmAction" method="post">
	               <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCWUWtEdJ4s9sKR58es2volbF5cXu7Rcl9wildxVE7ROcewQf5pBoPkOMWIiLuKbgDA==">
	               <input type="hidden" name="returnUrl" value="/admin/customers">
	               <input type="hidden" name="id" value="">
	            </form>
	            <form id="frmRemoveSavedSearch" method="post">
	               <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCWUWtEdJ4s9sKR58es2volbF5cXu7Rcl9wildxVE7ROcewQf5pBoPkOMWIiLuKbgDA==">
	               <input type="hidden" name="returnUrl" value="/admin/customers">
	               <input type="hidden" name="savedSearchId" value="">
	            </form>
	            <form id="frmFilter" method="get">
	               <input type="hidden" name="Query" value="">
	            </form>
	            <form id="frmBulkActions" method="get">
	               <input type="hidden" name="SelectedAll" value="false">
	            </form>
	            <div id="pages-index" class="page default has-contextual-help customers-page" define="{customersImportAndExport:new Bizweb.CustomersImportAndExport(this, {'customers_match_current_search':1})}" context="customersImportAndExport">
	               <header class="ui-title-bar-container   ui-title-bar-container--full-width">
	                  <div class="ui-title-bar ">
	                     <div class="ui-title-bar__main-group">
	                        <div class="ui-title-bar__heading-group">
	                           <span class="ui-title-bar__icon">
	                              <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
	                                 <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-discounts"></use>
	                              </svg>
	                           </span>
	                           <h1 class="ui-title-bar__title">Khách hàng</h1>
	                        </div>
	                        <div define="{titleBarActions: new Bizweb.TitleBarActions(this)}" class="action-bar">
	                           <div class="action-bar__item action-bar__item--link-container">
	                              <div class="action-bar__top-links">
	                                 <button class="ui-button ui-button--transparent action-bar__link" bind-event-click="showExportCustomers()" data-popover-index="1" type="button" name="button">
	                                    <svg class="next-icon next-icon--size-20 action-bar__link-icon next-icon--no-nudge">
	                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-export-minor"></use>
	                                    </svg>
	                                    Xuất file
	                                 </button>
	                                 <button class="ui-button ui-button--transparent action-bar__link" bind-event-click="openFileImportCustomers()" data-popover-index="2" type="button" name="button">
	                                    <svg class="next-icon next-icon--size-20 action-bar__link-icon next-icon--no-nudge">
	                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-import-minor"></use>
	                                    </svg>
	                                    Nhập danh sách
	                                 </button>
	                              </div>
	                              <div class="action-bar__more hide">
	                                 <div class="ui-popover__container">
	                                    <button class="ui-button ui-button--transparent" type="button" name="button">
	                                       <span data-singular-label="Chọn thao tác" data-multiple-label="Chọn thao tác" class="action-bar__more-label">Chọn thao tác</span>
	                                       <svg class="next-icon next-icon--size-20">
	                                          <use xlink:href="#next-disclosure"></use>
	                                       </svg>
	                                    </button>
	                                    <div class="ui-popover ui-popover--align-edge">
	                                       <div class="ui-popover__tooltip"></div>
	                                       <div class="ui-popover__content-wrapper">
	                                          <div class="ui-popover__content">
	                                             <div class="ui-popover__pane">
	                                                <ul class="action-bar__popover-wrapper">
	                                                   <li>
	                                                      <ul class="ui-action-list">
	                                                         <li class="ui-action-list__item">
	                                                            <button class="ui-action-list-action action-bar__popover-hidden-item" data-popover-index="1" bind-event-click="showExportCustomers()" type="button" name="button">
	                                                               <span class="ui-action-list-action__text">
	                                                                  <span class="action-bar__popover-icon-wrapper">
	                                                                     <svg class="next-icon next-icon--color-blue next-icon--size-16 action-bar__popover-icon">
	                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-export-minor"></use>
	                                                                     </svg>
	                                                                  </span>
	                                                                  Xuất file
	                                                               </span>
	                                                            </button>
	                                                         </li>
	                                                         <li class="ui-action-list__item">
	                                                            <button class="ui-action-list-action action-bar__popover-hidden-item" data-popover-index="2" bind-event-click="openFileImportCustomers()" type="button" name="button">
	                                                               <span class="ui-action-list-action__text">
	                                                                  <span class="action-bar__popover-icon-wrapper">
	                                                                     <svg class="next-icon next-icon--color-blue next-icon--size-16 action-bar__popover-icon">
	                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-import-minor"></use>
	                                                                     </svg>
	                                                                  </span>
	                                                                  Nhập danh sách
	                                                               </span>
	                                                            </button>
	                                                         </li>
	                                                      </ul>
	                                                   </li>
	                                                </ul>
	                                             </div>
	                                          </div>
	                                       </div>
	                                    </div>
	                                 </div>
	                              </div>
	                           </div>
	                           <div class="ui-title-bar__mobile-primary-actions">
	                              <div class="ui-title-bar__actions">
	                                 <a href="{{ url('/customer/create') }}" class="ui-button ui-button--primary ui-title-bar__action">Thêm khách hàng</a>
	                              </div>
	                           </div>
	                        </div>
	                     </div>
	                     <div class="ui-title-bar__actions-group">
	                        <div class="ui-title-bar__actions">
	                           <a href="{{ url('/customer/create') }}" class="ui-button ui-button--primary ui-title-bar__action">Thêm khách hàng</a>
	                        </div>
	                     </div>
	                  </div>
	                  <div class="collapsible-header">
	                     <div class="collapsible-header__heading"></div>
	                  </div>
	               </header>
	               <div class="ui-layout ui-layout--full-width">
	                  <div class="ui-layout__sections">
	                     <div class="ui-layout__section">
	                        <div class="ui-layout__item">
	                           <section class="ui-card">
	                              <div id="filterAndSavedSearch" define="{filterer: new Bizweb.FilterAndSavedSearch(this, {&quot;type_filter&quot;:&quot;customers&quot;})}" context="filterer">
	                                 <div class="next-tab__container ">
	                                    <ul class="next-tab__list filter-tab-list" id="filter-tab-list" role="tablist" data-has-next-tab-controller="true">
	                                       <li class="filter-tab-item" data-tab-index="1">
	                                          <a href="/admin/customers" class="filter-tab filter-tab-active show-all-items next-tab next-tab--is-active">Tất cả khách hàng</a>
	                                       </li>
	                                       <li class="filter-tab-item" data-tab-index="2" data-tab-id="2374922" data-tab-name="Nhận quảng cáo">
	                                          <a href="customers?savedSearchId=2374922" class="filter-tab next-tab">Nhận quảng cáo</a>
	                                       </li>
	                                       <li class="filter-tab-item" data-tab-index="3" data-tab-id="2374923" data-tab-name="Khách hàng thân thiết">
	                                          <a href="customers?savedSearchId=2374923" class="filter-tab next-tab">Khách hàng thân thiết</a>
	                                       </li>
	                                       <li class="next-tab__list__disclosure-item dropdown-container" id="hidden-search" style="display:none">
	                                          <span class="next-tab next-tab--disclosure filter-tab" id="more-savedsearch" tabindex="-1" aria-selected="true" aria-label="dropdown item" aria-expanded="false" role="button">
	                                             <svg class="next-icon next-icon--size-16 next-icon--no-nudge" id="svg-msaved">
	                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-ellipsis"></use>
	                                             </svg>
	                                          </span>
	                                          <ul class="dropdown-menu arrow-style dropdown-hidden-search dropdown-menu-right pull-right" role="tablist" id="dropdown-hidden-search"></ul>
	                                       </li>
	                                    </ul>
	                                 </div>
	                                 <div class="next-card__section next-card__section--no-bottom-spacing">
	                                    <div class="obj-filter hide-when-printing table-filter-container">
	                                       <div class="next-input-wrapper">
	                                          <div class="next-field__connected-wrapper">
	                                             <div class="next-field--connected--no-flex">
	                                                <div class="ui-popover__container">
	                                                   <button class="ui-button ui-btn-filter" type="button" name="button" id="ui-btn-filter">
	                                                      Lọc <span class="btn__text--deprioritize" id="btn__text--deprioritize-filter">khách hàng</span>
	                                                      <svg class="next-icon next-icon--size-20" id="svg-filter">
	                                                         <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-disclosure"></use>
	                                                      </svg>
	                                                   </button>
	                                                   <div class="ui-popover ui-popover--align-edge dropdown-menu margin-10 dropdown-filter">
	                                                      <div class="ui-popover__tooltip" style="left: 75px;"></div>
	                                                      <div class="ui-popover__content-wrapper">
	                                                         <div class="ui-popover__content" style="max-height: 272px; width: 205px;">
	                                                            <div class="ui-popover__section">
	                                                               <div class="add-filters">
	                                                                  <form bind-event-submit="submit(event)" class="clearfix">
	                                                                     <p class="filter-heading">Hiển thị khách hàng theo:</p>
	                                                                     <div class="filter-builder">
	                                                                        <div class="next-input-wrapper--half-spacing">
	                                                                           <div class="next-input-wrapper">
	                                                                              <label class="next-label helper--visually-hidden" for="filter-select-1">Chọn điều kiện lọc</label>
	                                                                              <div class="ui-select__wrapper">
	                                                                                 <select name="filter-select-1" bind="filterKey" bind-event-change="resetFilter()" id="filter-conditions" class="ui-select">
	                                                                                    <option value="">Chọn điều kiện lọc...</option>
	                                                                                    <option value="TotalSpent">Tổng chi tiêu</option>
	                                                                                    <option value="OrdersCount">Tổng đơn hàng</option>
	                                                                                    <option value="CreatedOn">Ngày tạo</option>
	                                                                                    <option value="AcceptsMarketing">Nhận tiếp thị</option>
	                                                                                    <option value="State">Trạng thái</option>
	                                                                                    <option value="Tag">Đã được tag với</option>
	                                                                                 </select>
	                                                                                 <svg class="next-icon next-icon--size-16">
	                                                                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
	                                                                                 </svg>
	                                                                              </div>
	                                                                           </div>
	                                                                        </div>
	                                                                        <div bind-show="filterKey == 'TotalSpent'" class="hide">
	                                                                           <div class="next-input-wrapper--half-spacing">
	                                                                              <div class="next-input-wrapper">
	                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-2">Chọn điều kiện lọc</label>
	                                                                                 <div class="ui-select__wrapper">
	                                                                                    <select bind="option" class="ui-select filter-select" name="filter-select-2">
	                                                                                       <option value="">Chọn điều kiện lọc...</option>
	                                                                                       <option value="equals">bằng</option>
	                                                                                       <option value="not_equals">không bằng</option>
	                                                                                       <option value="less_than">nhỏ hơn</option>
	                                                                                       <option value="greater_than">lớn hơn</option>
	                                                                                    </select>
	                                                                                    <svg class="next-icon next-icon--size-16">
	                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
	                                                                                    </svg>
	                                                                                 </div>
	                                                                              </div>
	                                                                              <div class="next-input-wrapper--half-spacing">
	                                                                                 <div class="next-input-wrapper">
	                                                                                    <label class="next-label helper--visually-hidden" for="filter-input-0">Filter-input-0</label>
	                                                                                    <input type="text" name="filter-input-0" id="filter-input-0" data-bind="value" data-bind-class="{error: isValid}" data-bind-show="optionSelected()" class="next-input hide">
	                                                                                 </div>
	                                                                              </div>
	                                                                           </div>
	                                                                           <div class="next-input-wrapper--vertical-block">
	                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
	                                                                           </div>
	                                                                        </div>
	                                                                        <div bind-show="filterKey == 'OrdersCount'" class="hide">
	                                                                           <div class="next-input-wrapper--half-spacing">
	                                                                              <div class="next-input-wrapper">
	                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-3">Chọn điều kiện lọc</label>
	                                                                                 <div class="ui-select__wrapper">
	                                                                                    <select bind="option" class="ui-select filter-select" name="filter-select-3" id="filter-select-3">
	                                                                                       <option value="">Chọn điều kiện lọc...</option>
	                                                                                       <option value="equals">bằng</option>
	                                                                                       <option value="not_equals">không bằng</option>
	                                                                                       <option value="less_than">nhỏ hơn</option>
	                                                                                       <option value="greater_than">lớn hơn</option>
	                                                                                    </select>
	                                                                                    <svg class="next-icon next-icon--size-16">
	                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
	                                                                                    </svg>
	                                                                                 </div>
	                                                                              </div>
	                                                                              <div class="next-input-wrapper--half-spacing">
	                                                                                 <div class="next-input-wrapper">
	                                                                                    <label class="next-label helper--visually-hidden" for="filter-input-1">Filter-input-1</label>
	                                                                                    <input type="text" name="filter-input-1" id="filter-input-1" data-bind="value" data-bind-class="{error: isValid}" data-bind-show="optionSelected()" class="next-input hide">
	                                                                                 </div>
	                                                                              </div>
	                                                                           </div>
	                                                                           <div class="next-input-wrapper--vertical-block">
	                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
	                                                                           </div>
	                                                                        </div>
	                                                                        <div bind-show="filterKey == 'CreatedOn'" class="hide">
	                                                                           <div class="next-input-wrapper--half-spacing">
	                                                                              <div class="next-input-wrapper">
	                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-4">Chọn điều kiện lọc</label>
	                                                                                 <div class="ui-select__wrapper">
	                                                                                    <select name="filter-select-4" id="filter-select-4" bind="option" class="ui-select filter-select">
	                                                                                       <option value="">Chọn điều kiện lọc...</option>
	                                                                                       <option value="less_than">Từ ngày này về trước</option>
	                                                                                       <option value="greater_than">Từ ngày</option>
	                                                                                    </select>
	                                                                                    <svg class="next-icon next-icon--size-16">
	                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
	                                                                                    </svg>
	                                                                                 </div>
	                                                                              </div>
	                                                                           </div>
	                                                                           <div class="next-input-wrapper--half-spacing hide" bind-show="optionRequiresValue()">
	                                                                              <div class="next-input-wrapper">
	                                                                                 <label for="filter_date" class="next-label">Ngày</label>
	                                                                                 <div class="next-input--stylized">
	                                                                                    <span class="next-input__add-on next-input__add-on--before">
	                                                                                       <svg class="next-icon next-icon--size-16">
	                                                                                          <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-calendar"></use>
	                                                                                       </svg>
	                                                                                    </span>
	                                                                                    <input type="text" name="filter-date-1" id="filter-date-1" class="next-input next-input--invisible input-date hasDatepicker" data-rel="datepicker" bind="value" bind-class="{error: isValid}" placeholder="DD-MM-YYYY">
	                                                                                 </div>
	                                                                              </div>
	                                                                           </div>
	                                                                           <div class="next-input-wrapper--vertical-block">
	                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
	                                                                           </div>
	                                                                        </div>
	                                                                        <div bind-show="filterKey == 'AcceptsMarketing'" class="hide">
	                                                                           <div class="next-input-wrapper--half-spacing">
	                                                                              <div class="next-input-wrapper">
	                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-5">Chọn điều kiện lọc</label>
	                                                                                 <div class="ui-select__wrapper">
	                                                                                    <select bind="option" class="ui-select filter-select" name="filter-select-5">
	                                                                                       <option value="">Chọn điều kiện lọc...</option>
	                                                                                       <option value="false">không</option>
	                                                                                       <option value="true">có</option>
	                                                                                    </select>
	                                                                                    <svg class="next-icon next-icon--size-16">
	                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
	                                                                                    </svg>
	                                                                                 </div>
	                                                                              </div>
	                                                                           </div>
	                                                                           <div class="next-input-wrapper--vertical-block">
	                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
	                                                                           </div>
	                                                                        </div>
	                                                                        <div bind-show="filterKey == 'State'" class="hide">
	                                                                           <div class="next-input-wrapper--half-spacing">
	                                                                              <div class="next-input-wrapper">
	                                                                                 <label class="next-label helper--visually-hidden" for="filter-select-6">Chọn điều kiện lọc</label>
	                                                                                 <div class="ui-select__wrapper">
	                                                                                    <select bind="option" class="ui-select filter-select" name="filter-select-6">
	                                                                                       <option value="">Chọn điều kiện lọc...</option>
	                                                                                       <option value="enabled">Có tài khoản</option>
	                                                                                       <option value="disabled">Chưa đăng ký</option>
	                                                                                    </select>
	                                                                                    <svg class="next-icon next-icon--size-16">
	                                                                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#select-chevron"></use>
	                                                                                    </svg>
	                                                                                 </div>
	                                                                              </div>
	                                                                           </div>
	                                                                           <div class="next-input-wrapper--vertical-block">
	                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
	                                                                           </div>
	                                                                        </div>
	                                                                        <div bind-show="filterKey == 'Tag'" class="hide">
	                                                                           <div class="next-input-wrapper--half-spacing">
	                                                                              <div class="next-input-wrapper">
	                                                                                 <label class="next-label helper--visually-hidden" for="filter-input-4">Filter-input-4</label>
	                                                                                 <input type="text" name="filter-input-4" id="filter-input-4" data-bind="value" bind-class="{error: isValid}" class="next-input">
	                                                                              </div>
	                                                                           </div>
	                                                                           <div class="next-input-wrapper--vertical-block">
	                                                                              <button class="ui-button btn-slim add-filter filtering-complete" type="button" bind-event-click="submitFilter(this)" name="button">Lọc</button>
	                                                                           </div>
	                                                                        </div>
	                                                                     </div>
	                                                                  </form>
	                                                               </div>
	                                                            </div>
	                                                         </div>
	                                                      </div>
	                                                   </div>
	                                                </div>
	                                             </div>
	                                             <div id="has--actions" data-tg-refresh-always="true" class=""></div>
	                                             <form action="/admin/customers" bind-event-submit="submitQuery()" class="next-form next-form--full-width next-field--connected--no-border-radius" method="get">
	                                                <label class="next-label helper--visually-hidden" for="query">Query</label>
	                                                <div class="next-input--stylized next-field--connected">
	                                                   <span class="next-input__add-on next-input__add-on--before">
	                                                      <svg class="next-icon next-icon--color-slate-lightest next-icon--size-16">
	                                                         <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-search-reverse"></use>
	                                                      </svg>
	                                                   </span>
	                                                   <input type="text" name="query" id="query" placeholder="Tìm kiếm khách hàng" bind="query" bind-event-input="submitQuery()" value="" class="next-input next-input--invisible">
	                                                </div>
	                                             </form>
	                                             <div id="saved-search-actions-next" class="saved-search-actions-next" data-tg-refresh="saved-search-actions-next">
	                                             </div>
	                                          </div>
	                                       </div>
	                                    </div>
	                                 </div>
	                              </div>
	                              <div class="ui-card__section has-bulk-actions customers" refresh="customers" id="customers-refresh">
	                                 <div class="ui-type-container clearfix">
	                                    <div class="table-wrapper" define="{bulkActions: new Bizweb.BulkActions(this,{&quot;type&quot;:&quot;khách hàng&quot;})}" context="bulkActions">
	                                       <table id="price-rule-table" class="table-hover bulk-action-context expanded">
	                                          <thead>
	                                             <tr>
	                                                <th class="select">
	                                                   <span>
	                                                      <div class="tooltip tooltip-left-align tooltip-bottom bulk-actions__select-all-tooltip">
	                                                         <div class="next-input-wrapper bulk-actions__select-all btn--select-all" bind-class="{'bulk-actions__select-all--has-selected-items': selectedItems.length > 0}" style="left: auto;">
	                                                            <label class="next-label next-label--switch btn btn-slim bulk-actions__select-all-btn" for="bulk-actions__select-all" bind-class="{'btn btn-slim bulk-actions__select-all-btn': selectedItems.length == 0, 'helper--visually-hidden': selectedItems.length > 0}">
	                                                            <span class="helper--visually-hidden">Select all customers</span>
	                                                            </label>
	                                                            <input type="checkbox" name="bulk-actions__select-all" id="bulk-actions__select-all" bind-event-change="addOrRemoveAllBulkActionItems(this)" class="next-checkbox js-no-dirty bulk-actions__select-all-checkbox all-bulk-action">
	                                                            <span class="next-checkbox--styled">
	                                                               <svg class="next-icon next-icon--size-10 checkmark">
	                                                                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-checkmark-thick"></use>
	                                                               </svg>
	                                                               <svg class="next-icon next-icon--size-10 indeterminate">
	                                                                  <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-minus"></use>
	                                                               </svg>
	                                                            </span>
	                                                         </div>
	                                                         <div class="tooltip-container" bind-class="{'hide': selectedItems.length > 0}">
	                                                            <span class="tooltip-label">Chọn tất cả khách hàng</span>
	                                                         </div>
	                                                      </div>
	                                                      <div class="bulk-actions" bind-class="{'bulk-actions--is-visible': selectedItems.length > 0}" all-items="1" items-page="50" total-records="1" style="width: 1012px;">
	                                                         <ul class="bulk-actions__main-bar segmented">
	                                                            <li>
	                                                               <a class="btn btn-slim btn-disabled btn--selection">
	                                                               <span class="selection-count bulk-action-items-count" selected-bulk-action-items="0">
	                                                               <span><span class="hidden-xs">Đã chọn</span> <span class="display-bulk-action-items-count"></span> khách hàng</span>
	                                                               </span>
	                                                               </a>
	                                                            </li>
	                                                            <li>
	                                                               <div class="ui-popover__container">
	                                                                  <button class="ui-button ui-button--size-small btn-dropdown-bulkaction" id="btn-dropdown-bulkaction" type="button" name="button">
	                                                                     <span class="hidden-xs">Chọn thao tác</span><span class="hidden-lg hidden-md hidden-sm">Thao tác</span> 
	                                                                     <svg class="next-icon next-icon--size-20" id="svg-bulkaction">
	                                                                        <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-disclosure"></use>
	                                                                     </svg>
	                                                                  </button>
	                                                                  <div class="dropdown-menu margin-10 dropdown-bulkaction">
	                                                                     <div class="ui-popover__tooltip"></div>
	                                                                     <div class="ui-popover__pane">
	                                                                        <div class="ui-popover__section ui-popover__section--no-padding">
	                                                                           <ul class="next-list next-list--compact">
	                                                                              <li><a define="{urlBulkActionDelete:'/admin/customers/bulkdeletes'}" class="next-list__item" href="javascript:void(0);" bind-event-click="showDeleteItemsModal()">Xóa khách hàng</a></li>
	                                                                           </ul>
	                                                                        </div>
	                                                                        <div class="ui-popover__section ui-popover__section--no-padding">
	                                                                           <ul class="next-list next-list--compact">
	                                                                              <li><a class="next-list__item" href="#" define="{urlBulkActionAddTag:'/admin/customers/bulkaddtags'}" bind-event-click="addTags()">Thêm tags</a></li>
	                                                                              <li><a class="next-list__item" href="" define="{urlBulkActionRemoveTag:'/admin/customers/bulkremovetags'}" bind-event-click="removeTags()">Xóa tags</a></li>
	                                                                           </ul>
	                                                                        </div>
	                                                                     </div>
	                                                                  </div>
	                                                               </div>
	                                                            </li>
	                                                         </ul>
	                                                         <span class="bulk-select-all">
	                                                            <p class="bulk-action-all-selector">
	                                                            </p>
	                                                         </span>
	                                                      </div>
	                                                   </span>
	                                                </th>
	                                                <th><span>Họ tên</span></th>
	                                                <th><span>Email</span></th>
	                                                <th class="tc"><span>Đơn hàng</span></th>
	                                                <th class="tc" style="min-width: 85px;"><span>Đơn hàng gần nhất</span></th>
	                                                <th class="tr"><span>Tổng chi tiêu</span></th>
	                                             </tr>
	                                          </thead>
	                                          <tbody>
	                                             <tr data-define="{nestedLinkContainer: new Bizweb.NestedLinkContainer(this)}">
	                                                <td class="select">
	                                                   <div class="next-input-wrapper">
	                                                      <label class="next-label helper--visually-hidden next-label--switch" for="customer_ids_8535637">Select customer, eq weee</label>
	                                                      <input type="checkbox" value="8535637" name="ids" bind-event-change="addOrRemoveBulkActionItem(8535637,this)" id="bulk-action-8535637" class="bulk-action-item next-checkbox">
	                                                      <span class="next-checkbox--styled">
	                                                         <svg class="next-icon next-icon--size-10 checkmark">
	                                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-checkmark-thick"></use>
	                                                         </svg>
	                                                      </span>
	                                                   </div>
	                                                </td>
	                                                <td class="name no-wrap">
	                                                   <div>
	                                                      <a id="customer-8535637" href="/admin/customers/8535637" data-nested-link-target="true">
	                                                      lê anh                                                                    </a>
	                                                   </div>
	                                                </td>
	                                                <td class="location type--subdued">
	                                                   tuana@gmail.com
	                                                </td>
	                                                <td class="orders tc">0</td>
	                                                <td class="last_order tc">
	                                                   <span>---</span>
	                                                </td>
	                                                <td class="money_spent tr">0₫</td>
	                                             </tr>
	                                          </tbody>
	                                       </table>
	                                       <script class="modal_source" define="{deleteItemsModal: new Bizweb.Modal(this)}" type="text/html">
	                                          <div class="modal-dialog">
	                                              <div class="modal-content">
	                                                  <div class="ui-modal__header">
	                                                      <div class="ui-modal__header-title">
	                                                          <h2 class="ui-title">Bạn chắc chắn muốn xóa những khách hàng này?</h2>
	                                                      </div>
	                                                      <div class="ui-modal__header-actions">
	                                                          <div class="ui-modal__close-button">
	                                                              <button class="ui-button ui-button--transparent close-modal" data-dismiss="modal" aria-label="Close modal" type="button" name="button"><svg class="next-icon next-icon--color-ink-lighter next-icon--size-20"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cancel-small-minor"></use> </svg></button>
	                                                          </div>
	                                                      </div>
	                                                  </div>
	                                                  <div class="ui-modal__body">
	                                                      <div class="ui-modal__section">
	                                                          <div>
	                                                              <p>Thao tác này sẽ xóa những khách hàng bạn đã chọn. Thao tác này không thể khôi phục.</p>
	                                                          </div>
	                                                      </div>
	                                                  </div>
	                                                  <div class="ui-modal__footer">
	                                                      <div class="ui-modal__footer-actions">
	                                                          <div class="ui-modal__secondary-actions">
	                                                              <div class="button-group">
	                                                                  <button class="ui-button close-modal" data-dismiss="modal" type="button" name="button">Hủy</button>
	                                                              </div>
	                                                          </div>
	                                                          <div class="ui-modal__primary-actions">
	                                                              <div class="button-group button-group--right-aligned">
	                                                                  <button class="ui-button btn-destroy-no-hover js-btn-loadable has-loading btn-delete-bulk-action" type="button" name="commit" bind-event-click="deleteItems()">Xóa</button>
	                                                              </div>
	                                                          </div>
	                                                      </div>
	                                                  </div>
	                                              </div>
	                                          </div>
	                                       </script>
	                                    </div>
	                                    <div class="t-grid-pager-boder">
	                                       <div class="t-pager t-reset clearfix fix-margin-pager">
	                                          <div class="col-md-6 col-lg-6 hidden-xs hidden-sm no-padding">
	                                             <div class="t-status-text dataTables_info">Hiển thị kết quả từ 1 - 1 trên tổng 1 </div>
	                                          </div>
	                                       </div>
	                                    </div>
	                                 </div>
	                              </div>
	                           </section>
	                        </div>
	                        <div class="ui-footer-help">
	                           <div class="ui-footer-help__content">
	                              <div class="ui-footer-help__icon">
	                                 <svg class="next-icon next-icon--size-24 next-icon--no-nudge" role="img" aria-labelledby="next-help-circle-title">
	                                    <title id="next-help-circle-title">Help</title>
	                                    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-help-circle"></use>
	                                 </svg>
	                              </div>
	                              <div>
	                                 Bạn có thể xem thêm hướng dẫn về quản lý khách hàng <a target="_blank" rel="noreferrer noopener" title="Hướng dẫn quản lý khách hàng" href="https://web-docs.sapo.vn/gioi-thieu-31.html?utm_campaign=cpn:docs-plm:tool_tip_khach_hang&amp;utm_source=admin&amp;utm_medium=referral&amp;utm_content=fm:text_link-km:-sz:&amp;utm_term=&amp;campaign=quantri">tại đây</a>
	                              </div>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	               </div>
	               <script class="modal_source" define="{exportCustomersModal: new Bizweb.Modal(this)}" type="text/html">
	                  <div class="modal-dialog">
	                      <div class="modal-content">
	                          <div class="ui-modal__header">
	                              <div class="ui-modal__header-title">
	                                  <h2 class="ui-title">Xuất danh sách khách hàng</h2>
	                              </div>
	                              <div class="ui-modal__header-actions">
	                                  <div class="ui-modal__close-button">
	                                      <button class="ui-button ui-button--transparent close-modal" data-dismiss="modal" aria-label="Close modal" type="button" name="button"><svg class="next-icon next-icon--color-ink-lighter next-icon--size-20"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cancel-small-minor"></use> </svg></button>
	                                  </div>
	                              </div>
	                          </div>
	                          <div class="ui-modal__body">
	                              <div class="ui-modal__section">
	                                  <div>
	                                      <form id="form-export-customers">
	                                          <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCWUWtEdJ4s9sKR58es2volbF5cXu7Rcl9wildxVE7ROczHbGv03eRUDPvcHDiX7McQ==" />
	                                          <input type="hidden" name="Query" />
	                                          <input type="hidden" name="TotalSpent" />
	                                          <input type="hidden" name="TotalSpentNot" />
	                                          <input type="hidden" name="TotalSpentMax" />
	                                          <input type="hidden" name="TotalSpentMin" />
	                                          <input type="hidden" name="OrdersCount" />
	                                          <input type="hidden" name="OrdersCountNot" />
	                                          <input type="hidden" name="OrdersCountMax" />
	                                          <input type="hidden" name="OrdersCountMin" />
	                                          <input type="hidden" name="CreatedOnMin" />
	                                          <input type="hidden" name="CreatedOnMax" />
	                                          <input type="hidden" name="AcceptsMarketing" />
	                                          <input type="hidden" name="State" />
	                                          <input type="hidden" name="Tag" />
	                                          <input type="hidden" name="Page" />
	                                          <fieldset class="next-fieldset-wrapper">
	                                              <legend class="helper--visually-hidden">Selection of Customer</legend>
	                                              <div class="next-input-wrapper">
	                                                  <label class="next-label next-label--switch" for="exportCustomersType-current-page">Trên trang này</label>
	                                                  <input type="radio" name="exportCustomersType" id="exportCustomersType-current-page" value="currentPage" class="next-radio" checked="checked">
	                                                  <span class="next-radio--styled"></span>
	                                              </div>
	                                                  <div class="next-input-wrapper">
	                                                      <label class="next-label next-label--switch" for="exportCustomersType-all-customers">Tất cả khách hàng</label>
	                                                      <input type="radio" name="exportCustomersType" id="exportCustomersType-all-customers" value="allCustomers" class="next-radio">
	                                                      <span class="next-radio--styled"></span>
	                                                  </div>
	                                              <div class="next-input-wrapper">
	                                                  <label class="next-label next-label--switch" for="exportCustomersType-selected-customers">Các khách hàng được chọn</label>
	                                                  <input type="radio" name="exportCustomersType" id="exportCustomersType-selected-customers" value="selectedCustomers" class="next-radio">
	                                                  <span class="next-radio--styled"></span>
	                                              </div>
	                                                  <div class="next-input-wrapper">
	                                                      <label class="next-label next-label--switch" for="exportCustomersType-current-search"><span id="customers-match-current-search"></span> khách hàng phù hợp với kết quả tìm kiếm hiện tại</label>
	                                                      <input type="radio" name="exportCustomersType" id="exportCustomersType-current-search" value="currentSearch" class="next-radio">
	                                                      <span class="next-radio--styled"></span>
	                                                  </div>
	                                          </fieldset>
	                                      </form>
	                                  </div>
	                              </div>
	                          </div>
	                          <div class="ui-modal__footer">
	                              <div class="ui-modal__footer-actions">
	                                  <div class="ui-modal__secondary-actions">
	                                      <div class="button-group">
	                                          <button class="ui-button close-modal" data-dismiss="modal" type="button" name="button">Hủy</button>
	                                      </div>
	                                  </div>
	                                  <div class="ui-modal__primary-actions">
	                                      <div class="button-group button-group--right-aligned">
	                                          <button class="ui-button ui-button--primary js-btn-loadable has-loading" type="button" name="commit" bind-event-click="exportCustomers()">Xuất danh sách khách hàng</button>
	                                      </div>
	                                  </div>
	                              </div>
	                          </div>
	                      </div>
	                  </div>
	               </script>
	               <script class="modal_source" define="{importCustomersModal: new Bizweb.Modal(this)}" type="text/html">
	                  <div class="modal-dialog">
	                      <div class="modal-content">
	                          <div class="ui-modal__header">
	                              <div class="ui-modal__header-title">
	                                  <h2 class="ui-title">Nhập danh sách khách hàng</h2>
	                              </div>
	                              <div class="ui-modal__header-actions">
	                                  <div class="ui-modal__close-button">
	                                      <button class="ui-button ui-button--transparent close-modal" data-dismiss="modal" aria-label="Close modal" type="button" name="button"><svg class="next-icon next-icon--color-ink-lighter next-icon--size-20"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#cancel-small-minor"></use> </svg></button>
	                                  </div>
	                              </div>
	                          </div>
	                          <div class="ui-modal__body">
	                              <div class="ui-modal__section">
	                                  <div>
	                                      <form id="form-import-customers" action="/admin/customers/import" method="post" enctype="multipart/form-data">
	                                          <input type="hidden" name="AuthenticityToken" value="1NTfhwE3AqACqIAea4ejCWUWtEdJ4s9sKR58es2volbF5cXu7Rcl9wildxVE7ROczHbGv03eRUDPvcHDiX7McQ==" />
	                                          <input name="importCustomersFile" type="file" size="30" id="importCustomersFileUpload" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" bind-event-change="enableButtonImport()" />
	                                          <div class="next-input-wrapper">
	                                              <label class="next-label" style="margin-left:25px" for="overrideExistCustomers">Ghi đè thông tin các khách hàng đã có</label>
	                                              <input class="next-checkbox" id="overrideExistCustomers" name="overrideExistCustomers" type="checkbox" value="true" /><input name="overrideExistCustomers" type="hidden" value="false" />
	                                              <span class="next-checkbox--styled"><svg class="next-icon next-icon--size-10 checkmark"> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-checkmark-thick"></use> </svg></span>
	                                          </div>
	                                      </form>
	                                  </div>
	                              </div>
	                          </div>
	                          <div class="ui-modal__footer">
	                              <div class="ui-modal__footer-actions">
	                                  <div class="ui-modal__secondary-actions">
	                                      <div class="button-group">
	                                          <button class="ui-button close-modal" data-dismiss="modal" type="button" name="button">Hủy</button>
	                                      </div>
	                                  </div>
	                                  <div class="ui-modal__primary-actions">
	                                      <div class="button-group button-group--right-aligned">
	                                          <button class="ui-button ui-button--primary js-btn-loadable has-loading" type="button" name="commit" bind-event-click="importCustomers()">Nhập danh sách khách hàng</button>
	                                      </div>
	                                  </div>
	                              </div>
	                          </div>
	                      </div>
	                  </div>
	               </script>
	            </div>
	            <div class="modal fade" id="bizweb-modal" data-width="" tabindex="-1" role="dialog"></div>
	            <div class="modal" data-tg-refresh="modal" id="modal_container" style="display: none;" aria-hidden="true" aria-labelledby="ModalTitle" tabindex="-1"></div>
	            <div class="modal-bg" data-tg-refresh="modal" id="modal_backdrop"></div>
	         </div>
	      </div>
	   </div>
	</main>
@stop
