<?php

Route::group([ 'middleware' => [ 'web','CheckAdmin' ], 'prefix' => 'customer', 'namespace' => 'Modules\Customer\Http\Controllers'], function(){

	Route::get('/',['as'=>'getIndex','uses'=>'CustomerController@getIndex']);
	Route::get('/create',['as'=>'getCreate','uses'=>'CustomerController@getCreate']);
	Route::get('/edit',['as'=>'getEdit','uses'=>'CustomerController@getEdit']);
	
});