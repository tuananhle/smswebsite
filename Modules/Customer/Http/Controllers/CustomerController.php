<?php

namespace Modules\Customer\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class CustomerController extends Controller
{
   public function getIndex()
   {
       return view('customer::index');
   }
   public function getCreate()
   {
       return view('customer::create');
   }
   public function getEdit()
   {
        return view('customer::edit');
   }
}
