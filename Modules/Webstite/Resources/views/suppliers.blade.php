@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
	<div id="banner">
    <!--breadcrumbs-->
    
    <!--End-breadcrumbs-->
    <!--Action boxes-->
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="widget-box" style="max-width: 600px;">
                <div class="widget-title"> <span class="icon"><i class="icon-ok"></i></span>
                    <h1> Cài Đặt Đối Tác </h1>
                </div>
                <br>
                <div class="widget-content">
                    <form class="form-horizontal form-material" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-group">
                            @if(count($suppliers) > 0)
                                @foreach($suppliers as $key => $picture)
                                    <div class="box-item-01">
                                        <div class="form-group item-img" style="position: relative;clear: both;">
                                            <input type="hidden" class="inp-id" name="id_bn[]" value="{{ $picture->id }}">
                                            @if($picture->Spli_images != '' && file_exists('uploads/imageTaitro/'.$picture->Spli_images))
                                                <img src="{{ asset('uploads/imageTaitro/'.$picture->Spli_images) }}" style="max-width: 100%; display: block">
                                            @else
                                                <img src="{{ asset('img/no-image.png') }}" style="max-width: 100%; display: block" class="aspect-ratio__content">
                                            @endif
                                                <input type="file" name="taitro[]" class="hide" onchange="changeImg(this)" multiple="multiple">
                                                <span class="btn btn-sm btn-info" onclick="$(this).prev().click()"> Chọn ảnh </span>
                                            
                                            @if($key > 0)
                                                <span class="btn btn-sm btn-danger" onclick="deleteDom(this)" style="position: absolute;top: -15px;right: 0;">xóa</span>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            @else
                            <div class="box-item-01">
                                <div class="form-group item-img" style="position: relative;clear: both;">
                                    <img src="{{ asset('img/no-image.png') }}" style="max-width: 100%; display: block" class="aspect-ratio__content">
                                    <input type="file" name="taitro[]" class="hide" onchange="changeImg(this)" multiple="multiple">
                                    <span class="btn btn-sm btn-info" onclick="$(this).prev().click()"> Chọn ảnh </span>
                                            
                                </div>
                            </div>
                            @endif
                            <span class="btn btn-sm btn-warning btn-btn--002" style="margin: 15px; float: right" onclick="appendDivSelectImg(this)">
                                <i class="mdi mdi-plus"></i> Thêm ảnh
                            </span>
                        </div>
                        <div class="form-group" style="clear: both">
                            <div class="col-sm-12">
                                <button class="btn btn-success" style="background: #263a53; color: white;">Xác nhận</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
	<script type="text/javascript">
        checkNumDom();

        function changeImg(input){
            if(input.files && input.files[0]){
                var reader = new FileReader();
                reader.onload = function(e){
                    $(input).prev().attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function appendDivSelectImg(that) {
            $(that).prev('.box-item-01').append('<div class="form-group item-img" style="position: relative;clear: both;">\
                                        {{-- @if(isset($picture) && file_exists('images/'.$picture->lg_image))
                                            <img src="{{ asset('images/'.$picture->lg_image) }}" style="max-width: 100%; display: block">
                                        @else --}}\
                                            <img src="{{ asset('img/no-image.png') }}" style="max-width: 100%; display: block" class="aspect-ratio__content">\
                                        {{-- @endif --}}\
                                            <input type="file" name="taitro[]" class="hide" onchange="changeImg(this)" multiple="multiple">\
                                           <span class="btn btn-sm btn-info" onclick="$(this).prev().click()"> Chọn ảnh </span>\
                                            <span class="btn btn-sm btn-danger" onclick="deleteDom(this)" style="position: absolute;top: -15px;right: 0;">xóa</span>\
                                    </div>');
            checkNumDom();
        }
        function deleteDom(that) {
            $(that).parent('.item-img').remove();
            $(that).remove();
            checkNumDom();
        }
        $(document).on('click', '.span-click-upload-img' , function () {
            $(this).prev().click();
        })
        function checkNumDom() {
            if($('.item-img').length >= 6)
            {
                $('.btn-btn--002').addClass('hide');
            }
            else
            {
                $('.btn-btn--002').removeClass('hide');
            }
        }
    </script>

@endsection	