<?php
Route::group([ 'middleware' => [ 'web','CheckAdmin' ], 'prefix' => 'admin/website', 'namespace' => 'Modules\Webstite\Http\Controllers'], function(){

    Route::get('/banner','WebstiteController@getBanner');
    Route::post('/banner','WebstiteController@postBanner');
    Route::get('/taitro','WebstiteController@getTaitro');
    Route::post('/taitro','WebstiteController@postTaitro');
});
