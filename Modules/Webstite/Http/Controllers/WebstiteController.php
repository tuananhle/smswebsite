<?php

namespace Modules\Webstite\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Webstite\Entities\Banner;
use Modules\Webstite\Entities\Supplier;
use DB;

class WebstiteController extends Controller
{
   public function getBanner()
   {
        $data['banner'] = Banner::orderby('id', 'DESC')->limit(6)->get();
        return view('webstite::banner',$data);
   }
   public function getTaitro()
   {
        $data['suppliers'] = Supplier::orderby('id', 'DESC')->limit(6)->get();
        return view('webstite::suppliers',$data);
   }
   public function postBanner(Request $request)
    {
        $getID = $request->id_bn;
        if ($getID == null) {
            DB::table('banner')->delete();
        }
        elseif (count($getID) > 0) {
            DB::table('banner')->whereNotIn('id', $getID)->delete();
        }
        if($files = $request->file('avatar'))
        {
            foreach($files as $file)
            {
                $name = rand().$file->getClientOriginalName();
                $file->move('uploads/imageBanner',$name);
                DB::table('banner')->insert([ 'bn_images' => $name ]);
            }
        }
        return back()->with('success','Cập nhật banner thành công');
    }
    public function postTaitro(Request $request)
    {
        $getID = $request->id_bn;
        if ($getID == null) {
            DB::table('suppliers')->delete();
        }
        elseif (count($getID) > 0) {
            DB::table('suppliers')->whereNotIn('id', $getID)->delete();
        }
        if($files = $request->file('taitro'))
        {
            foreach($files as $file)
            {
                $name = rand().$file->getClientOriginalName();
                $file->move('uploads/imageTaitro',$name);
                DB::table('suppliers')->insert([ 'Spli_images' => $name ]);
            }
        }
        return back()->with('success','Cập nhật nhà tài trợ thành công');
    }
}
