<?php

Route::group([ 'middleware' => [ 'web','CheckAdmin' ], 'prefix' => 'admin/settings', 'namespace' => 'Modules\Setting\Http\Controllers'], function(){

	Route::get('/create', 'SettingController@getCreate');
	Route::post('/create', 'SettingController@postCreate');

	Route::get('/', 'SettingController@getDoarboad');

	Route::get('/addresses', 'SettingAddressController@getAddresses');
	Route::post('/addresses', 'SettingAddressController@postAddresses');
	Route::post('/addresses/delete/{id}', 'SettingAddressController@delAdShop');
});