<?php

namespace Modules\Setting\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Setting\Entities\AddressSetting;

class SettingAddressController extends Controller
{
    public function getAddresses()
    {
    	$data['countProduct'] = AddressSetting::count();
    	$data['allProduct']   = AddressSetting::all();
        return view('setting::pages.address', $data);
    }
    public function postAddresses(Request $request)
    {

    	if ($request->next_card__inp > 0)
    	{
    		$idAdShop  = $request->next_card__inp;
    		$arrUpdate = [
    			'name_as' 		=>	$request->Name,
			    'email_as'		=>	$request->Email,
			    'phone_as'		=>	$request->Phone,
			    'address_as'	=>	$request->Address,
			    'city_as'		=>	$request->City,
			    'districtId_as' =>	$request->District,
    		];
    		$update = AddressSetting::where('id_as', $idAdShop)->update($arrUpdate);
    		if($update) {
		    	return back()->with('success','Cập nhật thành công');
		    }
		    else {
		    	return back()->with('error', 'Cập nhật thất bại');
		    }
    	}
    	else
    	{
    		$addressShop = new AddressSetting;
    		$addressShop->name_as 		=	$request->Name;
		    $addressShop->email_as		=	$request->Email;
		    $addressShop->phone_as		=	$request->Phone;
		    $addressShop->address_as	=	$request->Address;
		    $addressShop->city_as		=	$request->City;
		    $addressShop->districtId_as =	$request->District;
		    $addressShop->save();
		    if($addressShop->id_as) {
		    	return back()->with('success','Cập nhật thành công');
		    }
		    else {
		    	return back()->with('error', 'Cập nhật thất bại');
		    }
    	}
    }
    public function delAdShop($id)
    {
    	AddressSetting::where('id_as', $id)->delete();
    }
}