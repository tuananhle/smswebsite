<?php

namespace Modules\Setting\Http\Controllers;


use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Setting\Entities\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DB,File;

class SettingController extends Controller
{
    public function getDoarboad()
    {
        return view('setting::pages.doarboad');
    }
    public function getCreate()
    {
    	$data['setting'] = Setting::first();
        return view('setting::pages.create',$data);
    }
    public function postCreate(Request $request)
    {
        if (Setting::count() < 1 )
        {
            $file_name = $request->file('favicon')->getClientOriginalName();
        	$logo = $request->file('logo')->getClientOriginalName();
        	$setting = new Setting();
        
            $setting->title   = $request->title;
	    	$setting->webname = $request->webname;
	    	$setting->company = $request->company;
	    	$setting->address_one = $request->address1;
	    	$setting->address_two = $request->address2;
	    	$setting->phone1 = $request->phone1;
	    	$setting->phone2 = $request->phone2;
	    	$setting->fax = $request->fax;
            $setting->email  = $request->email;
	    	$setting->email2 = $request->email2;
	    	$setting->facebook = $request->facebook;
            $setting->google   = $request->google;
            $setting->GA = $request->GA;
            $setting->fbPixel  = $request->fbPixel;
	    	$setting->iframe_map  = $request->iframe_map;
	    	$setting->keywords = $request->keywords;
	    	$setting->favicon  = $file_name;
            $request->file('favicon')->move('uploads/image/',$file_name);
	    	$request->file('logo')->move('uploads/image/',$logo);
	    	$setting->save();
        }else {

        	$tableP = DB::table('settings');

        	$image = $tableP->select('favicon')->first()->favicon;
        	$img_hientai = public_path('uploads/image').'/'. $image;

            $image_logo = $tableP->select('logo')->first()->logo;
            $img_hientai_logo = public_path('uploads/image').'/'. $image_logo;

        	$arrUpdate = [
        		'title' => $request->input('title'), 
                'company' => $request->company, 
        		'webname' => $request->webname, 
        		'address_one' => $request->address1, 
        		'address_two' => $request->address2, 
        		'phone1' => $request->phone1, 
        		'phone2' => $request->phone2, 
        		'fax' => $request->fax, 
                'email' => $request->email, 
        		'email2' => $request->email2, 
        		'facebook' => $request->facebook, 
                'google' => $request->google, 
                'GA' => $request->GA, 
                'fbPixel' => $request->fbPixel, 
        		'iframe_map' => $request->iframe_map, 
        		'keywords' => $request->keywords, 
        	];

        	$tableP->update($arrUpdate);

            if (!empty(Input::hasFile('favicon')))
            {
                $file_name = rand().Input::file('favicon')->getClientOriginalName();
                
                $tableP->update([ 
                    'favicon' => $file_name, 
                     
                ]);
                Input::file('favicon')->move('uploads/image/',$file_name);
               
                if (File::exists($img_hientai) )
                {
                    File::delete($img_hientai);
                   
                }
            }else {
                echo 'ko có';
            }

        	if ( !empty(Input::hasFile('logo')))
        	{
               
	            $logo = rand().Input::file('logo')->getClientOriginalName();
	            $tableP->update([ 
                    
        			'logo' => $logo, 
        		]);
                
	            Input::file('logo')->move('uploads/image/',$logo);
	            if (File::exists($img_hientai_logo) )
	            {
	                File::delete($img_hientai_logo);
	            }
	        }else {
	            echo 'ko có';
	        }
        }
        return back()->with('success','thành công');
    }
   
}
