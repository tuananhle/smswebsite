<?php

namespace Modules\Setting\Entities;

use Illuminate\Database\Eloquent\Model;

class AddressSetting extends Model
{
	protected $table 	= 'address_setting';
	protected $primaryKey = 'id_as';
    protected $fillable = [];
}
