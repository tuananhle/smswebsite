@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Cấu hình địa chỉ cửa hàng')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div class="address-setting" id="address-setting-content">
                    <header class="ui-title-bar-container">
                        <div class="ui-title-bar ui-title-bar--separator">
                            <div class="ui-title-bar__navigation">
                                <div class="ui-breadcrumbs">
                                    <a href="/admin/settings" class="ui-button ui-button--transparent ui-breadcrumb">
                                        <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left-thinner"></use>
                                        </svg>
                                        <span class="ui-breadcrumb__item">Cấu hình</span>
                                    </a>
                                </div>
                            </div>
                            <div class="ui-title-bar__main-group">
                                <div class="ui-title-bar__heading-group">
                                    <span class="ui-title-bar__icon">
                                        <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-notifications"></use>
                                        </svg>
                                    </span>
                                    <h1 class="ui-title-bar__title"> Cấu hình địa chỉ cửa hàng </h1>
                                </div>
                            </div>
                        </div>
                    </header>
                    <section class="ui-annotated-section-container">
                        <div class="ui-annotated-section email-template">
                            <div class="ui-annotated-section__annotation">
                                <div class="ui-annotated-section__title">
                                    <h2 class="ui-heading"> Địa chỉ cửa hàng </h2>
                                </div>
                                <div class="ui-annotated-section__description">
                                    <p>
                                        Quản lý địa chỉ cửa hàng, chi nhánh, kho hàng giúp chúng tôi gửi đơn hàng của bạn đến nhà vận chuyển nhanh gọn và chính xác hơn.
                                    </p>
                                    <a href="javascript::void(0)" onclick="addAddress()" class="ui-button" id="ht-store-address">Thêm địa chỉ cửa hàng</a>
                                </div>
                            </div>
                            <div class="ui-annotated-section__content" id="box_item__section_nva01">
                                @if($countProduct > 0)
                                    @foreach($allProduct as $key => $itemProduct)
                                        <section class="ui-card">
                                            <div class="next-card__section">
                                                <div class="ui-modal__section">
                                                    <form method="post">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="next_card__inp" value="{{ $itemProduct->id_as }}">
                                                        <div class="ui-form__section">
                                                            <div class="next-input-wrapper">
                                                                <label class="ui-label" for="Name"> Tên địa điểm @if($key == 0) ( Cửa hàng chính ) @endif</label>
                                                                <input name="Name" class="next-input" required placeholder="Nhập Tên địa điểm" value="{{ $itemProduct->name_as }}">
                                                            </div>
                                                            <div class="ui-form__group">
                                                                <div class="next-input-wrapper">
                                                                    <label class="ui-label" for="Email"> Email </label>
                                                                    <input name="Email" class="next-input" placeholder="Nhập email" value="{{ $itemProduct->email_as }}">
                                                                </div>
                                                                <div class="next-input-wrapper">
                                                                    <label class="ui-label" for="Phone"> Điện thoại </label>
                                                                    <input name="Phone" class="next-input" placeholder="Nhập điện thoại" value="{{ $itemProduct->phone_as }}">
                                                                </div>
                                                            </div>
                                                            <div class="next-input-wrapper">
                                                                <label class="ui-label" for="Address"> Địa chỉ </label>
                                                                <input name="Address" class="next-input" required placeholder="Nhập địa chỉ" value="{{ $itemProduct->address_as }}">
                                                            </div>
                                                            <div class="ui-form__group">
                                                                <div class="next-input-wrapper">
                                                                    <label class="control-label" for="City"> Tỉnh / Thành phố </label>
                                                                    <input class="next-input" name="City" required placeholder="Nhập Tỉnh / Thành phố" value="{{ $itemProduct->city_as }}">
                                                                </div>
                                                                <div bind-show="have_city" class="next-input-wrapper">
                                                                    <label class="control-label" for="District"> Quận huyện </label>
                                                                    <input class="next-input" name="District" required placeholder="Nhập Quận huyện" value="{{ $itemProduct->districtId_as }}">
                                                                </div>
                                                            </div>
                                                            <div class="next-card__section" style="padding:8px">
                                                                <div class="ui-stack ui-stack--wrap">
                                                                    <div class="ui-stack-item ui-stack-item--fill">
                                                                        <button class="ui-button"> Cập nhật </button>
                                                                    </div>
                                                                    @if($key > 0 )
                                                                        <div class="ui-stack-item">
                                                                            <span data-id="{{ $itemProduct->id_as }}" class="ui-button btn-destroy" onclick="deleteAddressModal(this)" aria-label="Delete address" type="button" name="button"> Xóa địa chỉ </span>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </section>
                                    @endforeach
                                @else
                                    <section class="ui-card">
                                        <div class="next-card__section">
                                            <div class="ui-modal__section">
                                                <form method="post">
                                                    {{ csrf_field() }}
                                                    <div class="ui-form__section">
                                                        <div class="next-input-wrapper">
                                                            <label class="ui-label" for="Name"> Tên địa điểm ( Cửa hàng chính ) </label>
                                                            <input name="Name" class="next-input" placeholder="Nhập Tên địa điểm">
                                                        </div>
                                                        <div class="ui-form__group">
                                                            <div class="next-input-wrapper">
                                                                <label class="ui-label" for="Email"> Email </label>
                                                                <input name="Email" class="next-input" placeholder="Nhập email">
                                                            </div>
                                                            <div class="next-input-wrapper">
                                                                <label class="ui-label" for="Phone"> Điện thoại </label>
                                                                <input name="Phone" class="next-input" placeholder="Nhập điện thoại">
                                                            </div>
                                                        </div>
                                                        <div class="next-input-wrapper">
                                                            <label class="ui-label" for="Address"> Địa chỉ </label>
                                                            <input name="Address" class="next-input" placeholder="Nhập địa chỉ">
                                                        </div>
                                                        <div class="ui-form__group">
                                                            <div class="next-input-wrapper">
                                                                <label class="control-label" for="City"> Tỉnh / Thành phố </label>
                                                                <input class="next-input" name="City" placeholder="Nhập Tỉnh / Thành phố">
                                                            </div>
                                                            <div bind-show="have_city" class="next-input-wrapper">
                                                                <label class="control-label" for="District"> Quận huyện </label>
                                                                <input class="next-input" name="District" placeholder="Nhập Quận huyện">
                                                            </div>
                                                        </div>
                                                        <div class="next-card__section" style="padding:8px">
                                                            <div class="ui-stack ui-stack--wrap">
                                                                <div class="ui-stack-item">
                                                                    <button class="ui-button"> Cập nhật </button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
    										</div>
                                        </div>
                                    </section>
                                @endif
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
	function addAddress()
    {
        $html = '<section class="ui-card">\
                    <div class="next-card__section">\
                        <div class="ui-modal__section">\
                            <form method="post">\
                                {{ csrf_field() }}\
                                <div class="ui-form__section">\
                                    <div class="next-input-wrapper">\
                                        <label class="ui-label" for="Name"> Tên địa điểm </label>\
                                        <input name="Name" class="next-input" placeholder="Nhập Tên địa điểm">\
                                    </div>\
                                    <div class="ui-form__group">\
                                        <div class="next-input-wrapper">\
                                            <label class="ui-label" for="Email"> Email </label>\
                                            <input name="Email" class="next-input" placeholder="Nhập email">\
                                        </div>\
                                        <div class="next-input-wrapper">\
                                            <label class="ui-label" for="Phone"> Điện thoại </label>\
                                            <input name="Phone" class="next-input" placeholder="Nhập điện thoại">\
                                        </div>\
                                    </div>\
                                    <div class="next-input-wrapper">\
                                        <label class="ui-label" for="Address"> Địa chỉ </label>\
                                        <input name="Address" class="next-input" placeholder="Nhập địa chỉ">\
                                    </div>\
                                    <div class="ui-form__group">\
                                        <div class="next-input-wrapper">\
                                            <label class="control-label" for="City">Tỉnh / Thành phố</label>\
                                            <input class="next-input" name="City" placeholder="Nhập Tỉnh / Thành phố">\
                                        </div>\
                                        <div bind-show="have_city" class="next-input-wrapper">\
                                            <label class="control-label" for="District">Quận huyện</label>\
                                            <input class="next-input" name="District" placeholder="Nhập Quận huyện">\
                                        </div>\
                                    </div>\
                                    <div class="next-card__section" style="padding:8px">\
                                        <div class="ui-stack ui-stack--wrap">\
                                            <div class="ui-stack-item ui-stack-item--fill">\
                                                <button class="ui-button"> Cập nhật </button>\
                                            </div>\
                                            <div class="ui-stack-item">\
                                                <span data-id=" " class="ui-button btn-destroy" onclick="deleteAddressModal(this)" aria-label="Delete address" type="button" name="button">\
                                                    Xóa địa chỉ\
                                                </span>\
                                            </div>\
                                        </div>\
                                    </div>\
                                </div>\
                            </form>\
                        </div>\
                    </div>\
                </section>';
		$('#box_item__section_nva01').append($html);
	}

    function deleteAddressModal(that)
    {
        $(that).parents('.ui-card').remove();
        var id_adshop = $(that).data('id');
        if (id_adshop != '')
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                type: "POST",
                url: "/admin/settings/addresses/delete/"+id_adshop,
                contentType: false,
                processData: false,
                success: function(data) {
                    console.log('Xóa thành công');
                }
            });
        }
    }
</script>
@endsection