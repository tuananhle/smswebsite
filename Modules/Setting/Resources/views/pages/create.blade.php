@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div>
                    <form autocomplete="off" data-context="form" accept-charset="UTF-8" method="post" action="" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <header class="ui-title-bar-container">
                            <div class="ui-title-bar ui-title-bar--separator">
                                <div class="ui-title-bar__navigation">
                                    <div class="ui-breadcrumbs">
                                        <a href="/admin/settings" class="ui-button ui-button--transparent ui-breadcrumb">
                                            <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left-thinner"></use>
                                            </svg>
                                            <span class="ui-breadcrumb__item">Cấu hình</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="ui-title-bar__main-group">
                                    <div class="ui-title-bar__heading-group">
                                        <span class="ui-title-bar__icon">
                                            <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-settings"></use>
                                            </svg>
                                        </span>
                                        <h1 class="ui-title-bar__title">Cấu hình chung</h1>
                                    </div>
                                </div>
                            </div>
                            <div class="collapsible-header">
                                <div class="collapsible-header__heading"></div>
                            </div>
                        </header>
                        <section class="ui-annotated-section-container">
                            <div class="ui-annotated-section">
                                <div class="ui-annotated-section__annotation">
                                    <div class="ui-annotated-section__title">
                                        <h2 class="ui-heading">Thông tin website</h2>
                                    </div>
                                    <div class="ui-annotated-section__description">
                                        <p>Thông tin được sử dụng để chúng tôi và khách hàng liên hệ đến bạn.</p>
                                    </div>
                                </div>
                                <div class="ui-annotated-section__content">
                                    <div class="next-card">
                                        <div class="section-content">
                                            <div class="next-card__section">
                                                <div class="ui-form__section">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="Name">Tên website</label>
                                                        <input type="text" size="30" class="next-input"  name="webname" placeholder="Nhập tên website" value="{{ old('webname', isset($setting) ? $setting->webname : null) }}">
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="MetaTitle">Tiêu đề trang chủ</label>
                                                        <input type="text" size="30" class="next-input" name="title" placeholder="Nhập tiêu đề trang chủ" value="{{ old('title', isset($setting) ? $setting->title : null) }}">
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="MetaDescription">Mô tả trang chủ</label>
                                                        <textarea name="keywords" id="MetaDescription" class="next-input" placeholder="Nhập một mô tả để nâng cao xếp hạng trên công cụ tìm kiếm như Google.">{!! old('keywords', isset($setting) ? $setting->keywords : null) !!}</textarea>
                                                    </div>
                                                    <div class="ui-form__group">
                                                        <div class="next-input-wrapper">
                                                            <label class="next-label" for="Email">Email quản trị</label>
                                                            <input type="text" size="30" class="next-input" name="email" placeholder="Nhập email quản trị" value="{{ old('email', isset($setting) ? $setting->email : null) }}">
                                                            <p id="help-email" class="next-input__help-text">Email được sử dụng cho Sapo Web liên lạc với bạn.</p>
                                                        </div>
                                                        <div class="next-input-wrapper">
                                                            <label class="next-label" for="CustomerEmail">Email gửi thông báo</label>
                                                            <input type="text" size="30" class="next-input" name="email2" placeholder="Nhập email gửi thông báo" value="{{ old('email2', isset($setting) ? $setting->email2 : null) }}">
                                                            <p id="help-customerEmail" class="next-input__help-text">Email sử dụng để gửi thông báo và nhận liên hệ từ các khách hàng của bạn.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="ui-annotated-section-container">
                            <div class="ui-annotated-section">
                                <div class="ui-annotated-section__annotation">
                                    <div class="ui-annotated-section__title">
                                        <h2 class="ui-heading">Thông tin liên hệ</h2>
                                    </div>
                                    <div class="ui-annotated-section__description">
                                        <p>Thông tin được sử dụng trong các thông báo về đơn hàng và địa chỉ để liên hệ đến cửa hàng.</p>
                                    </div>
                                </div>
                                <div class="ui-annotated-section__content">
                                    <div class="next-card">
                                        <div class="section-content">
                                            <div class="next-card__section">
                                                <div class="ui-form__section">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="TradeName">Tên kinh doanh</label>
                                                        <input type="text" size="30" class="next-input" name="company" placeholder="Nhập tên kinh doanh" value="{{ old('company', isset($setting) ? $setting->company : null) }}">
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="PhoneNumber">Điện thoại</label>
                                                        <input type="text" size="30" class="next-input" name="phone1" placeholder="Nhập số điện thoại" value="{{ old('phone1', isset($setting) ? $setting->phone1 : null) }}">
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="PhoneNumber">Hotline</label>
                                                        <input type="text" size="30" class="next-input" name="phone2" placeholder="Nhập số hotline" value="{{ old('phone2', isset($setting) ? $setting->phone2 : null) }}">
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="PhoneNumber">Fax</label>
                                                        <input type="text" size="30" class="next-input" name="fax" placeholder="Nhập fax" value="{{ old('fax', isset($setting) ? $setting->fax : null) }}">
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="Address">Địa chỉ</label>
                                                        <textarea id="Address" name="address1" class="next-input" placeholder="Nhập địa chỉ liên hệ">{!! old('address1', isset($setting) ? $setting->address_one : null) !!}</textarea>
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="Address">Facebook</label>
                                                        <textarea id="Address" name="facebook" class="next-input" placeholder="Nhập facebook ">{!! old('facebook', isset($setting) ? $setting->facebook : null) !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="ui-annotated-section-container">
                            <div class="ui-annotated-section">
                                <div class="ui-annotated-section__annotation">
                                    <div class="ui-annotated-section__title">
                                        <h2 class="ui-heading">Google Analytics</h2>
                                    </div>
                                    <div class="ui-annotated-section__description">
                                        <p>Nhập mã Google Analytics để bạn có thể theo dõi các thống kê về truy cập của website.</p>
                                    </div>
                                </div>
                                <div class="ui-annotated-section__content">
                                    <div class="next-card">
                                        <div class="section-content">
                                            <div class="next-card__section">
                                                <div class="ui-form__section">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="GaCode">Mã Google Analytics</label>
                                                        <textarea class="next-input next-resize-vertical" cols="20" id="GaCode" name="GA" placeholder="Nhập mã Google Analytics tại đây" rows="3">{!! old('GA', isset($setting) ? $setting->GA : null) !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="ui-annotated-section-container">
                            <div class="ui-annotated-section">
                                <div class="ui-annotated-section__annotation">
                                    <div class="ui-annotated-section__title">
                                        <h2 class="ui-heading">Facebook Pixel</h2>
                                    </div>
                                    <div class="ui-annotated-section__description">
                                        <p>Facebook Pixel giúp bạn tạo chiến dịch quảng cáo để tìm khách hàng mới.</p>
                                    </div>
                                </div>
                                <div class="ui-annotated-section__content">
                                    <div class="next-card">
                                        <div class="section-content">
                                            <div class="next-card__section">
                                                <div class="ui-form__section">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="FacebookPixelId">Facebook Pixel ID</label>
                                                        <textarea class="next-input next-resize-vertical" cols="20" id="GaCode" name="fbPixel" placeholder="Nhập mã Facebook Pixel ID tại đây" rows="3">{!! old('fbPixel', isset($setting) ? $setting->fbPixel : null) !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="ui-annotated-section-container">
                            <div class="ui-annotated-section">
                                <div class="ui-annotated-section__annotation">
                                    <div class="ui-annotated-section__title">
                                        <h2 class="ui-heading">Iframe Map</h2>
                                    </div>
                                    <div class="ui-annotated-section__description">
                                        <p>Lưu ý:Chỉnh width trong iframe thành 100% để đẹp hơn</p>
                                    </div>
                                </div>
                                <div class="ui-annotated-section__content">
                                    <div class="next-card">
                                        <div class="section-content">
                                            <div class="next-card__section">
                                                <div class="ui-form__section">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="FacebookPixelId">Iframe Map</label>
                                                        <textarea class="next-input next-resize-vertical" cols="20" id="GaCode" name="iframe_map" placeholder="Nhập mã iframe_map tại đây" rows="3">{!! old('fbPixel', isset($setting) ? $setting->iframe_map : null) !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="ui-annotated-section-container">
                            <div class="ui-annotated-section">
                                <div class="ui-annotated-section__annotation">
                                    <div class="ui-annotated-section__title">
                                        <h2 class="ui-heading">Favicon</h2>
                                    </div>
                                    <div class="ui-annotated-section__description">
                                        <p>Biểu tượng bênh cạnh URL trên tab</p>
                                    </div>
                                </div>
                                <div class="ui-annotated-section__content">
                                    <div class="next-card">
                                        <div class="section-content">
                                            <div class="next-card__section">
                                                <div class="ui-form__section">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="FacebookPixelId"> Khuyến khích 16x16 </label>
                                                        <div class="form-group">
                                                            <input type="file" class="form hidden" name="favicon" id="inp-favicon" value="{{ old('company', isset($setting) ? $setting->company : null) }}" onchange="onChangeImageType(this)">
                                                            @if(isset($setting->favicon) && $setting->favicon != '')
                                                                <img src="{{ asset('uploads/image/'.$setting->favicon) }}" onclick="$('#inp-favicon').click()" style="max-width:100px;display:block;cursor:pointer" id="img-setting-type">
                                                            @else
                                                                <img src="{{ asset('img/no-img.png') }}" onclick="$('#inp-favicon').click()" style="max-width:100px;display:block;cursor:pointer" id="img-setting-type">
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section class="ui-annotated-section-container">
                            <div class="ui-annotated-section">
                                <div class="ui-annotated-section__annotation">
                                    <div class="ui-annotated-section__title">
                                        <h2 class="ui-heading">Logo</h2>
                                    </div>
                                    <div class="ui-annotated-section__description">
                                        <p>Logo của website(Công ty)</p>
                                    </div>
                                </div>
                                <div class="ui-annotated-section__content">
                                    <div class="next-card">
                                        <div class="section-content">
                                            <div class="next-card__section">
                                                <div class="ui-form__section">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="FacebookPixelId">  </label>
                                                        <div class="form-group">
                                                            <input type="file" class="form hidden" name="logo" id="inp-logo" value="" onchange="onChangeImageLogo(this)">
                                                            @if(isset($setting->logo) && $setting->logo != '')
                                                                <img src="{{ asset('uploads/image/'.$setting->logo) }}" onclick="$('#inp-logo').click()" style="max-width:100px;display:block;cursor:pointer" id="img-setting-logo">
                                                            @else
                                                                <img src="{{ asset('img/no-img.png') }}" onclick="$('#inp-logo').click()" style="max-width:100px;display:block;cursor:pointer" id="img-setting-logo">
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="ui-page-actions">
                            <button type="submit" class="btn btn-primary"> Cập nhật </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function onChangeImageType(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    var srcImg  =  e.target.result;
                    $('#img-setting-type').attr({ 'src': srcImg });
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        function onChangeImageLogo(input)
        {
            if(input.files && input.files[0])
            {
                var reader = new FileReader();
                reader.onload = function(e)
                {
                    var srcImg  =  e.target.result;
                    $('#img-setting-logo').attr({ 'src': srcImg });
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
</main>
@endsection