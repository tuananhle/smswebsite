@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Cấu hình')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div class="page">
                    <header class="ui-title-bar-container">
                        <div class="ui-title-bar">
                            <div class="ui-title-bar__main-group">
                                <div class="ui-title-bar__heading-group">
                                    <h1 class="ui-title-bar__title">Cấu hình</h1>
                                </div>
                            </div>
                        </div>
                        <div class="collapsible-header">
                            <div class="collapsible-header__heading"></div>
                        </div>
                    </header>
                    <div class="ui-layout">
                        <div class="ui-layout__sections">
                            <style type="text/css">@supports(display:grid){@media screen and (min-width:1025px){.area-settings-nav{grid-template-rows:repeat(6,auto);}}@media screen and (min-width:1201px){.area-settings-nav{grid-template-rows:repeat(4,auto);}}}
                            </style>
                            <div class="ui-layout__section">
                                <div class="ui-layout__item">
                                    <section class="ui-card">
                                        <nav>
                                            <h2 class="helper--visually-hidden">Cấu hình</h2>
                                            <ul class="area-settings-nav">
                                                <li class="area-settings-nav__item">
                                                    <a class="area-settings-nav__action" href="{{ url('admin/settings/create') }}" aria-disabled="false">
                                                        <div class="area-settings-nav__media">
                                                            <svg class="next-icon next-icon--size-35 next-icon--no-nudge">
                                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-settings"></use>
                                                            </svg>
                                                        </div>
                                                        <div>
                                                            <p class="area-settings-nav__title">Cấu hình chung</p>
                                                            <p class="area-settings-nav__description">Xem và chỉnh sửa thông tin cửa hàng của bạn</p>
                                                        </div>
                                                    </a>
                                                </li>
                                                <li class="area-settings-nav__item">
                                                    <a class="area-settings-nav__action" href="/admin/settings/addresses" aria-disabled="false">
                                                        <div class="area-settings-nav__media">
                                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                        </div>
                                                        <div>
                                                            <p class="area-settings-nav__title">Địa chỉ cửa hàng</p>
                                                            <p class="area-settings-nav__description">Quản lý thông tin địa chỉ cửa hàng</p>
                                                        </div>
                                                    </a>
                                                </li>
                                            </ul>
                                        </nav>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection