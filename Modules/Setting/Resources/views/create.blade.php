@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div id="customers-create" class="page">
                    <script type="text/javascript">
                        Page();
                    </script>
                    <form autocomplete="off" class="address-form" method="post" enctype="multipart/form-data" >
                        {{ csrf_field() }}
                        <header class="ui-title-bar-container">
                            <div class="ui-title-bar">
                                <div class="ui-title-bar__navigation">
                                    <div class="ui-breadcrumbs">
                                    </div>
                                </div>
                                <div class="ui-title-bar__main-group">
                                    <div class="ui-title-bar__heading-group">
                                        <span class="ui-title-bar__icon">
                                            <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                                <use xlink:href="#next-customers"></use>
                                            </svg>
                                        </span>
                                        <h1 class="ui-title-bar__title">Cấu hình chung</h1>
                                    </div>
                                    <div data-define="{titleBarActions: new Bizweb.TitleBarActions(this)}" class="action-bar">
                                        <div class="ui-title-bar__mobile-primary-actions">
                                            <div class="ui-title-bar__actions">
                                                <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary ui-title-bar__action btn-primary has-loading" type="submit" name="save-customer" value="Submit" disabled="disabled">Lưu</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ui-title-bar__actions-group">
                                    <div class="ui-title-bar__actions">
                                        <button class="ui-button ui-button--primary js-btn-loadable js-btn-primary ui-title-bar__action btn-primary has-loading" type="submit" name="save-customer" value="Submit" disabled="disabled">Lưu</button>
                                    </div>
                                </div>
                            </div>
                            <div class="collapsible-header">
                                <div class="collapsible-header__heading">
                                </div>
                            </div>
                        </header>
                        <div class="ui-layout">
                            <div class="ui-layout__sections">
                                <div class="ui-layout__section ui-layout__section--primary">
                                    <div class="ui-layout__item" id="customer-overview">
                                        <section class="ui-card">
                                            <header class="ui-card__header">
                                                <h2 class="ui-heading">Thông tin cấu hình</h2>
                                            </header>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="ui-form__section">
                                                        <div class="ui-form__group">
                                                            <div class="next-input-wrapper">
                                                                <label class="next-label" for="LastName">Title</label>
                                                                <input type="text" class="form" name="title" placeholder="Name" value="{{ old('title', isset($setting) ? $setting->title : null) }}">
                                                            </div>
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Tên Công ty</label>
                                                            <input type="text" class="form" name="company" placeholder="Tên công ty" value="{{ old('company', isset($setting) ? $setting->company : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Địa chỉ 1</label>
                                                            <input type="text" class="form" name="address1" placeholder="Địa chỉ 1" value="{{ old('address1', isset($setting) ? $setting->address_one : null) }}" >
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Địa chỉ 2</label>
                                                            <input type="text" class="form" name="address2" placeholder="Địa chỉ 2" value="{{ old('address2', isset($setting) ? $setting->address_two : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Số điện thoại 1</label>
                                                            <input type="text" class="form" name="phone1" placeholder="Số điện thoại 1" value="{{ old('phone1', isset($setting) ? $setting->phone1 : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Số điện thoại 2</label>
                                                            <input type="text" class="form" name="phone2" placeholder="Số điện thoại 2" value="{{ old('phone2', isset($setting) ? $setting->phone2 : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label">Fax</label>
                                                            <input type="text" class="form" name="fax" placeholder="FAX" value="{{ old('fax', isset($setting) ? $setting->fax : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Email</label>
                                                            <input type="text" class="form" name="email" placeholder="email" value="{{ old('email', isset($setting) ? $setting->email : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Facebook</label>
                                                            <input type="text" class="form" name="facebook" placeholder="Facebook" value="{{ old('facebook', isset($setting) ? $setting->facebook : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Google</label>
                                                            <input type="text" class="form" name="google" placeholder="Google" value="{{ old('google', isset($setting) ? $setting->google : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Favion(khuyến khích 16pX16px)</label>
                                                            <span class="favi" onclick="$(this).next().click()">Chọn ảnh</span>
                                                            <input type="file" class="form hidden" name="favicon" placeholder="Favion" value="{{ old('company', isset($setting) ? $setting->company : null) }}" onchange="onChangeImageType(this)">
                                                            {{-- <img src="{{ asset('uploads/image/'.$setting->favicon) }}" style="max-width: 100px; display: block" id="img-setting-type"> --}}
                                                            <img src="{{ asset('uploads/img/no-img.png') }}" style="max-width: 100px; display: block" id="img-setting-type">
                                                        </div>
                                                       
                                                        <div class="next-input-wrapper" id="ht-cus-email">
                                                            <label class="next-label" >Keyword for SEO</label>
                                                            <input type="text" class="form" name="keyword" placeholder="Keyword for SEO" value="{{ old('keyword', isset($setting) ? $setting->keyword : null) }}" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ui-page-actions">
                            <button type="submit" class="btn btn-primary"> Cập nhật </button>
                        </div>
                    </form>
                    <div class="modal" data-tg-refresh="modal" id="modal_container" style="display: none;" aria-hidden="true" aria-labelledby="ModalTitle" tabindex="-1"></div>
                    <div class="modal-bg" data-tg-refresh="modal" id="modal_backdrop"></div>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    function onChangeImageType(input)
    {
        if(input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function(e)
            {
                var srcImg  =  e.target.result;
                $('#img-setting-type').attr({ 'src': srcImg });
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    
</script>
@endsection