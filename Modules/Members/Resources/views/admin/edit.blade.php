@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
    <main id="AppFrameMain" class="ui-app-frame__main">
        <div class="wrapper" id="wrapper">
            <div id="body" class="page-content clearfix" data-tg-refresh="body">
                <div id="content">
                    <div id="customers-create" class="page">
                        <form autocomplete="off" class="address-form" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <header class="ui-title-bar-container">
                                <div class="ui-title-bar">
                                    <div class="ui-title-bar__navigation">
                                        <div class="ui-breadcrumbs">
                                            <a href="/admin/members/list" class="ui-button ui-button--transparent ui-breadcrumb">
                                                <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                                    <use xlink:href="#chevron-left-thinner"></use>
                                                </svg>
                                                <span class="ui-breadcrumb__item"> Thành viên </span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="ui-title-bar__main-group">
                                        <div class="ui-title-bar__heading-group">
                                            <span class="ui-title-bar__icon">
                                                <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                                    <use xlink:href="#next-customers"></use>
                                                </svg>
                                            </span>
                                            <h1 class="ui-title-bar__title"> {{ $user->name }} </h1>
                                        </div>
                                    </div>
                                </div>
                            </header>
                            <style>
                                .fixtopli__nav:hover .product-photo-hover-overlay {
                                    opacity: 1;
                                }
                                @media(max-width:468px){
                                    ul.ui-type-container li {
                                        clear: both!important;
                                        display: block!important;
                                    }
                                    .fixtopli__nav {
                                        top:0!important;
                                    }
                                    .boxinfo__nva {
                                        width: 100%!important;
                                    }
                                    .boximg__nva {
                                        width: 87%!important;
                                    }
                                }
                            </style>
                            <div class="ui-layout">
                                <div class="ui-layout__sections">
                                    <div class="ui-layout__section ui-layout__section--primary">
                                        <div class="ui-layout__item" id="customer-overview">
                                            <section class="ui-card">
                                                <div class="ui-card__section">
                                                    <header class="ui-card__header">
                                                        <h2 class="ui-heading"> Thông tin cơ bản </h2>
                                                    </header>
                                                    <ul class="ui-type-container" style="list-style:none">
                                                        <li style="width:30%;display:inline-block" class="boximg__nva next-card__section">
                                                            <div class="fixtopli__nav next-upload-dropzone__wrapper" >
                                                                <div class="aspect-ratio aspect-ratio--square">
                                                                    @if($user->avatar)
                                                                        <img id="avatar__member__nva" class="aspect-ratio__content" src="{{ asset('uploads/images/members/'.$user->avatar) }}" style="width:90%;">
                                                                    @else
                                                                        <img id="avatar__member__nva" class="aspect-ratio__content" src="{{ asset('img/no-image.png') }}" style="max-width: 60px;">
                                                                    @endif
                                                                    <div class="product-photo-hover-overlay drag">
                                                                        <input type="hidden" name="img_old" value="{{ $user->avatar  }}">
                                                                        <div style="position:absolute;top:40%;width:100%;text-align:center;">
                                                                            <span class="btn btn-sm btn-info" onclick="$(this).next().click()"> Chọn ảnh </span>
                                                                            <input type="file" name="avatarImage" class="hide" onchange="fileInputAvatarImage(this)">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li style="display:inline-block;width:50%;margin-top:0" class="boxinfo__nva ui-form__section">
                                                            <div class="box__if_nva_ed">
                                                                <div class="ui-form__group">
                                                                    <div class="next-input-wrapper">
                                                                        <label class="next-label" for="LastName">Tên quản trị viên</label>
                                                                        <input type="text" class="next-input @if($errors->has('username')) {{ 'input-error' }} @endif" name="username" placeholder="Name" value="{{ $user->name }}">
                                                                        @if($errors->has('username'))<span style="color: #f33"> {{ $errors->first('username') }} </span>@endif
                                                                    </div>
                                                                </div>
                                                                <div class="next-input-wrapper" id="ht-cus-email">
                                                                    <label class="next-label" >Email</label>
                                                                    <input type="text" class="next-input @if($errors->has('email')) {{ 'input-error' }} @endif" name="email" placeholder="Email" value="{{ $user->email }}">
                                                                    @if($errors->has('email'))<span style="color: #f33"> {{ $errors->first('email') }} </span>@endif
                                                                </div>
                                                                <div class="next-input-wrapper" id="edt__psw_ads01">
                                                                    <p>Vai trò: </p>
                                                                </div>
                                                                <div class="next-input-wrapper">
                                                                    <label class="next-label next-label--switch" for="selector"> Quản trị viên </label>
                                                                    <input class="next-checkbox" id="selector_1" name="selector" type="radio" value="1" @if($user->level == 1) checked @endif>
                                                                    <span class="next-checkbox--styled">
                                                                        <svg class="next-icon next-icon--size-10 checkmark">
                                                                            <use xlink:href="#next-checkmark-thick"></use>
                                                                        </svg>
                                                                    </span>
                                                                </div>
                                                                <div class="next-input-wrapper">
                                                                    <label class="next-label next-label--switch" for="selector"> Nhân viên </label>
                                                                    <input class="next-checkbox" id="selector_2" name="selector" type="radio" value="2" @if($user->level == 2) checked @endif>
                                                                    <span class="next-checkbox--styled">
                                                                        <svg class="next-icon next-icon--size-10 checkmark">
                                                                            <use xlink:href="#next-checkmark-thick"></use>
                                                                        </svg>
                                                                    </span>
                                                                </div>
                                                                <div class="next-input-wrapper" id="edt__psw_ads01">
                                                                    <span style="cursor:pointer;color:#027dee;margin:18px 0 0;" onclick="editPassWordUser()"> Thay đổi mật khẩu </span>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="ui-page-actions">
                                <button type="submit" class="btn btn-primary"> Cập nhật </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <button type="button" class="hidden btn-open-edtp__nva02" data-toggle="modal" data-target="#edtp__nva02">Open 01</button>
    <div class="modal fade" id="edtp__nva02" role="dialog">
        <div class="modal-dialog" style="border-radius:3px;">
            <div class="modal-content" style="border-radius:3px;">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 style="font-weight:600;" class="modal-title">Thay đổi mật khẩu</h3>
                </div>
                <div class="modal-body">
                    <div class="next-input-wrapper" id="ht-cus-email">
                        <label class="next-label"> Mật khẩu cũ </label>
                        <input type="password" class="next-input err__ip" name="password_old" placeholder="Mật khẩu cũ" required="">
                        <span class="mes_err__ads01 spn__err" style="color: #f33"></span>
                    </div>
                    <div class="next-input-wrapper" id="ht-cus-email">
                        <label class="next-label"> Mật khẩu mới </label>
                        <input type="password" class="next-input err__ip" name="password_new" placeholder="Mật khẩu mới" required="">
                        <span class="mes_err__pns spn__err" style="color: #f33"></span>
                    </div>
                    <div class="next-input-wrapper" id="ht-cus-email">
                        <label class="next-label"> Nhập lại mật khẩu mới </label>
                        <input type="password" class="next-input err__ip" name="repassword_new" placeholder="Nhập lại mật khẩu mới" required="">
                        <span class="mes_err__rpns spn__err" style="color: #f33"></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <span class="btn btn-primary" onclick="updatePassword({{ $user->id }})"> Cập nhật </span>
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Hủy </button>
                </div>
            </div>
        </div>
    </div>
@endsection