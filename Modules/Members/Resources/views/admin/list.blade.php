@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Quản lý thành viên')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div id="products-new" class="product-detail page">
                    <header class="ui-title-bar-container">
                        <div class="ui-title-bar">
                            <div class="ui-title-bar__main-group">
                                <div class="ui-title-bar__heading-group">
                                    <span class="ui-title-bar__icon">
                                        <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-products"></use>
                                        </svg>
                                    </span>
                                    <h1 class="ui-title-bar__title"> Thành viên </h1>
                                </div>
                            </div>
                            <div class="ui-title-bar__actions-group">
                                <div class="ui-title-bar__actions">
                                    <a href="{{ url('admin/members/add') }}" class="ui-button ui-button--primary ui-title-bar__action">Thêm mới</a>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div class="ui-layout">
                        <div class="ui-layout__sections">
                            <div class="ui-layout__section">
                                <div class="ui-layout__item">
                                    <section class="ui-card">
                                        <div id="filterAndSavedSearch" context="filterer">
                                            <div class="next-tab__container ">
                                                <ul class="next-tab__list filter-tab-list" id="filter-tab-list" role="tablist" data-has-next-tab-controller="true">
                                                    <li class="filter-tab-item" data-tab-index="1">
                                                        <p class="filter-tab filter-tab-active show-all-items next-tab next-tab--is-active">Tất cả thành viên</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="ui-card__section has-bulk-actions collections" refresh="collections" id="collections-refresh">
                                            <div class="ui-type-container clearfix">
                                                <div class="table-wrapper" context="bulkActions">
                                                    <table id="price-rule-table" class="table-hover bulk-action-context expanded">
                                                        <thead>
                                                            <tr>
                                                                <th></th>
                                                                <th class="name"><span> Họ tên </span></th>
                                                                <th class="alias hidden-xs"><span> Email </span></th>
                                                                <th class="summary hidden-xs"><span>Chức vụ</span></th>
                                                                <th class="conditions"><span>Thao tác</span></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($user as $item)
                                                            <tr>
                                                                <td>
                                                                    <a data-nested-link-target="true" href="{{ route('getEdit',$item->id) }}">
                                                                        @if($item->avatar == '') <img src="/img/no-image.png" style="width:80px;" alt="{{ $item->name }}">
                                                                        @else <img src="{{ asset('uploads/images/members/'.$item->avatar) }}" alt="{{ $item->name }}" style="width:80px;">
                                                                        @endif
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a data-nested-link-target="true" href="{{ route('getEdit',$item->id) }}">
                                                                        {{ $item->name }}
                                                                    </a>
                                                                </td>
                                                                <td class="hidden-xs">
                                                                    <span>{{ $item->email }}</span>
                                                                </td>
                                                                <td class="hidden-xs">
                                                                    @if( $item->level == 1)
                                                                        {!! 'Quản trị viên' !!}
                                                                    @else
                                                                        {!! 'Nhân viên' !!}
                                                                    @endif
                                                                </td>
                                                                <td>
                                                                    <ul class="item-actions-list" style="list-style:none;display:inline-block;">
                                                                        <li>
                                                                            <a class="edit" href="{{ route('getEdit',$item->id) }}">
                                                                                <i class="fa fa-pencil" style="color: green;"></i>
                                                                                Sửa
                                                                            </a>
                                                                        </li>
                                                                        <li style="margin-left:15px">
                                                                            <a class="remove" href="{{ url('admin/members/del/'.$item->id) }}" onclick="return confirm('Bạn có muốn xóa?')" >
                                                                                <i class="fa  fa-trash-o " style="color: red;"></i>
                                                                                Xóa
                                                                            </a>
                                                                        </li>
                                                                    </ul>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="t-grid-pager-boder">
                                                    <div class="t-pager t-reset clearfix fix-margin-pager">
                                                        <div class="col-md-6 col-lg-6 hidden-xs hidden-sm no-padding">
                                                            <div class="t-status-text dataTables_info">
                                                                {{ $user->links() }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection