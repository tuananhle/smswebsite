<?php

namespace Modules\Members\Entities;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admins';
    protected $guarded 	= [];
}
