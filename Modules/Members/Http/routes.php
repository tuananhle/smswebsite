<?php

Route::group([ 'middleware' => [ 'web','CheckAdmin' ], 'prefix' => 'admin/members', 'namespace' => 'Modules\Members\Http\Controllers' ], function()
{
	
	Route::get('/add',['as'=>'getRegister','uses'=>'MembersController@getRegister']);
	Route::post('/add',['as'=>'postRegister','uses'=>'MembersController@postRegister']);
	Route::get('/list',['as'=>'getList','uses'=>'MembersController@getList']);
	Route::get('/del/{id}',['as'=>'getDel','uses'=>'MembersController@getDel']);
	Route::get('/edit/{id}',['as'=>'getEdit','uses'=>'MembersController@getEdit']);
	Route::post('/edit/{id}',['as'=>'postEdit','uses'=>'MembersController@postEdit']);
	Route::post('/editpass/{id}',['as'=>'postEditPass','uses'=>'MembersController@postEditPass']);

});
