<?php

namespace Modules\Members\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;


class adminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(Auth::guard('admin')->guest()){
            return redirect('admin/login');
        }
        return $next($request);
    }
}
