<?php
/**
 * Create by NVA - Anh Duc Software
 * Done - 00:09 17/08/2018
 */
namespace Modules\Members\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Redirect;
use Modules\Members\Entities\Admin;
use Session,Hash,Auth;
use Validator;


class MembersController extends Controller
{
    public function getRegister(){
        return view('members::admin.add');
    }
    public function postRegister(Request $request){
        $rules = [
            'username'  =>'required|unique:admins,name',
            'email'     =>'required|unique:admins,email',
            'password'  =>'required',
            'repassword'=>'required|same:password'
        ];
        $messages = [
            'username.required'  => 'Tên tài khoản không được để trống',
            'username.unique'    => 'Tên tài khoản đã tồn tại',
            'email.required'     => 'Email không được để trống',
            'email.unique'       => 'Email đã được sử dụng',
            'password.required'  => 'Mật khẩu không được để trống',
            'repassword.required'=> 'Vui lòng nhập lại mật khẩu',
            'repassword.same'    => 'Mật khẩu nhập lại không khớp'
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }

        $admin = new Admin();
        $admin->name = $request->username;
        $admin->password = Hash::make($request->password);
        $admin->email = $request->email;
        $admin->level = $request->selector;
        $name_avatar  = '';
        if($imgAvatar = $request->file('avatarImage')){
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $name_avatar = $nameAvatar;
        }
        else { $name_avatar = ''; }
        $admin->avatar = $name_avatar;
        $admin->save();
        if ($admin->id){
            if($imgAvatar){
                $imgAvatar->move('uploads/images/members', $nameAvatar);
            }
            return redirect('admin/members/list')->with('success', 'Thêm mới thành công!');
        }
        else{
            if ($name_avatar != ''){
                if (file_exists(public_path('uploads/images/members').'/'.$name_avatar)) {
                    unlink(public_path('uploads/images/members').'/'.$name_avatar);
                }
            }
            return Redirect::back()->with('error', 'Thêm mới thất bại. Vui lòng kiểm tra lại');
        }
    }
    public function getList(){
        $data['user'] = Admin::orderby('id', 'desc')->paginate(20);
        return view('members::admin.list',$data);
    }
    public function getEdit($id){
        $data['user'] = Admin::where('id',$id)->first();
        return view('members::admin.edit',$data);
    }
    public function postEdit(Request $request ,$id){
        $rules = [
            'username'  =>'required|unique:admins,name,'.$id.',id',
            'email'     =>'required|unique:admins,email,'.$id.',id'
        ];
        $messages = [
            'username.required'  => 'Tên tài khoản không được để trống',
            'username.unique'    => 'Tên tài khoản đã tồn tại',
            'email.required'     => 'Email không được để trống',
            'email.unique'       => 'Email đã được sử dụng'
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }
        $arrUpdate = [
            'name'      => $request->username,
            'email'     => $request->email,
            'level'     => $request->selector
        ];
        $imgAvatar = $request->file('avatarImage');
        if($imgAvatar){
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $arrUpdate['avatar'] = $nameAvatar;
        }
        $idAdmin = Admin::where('id', $id)->update($arrUpdate);
        if ($idAdmin){
            if($imgAvatar){
                $imgAvatar->move('uploads/images/members', $nameAvatar);
                if ($request->img_old != ''){
                    if (file_exists(public_path('uploads/images/members').'/'.$request->img_old)) {
                        unlink(public_path('uploads/images/members').'/'.$request->img_old);
                    }
                }
            }
            return redirect('admin/members/list')->with('success', 'Cập nhật thành công!');
        }
        else{
            if ($name_avatar != ''){
                if (file_exists(public_path('uploads/images/members').'/'.$name_avatar)) {
                    unlink(public_path('uploads/images/members').'/'.$name_avatar);
                }
            }
            return Redirect::back()->with('error', 'Cập nhật thất bại. Vui lòng kiểm tra lại');
        }
    }
    public function postEditPass(Request $request, $id){
        $pass_old    = $request->pass_old;
        $pass_new    = trim($request->pass_new);
        $re_pass_new = trim($request->re_pass_new);

        $idAdmin = Admin::where('id', $id)->first();
        if (Hash::check($pass_old, $idAdmin->password)){
            if ($pass_new == null){
                return \response()->json([
                    'code' => 400,
                    'error' => 'pass_new',
                    'messages' => 'Vui lòng nhập mật khẩu mới'
                ]);
            }
            if ($re_pass_new == null){
                return \response()->json([
                    'code' => 400,
                    'error' => 're_pass_new',
                    'messages' => 'Vui lòng nhập lại mật khẩu mới'
                ]);
            }
            else{
                if ($pass_new == $re_pass_new){
                    $updatePass = Admin::where('id', $id)->update([
                        'password' => Hash::make($pass_new),
                    ]);
                    if ($updatePass){
                        return \response()->json([
                            'code' => 200,
                            'messages' => 'Cập nhật mật khẩu thành công'
                        ]);
                    }
                    else{
                        return \response()->json([
                            'code' => 404,
                            'messages' => 'Lỗi không xác định'
                        ]);
                    }
                }
                else{
                    return \response()->json([
                        'code' => 400,
                        'error' => 're_pass_new',
                        'messages' => 'Mật khẩu không khớp'
                    ]);
                }
            }
        }
        else{
            return \response()->json([
                'code' => 403,
                'messages' => 'Mật khẩu cũ không đúng'
            ]);
        }
    }
    public function getDel($id){
        $data['admin'] = Admin::find($id);
        $data['admin']->delete($id);
        return redirect()->route('getList')->with('success', 'Xóa thành công');
    }
}