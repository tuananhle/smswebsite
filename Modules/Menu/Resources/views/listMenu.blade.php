@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Tổng quan')
@section('main')
	<main id="AppFrameMain" class="ui-app-frame__main">
	   <div class="wrapper" id="wrapper">
	      <div id="body" class="page-content clearfix" data-tg-refresh="body">
	         <div id="content">
	            <script type="text/javascript">
	               Page();
	            </script>
	            <header class="ui-title-bar-container">
	               <div class="ui-title-bar ui-title-bar--separator">
	                  <div class="ui-title-bar__main-group">
	                     <div class="ui-title-bar__heading-group">
	                        <span class="ui-title-bar__icon">
	                           <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
	                              <use xlink:href="#next-navigation"></use>
	                           </svg>
	                        </span>
	                        <h1 class="ui-title-bar__title">Menu</h1>
	                     </div>
	                     <div define="{titleBarActions: new Bizweb.TitleBarActions(this)}" class="action-bar">
	                        <div class="action-bar__item action-bar__item--link-container">
	                           
	                           
	                        </div>
	                        <div class="ui-title-bar__mobile-primary-actions">
	                           <div class="ui-title-bar__actions">
	                              <a href="/admin/links/create" class="ui-button ui-button--primary ui-title-bar__action">Thêm Menu</a>
	                           </div>
	                        </div>
	                     </div>
	                  </div>
	                  <div class="ui-title-bar__actions-group">
	                     <div class="ui-title-bar__actions">
	                        <a href="{{ url('admin/menu/create') }}" class="ui-button ui-button--primary ui-title-bar__action">Thêm Menu</a>
	                     </div>
	                  </div>
	               </div>
	               <div class="collapsible-header">
	                  <div class="collapsible-header__heading"></div>
	               </div>
	            </header>
	            <section class="ui-annotated-section-container">
	               <div class="ui-annotated-section">
	                  <div class="ui-annotated-section__annotation">
	                     <div class="ui-annotated-section__title">
	                        <h2 class="ui-heading">Menus</h2>
	                     </div>
	                     <div class="ui-annotated-section__description">
	                        <p class="text-muted">
	                           Danh sách menu sẽ giúp khách hàng duyệt web của bạn dễ dàng hơn.
	                        </p>
	                        <p class="text-muted">
	                           Bạn có thể áp dụng chúng cho giao diện của bạn trong
	                           <a href="javascript:;">Chỉnh sửa giao diện</a>.
	                        </p>
	                        <p class="text-muted">
	                           Danh sách menu còn có thể sử dụng để
	                           <a href="javascript:;" target="_blank" rel="noreferrer noopener">tạo menu đa cấp</a>
	                           cho trang web của bạn.
	                        </p>
	                     </div>
	                  </div>
	                  <div class="ui-annotated-section__content">
	                     <section class="ui-card menus__menu-structure">
	                        <div class="ui-card__header" bind-class="{ui_menu_empty: !show1915809}">
	                           <div class="ui-stack">
	                              <div class="ui-stack-item ui-stack-item--fill">
	                                 <h2 class="ui-heading">
	                                    Main menu
	                                 </h2>
	                              </div>
	                              <div class="ui-stack-item">
	                                 {{-- <a class="" href="/admin/links/1915809">Sửa menu</a>
	                                 <a class="display-toggle" href="javascript:void(0);" title="Ẩn/Hiện Menu" define="{show1915809: true}" bind-event-click="show1915809 = !show1915809">
	                                    <svg class="next-icon next-icon--size-16 next-icon--no-nudge next-icon--ani-rotate-270" bind-class="{'next-icon--ani-rotate-270': show1915809, 'next-icon--ani-rotate-90': !show1915809 }">
	                                       <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left-thinner-local"></use>
	                                    </svg>
	                                 </a> --}}
	                              </div>
	                           </div>
	                        </div>
	                        <div class="ui-card__section ui-card__section--type-subdued">
	                           <div class="ui-type-container" bind-show="show1915809">
	                              <div class="linklist-container" define="{linkListContainer1915809: new Bizweb.LinkListContainer(this, 1915809)}">
	                                 <form id="linkListForm1915809">
	                                    <ul class="link-list-group js-menu-resources ui-sortable--next menu__list-items ui-sortable">
	                                    	@foreach($menu as $item)
	                                       <li class="link-list-group-item js-menu-resource ui-sortable-handle" id="sortable_link_7755564" style="opacity: 1;">
	                                          <div class="ui-stack ui-stack--wrap ui-stack--spacing-none sortable-menu-item">
	                                             <div class="ui-stack-item ui-stack-item--fill menu-item ui-sortable__item ui-sortable__helper-visible">
	                                                <input name="Links[].Id" value="7755564" type="hidden">
	                                                <span class="ui-sortable__handle">
	                                                   <svg class="next-icon next-icon--color-slate-lighter next-icon--size-12 drag-handle">
	                                                      <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-drag-handle"></use>
	                                                   </svg>
	                                                </span>
	                                                <div class="menu-item-name ui-sortable__item ui-sortable__helper-visible">
	                                                   {{ $item->mn_name }}
	                                                </div>
	                                                <a id="btnGroupDrop7755564" href="{{ url('admin/menu/edit/'.$item->mn_id) }}" class="link-list-button">
	                                                Sửa
	                                                </a>
	                                             </div>
	                                          </div>
	                                       </li>
	                                       @endforeach
	                                    </ul>
	                                 </form>
	                              </div>
	                           </div>
	                        </div>
	                     </section>
	                  </div>
	               </div>
	            </section>
	           
	            <div class="modal" data-tg-refresh="modal" id="modal_container" style="display: none;" aria-hidden="true" aria-labelledby="ModalTitle" tabindex="-1"></div>
	            <div class="modal-bg" data-tg-refresh="modal" id="modal_backdrop"></div>
	            <div id="local-icon-symbols" data-tg-refresh="local-icon-symbols" data-tg-refresh-always="true" style="display: none;">
	               <svg xmlns="http://www.w3.org/2000/svg">
	                  <symbol id="chevron-left-thinner-local">
	                     <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
	                        <path d="M12 16c-.256 0-.512-.098-.707-.293l-5-5c-.39-.39-.39-1.023 0-1.414l5-5c.39-.39 1.023-.39 1.414 0s.39 1.023 0 1.414L8.414 10l4.293 4.293c.39.39.39 1.023 0 1.414-.195.195-.45.293-.707.293z"></path>
	                     </svg>
	                  </symbol>
	               </svg>
	            </div>
	         </div>
	      </div>
	   </div>
	</main>
@endsection