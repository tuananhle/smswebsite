<?php

namespace Modules\Menu\Entities;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menus';
	protected $primaryKey = 'mn_id';
    protected $guarded = [];
}
