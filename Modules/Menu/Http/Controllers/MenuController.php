<?php

namespace Modules\Menu\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Validator;
use Modules\Menu\Entities\Menu;

class MenuController extends Controller
{
   
    public function listMenu()
    {
        $data['menu'] = Menu::get();
        return view('menu::listMenu',$data);
    }
    public function getAdd()
    {
        return view('menu::create');
    }
    public function postAdd(Request $request)
    {
        $rules = [
            'Title'  => 'required|unique:menus,mn_name',
            'Alias' => 'required|unique:menus,mn_alias',
        ];
        $messages = [
            'Title.required'  => 'Không được để trống trường này',
            'Title.unique'    => 'Nhà cung cấp đã tồn tại',
            'Alias.unique'   => 'Đường dẫn đã tồn tại',
            'Alias.required' => 'Đường dẫn không được để trống',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }
        $menu = new Menu();
        $menu->mn_name = $request->Title;
        $menu->mn_content = $request->menuContent;
        $menu->mn_alias =$request->Alias;
        $menu->save();
        return redirect('/admin/menu');
    }
    public function getEdit($id)
    {
        $data['menuu'] = Menu::where('mn_id',$id)->first();
        return view('menu::edit',$data);
    }
    public function postEdit(Request $request, $id)
    {

        $rules = [
            'Title'  => 'required',
            'Alias' => 'required',
        ];
        $messages = [
            'Title.required'  => 'Không được để trống trường này',
            'Alias.required' => 'Đường dẫn không được để trống',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }
        $menu = Menu::find($id);
        $menu->mn_name = $request->Title;
        $menu->mn_content = $request->menuContent;
        $menu->mn_alias =$request->Alias;
        $menu->save();
        if ($menu->mn_id)
        {
            return back()->with('success','Thêm menu thành công');
        }
        else
        {
            return back()->with('error', 'Đăng menu không thành công');
        }
        return redirect('/admin/menu');
    }
}
