<?php

Route::group([ 'middleware' => [ 'web','CheckAdmin' ], 'prefix' => 'admin/menu', 'namespace' => 'Modules\Menu\Http\Controllers' ], function()
{
    Route::get('/', 'MenuController@listMenu');
    Route::get('/create', 'MenuController@getAdd');
    Route::post('/create', 'MenuController@postAdd');
    Route::get('/edit/{id}', 'MenuController@getEdit');
    Route::post('/edit/{id}', 'MenuController@postEdit');
});
