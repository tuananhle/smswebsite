<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCategoiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_categoies', function (Blueprint $table) {
            $table->increments('pc_id');
            $table->string('pc_name');
            $table->longText('pc_summary')->nullable();
            $table->string('pc_alias')->nullable();
            $table->string('pc_meta_title')->nullable();
            $table->longText('pc_meta_description')->nullable();
            $table->string('pc_avatar')->nullable();
            $table->integer('pc_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_categoies');
    }
}
