<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('pro_name');
            $table->longText('pro_content');
            $table->longText('pro_summary')->nullable();
            $table->text('pro_images')->nullable();
            $table->integer('pro_price')->nullable();  // giá sản phẩm 
            $table->integer('pro_com_price')->nullable();  // giá so sánh 
            $table->tinyInteger('pro_taxable')->default(0);  //Thuế VAT
            $table->string('pro_code')->nullable(); // Mã sản phẩm
            $table->string('pro_barcode')->nullable();  // Mã vạch sản phẩm
            $table->string('pro_qty')->nullable(); // Số lượng sản phẩm
            $table->tinyInteger('pro_policy')->default(0); // Cho phép đặt hàng khi hết hàng , 1 là có 0 là không
            $table->tinyInteger('pro_sta_shopping')->default(0);  //  trạng thái có vận chuyển hay k . có là 1 k có là 0
            $table->string('pro_mass')->default(0);  // Khối lượng
            $table->string('pro_size')->nullable();  // kích thước
            $table->string('pro_color')->nullable();  // màu sắc
            $table->string('pro_material')->nullable();  // chất liệu
            $table->tinyInteger('pro_status')->default(1);  // Trạng thái ẩn/hiện , 0 ẩn - 1 hiện
            $table->integer('pro_type')->unsigned(); // Loại sản phẩm
            $table->integer('pro_vendor')->unsigned();  // Nhà cung cấp
            $table->integer('pro_cate')->unsigned(); // Danh mục sản phẩm
            $table->string('pro_tags')->nullable();
            $table->string('pro_meta_title')->nullable();
            $table->string('pro_meta_des')->nullable();
            $table->string('pro_alias'); // Đường dẫn
            $table->unique('pro_code');
            $table->unique('pro_alias');
            $table->foreign('pro_type')->references('pt_id')->on('product_type')->onDelete('cascade');
            $table->foreign('pro_vendor')->references('pv_id')->on('product_vendor')->onDelete('cascade');
            $table->foreign('pro_cate')->references('pc_id')->on('product_categoies')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
