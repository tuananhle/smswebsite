<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTypeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_type', function (Blueprint $table) {
            $table->increments('pt_id');
            $table->string('pt_name');
            $table->integer('pt_cate')->nullable();
            $table->longText('pt_summary')->nullable();
            $table->string('pt_alias')->nullable();
            $table->string('pt_meta_title')->nullable();
            $table->longText('pt_meta_description')->nullable();
            $table->string('pt_avatar')->nullable();
            $table->integer('pt_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_type');
    }
}
