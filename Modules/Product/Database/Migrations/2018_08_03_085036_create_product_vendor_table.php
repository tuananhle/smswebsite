<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_vendor', function (Blueprint $table) {
            $table->increments('pv_id');
            $table->string('pv_name');
            $table->longText('pv_summary')->nullable();
            $table->string('pv_alias')->nullable();
            $table->string('pv_meta_title')->nullable();
            $table->longText('pv_meta_description')->nullable();
            $table->longText('pv_avatar')->nullable();
            $table->integer('pv_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_vendor');
    }
}
