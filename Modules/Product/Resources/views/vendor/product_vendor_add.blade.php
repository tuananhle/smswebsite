@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Nhà cung cấp')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div id="products-new" class="product-detail page">
                    <form autocomplete="off" data-context="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <header class="ui-title-bar-container">
                            <div class="ui-title-bar">
                                <div class="ui-title-bar__navigation">
                                    <div class="ui-breadcrumbs">
                                        <a href="/admin/products/vendor" class="ui-button ui-button--transparent ui-breadcrumb">
                                            <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left-thinner"></use>
                                            </svg>
                                            <span class="ui-breadcrumb__item"> Quay lại danh sách </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="ui-title-bar__main-group">
                                    <div class="ui-title-bar__heading-group">
                                        <span class="ui-title-bar__icon">
                                            <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-products"></use>
                                            </svg>
                                        </span>
                                        <h1 class="ui-title-bar__title"> Nhà cung cấp </h1>
                                    </div>
                                </div>
                            </div>
                        </header>
                        
                        <div class="ui-layout">
                            <div class="ui-layout__sections">
                                <div class="ui-layout__section ui-layout__section--primary">
                                    <div class="ui-layout__item">
                                        <section class="ui-card" id="product-form-container">
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="product-type-name"> Nhà cung cấp </label>
                                                        <input  name="product[vendor][name]" autofocus="autofocus" id="product-type-name" 
                                                                placeholder="Nhập nhà cung cấp" class="@if($errors->has('product.vendor.name')) {{ 'input-error' }} @endif next-input" 
                                                                size="30" type="text" onkeyup="metaTitle(this)" value="{{ old('product.vendor.name') }}">
                                                        @if($errors->has('product.vendor.name'))<span style="color: #f33"> {{ $errors->first('product.vendor.name') }} </span>@endif
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="product-type-summary"> Mô tả </label>
                                                        <textarea id="product-type-summary" class="next-input ckeditor" name="product[vendor][summary]">{!! old('product.vendor.summary') !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="ui-layout__item">
                                        <section class="ui-card">
                                            <header class="ui-card__header">
                                                <div class="ui-stack ui-stack--wrap">
                                                    <div class="ui-stack-item ui-stack-item--fill">
                                                        <h2 class="ui-heading">Xem trước kết quả tìm kiếm</h2>
                                                    </div>
                                                </div>
                                            </header>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="google-preview">
                                                        <span id="nva__google__title" class="google__title">tiêu đề</span>
                                                        <div class="google__url">
                                                            {{ url('/') }}/<span id="nva__google__alias">tieu-de</span>
                                                        </div>
                                                        <div class="google__description nva__google__description">Thẻ mô tả</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="ui-form__section">
                                                        <div class="next-input-wrapper">
                                                            <div class="ui-form__label-wrapper">
                                                                <label class="next-label" for="seo-title-tag">Thẻ tiêu đề</label>
                                                                <span class="type--subdued">Số ký tự đã dùng: <b id="ltn__title">0</b>/70</span>
                                                            </div>
                                                            <input id="seo-title-tag" class="next-input" size="30" type="text" name="product[vendor][metaTitle]" maxlength="70" onkeyup="metaTitle(this)">
                                                        </div>
                                                        <div class="next-input-wrapper">
                                                            <div class="ui-form__label-wrapper">
                                                                <label class="next-label" for="seo-description-tag">Thẻ mô tả</label>
                                                                <span class="type--subdued">Số ký tự đã dùng: <b id="seo_description_lnt">0</b>/320</span>
                                                            </div>
                                                            <textarea id="seo-description-tag" onkeyup="onMetaDescription(this)" class="ui-text-area" name="product[vendor][metaDescription]" maxlength="320" placeholder="Thẻ mô tả"></textarea>
                                                        </div>
                                                        <div class="ui-form__section">
                                                            <div class="next-input-wrapper">
                                                                <label class="next-label" for="object-handle">Đường dẫn / Alias</label>
                                                                <div class="next-input--stylized next-input--has-content @if($errors->has('product.vendor.alias')) {{ 'input-error' }} @endif">
                                                                    <span class="next-input__add-on next-input__add-on--before" style="padding-right:0">{{ url('/') }}/</span>
                                                                    <input value="{{ old('product.vendor.alias') }}" id="object-handle" onkeyup="onAliasSeo(this)" class="next-input next-input--invisible" size="30" type="text" name="product[vendor][alias]">
                                                                </div>
                                                                <span style="color: #f33"> {{ $errors->first('product.vendor.alias') }} </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <div class="ui-layout__section ui-layout__section--secondary">
                                    <div class="ui-layout__item">
                                        <div class="next-card">
                                            <header class="next-card__header">
                                                <h3 class="ui-heading">Trạng thái</h3>
                                            </header>
                                            <section class="next-card__section">
                                                <div class="visibility" id="PublishingPanel">
                                                    <div class="ui-form__section">
                                                        <div class="ui-form__element">
                                                            <fieldset class="ui-choice-list">
                                                                <ul>
                                                                    <li>
                                                                        <div class="next-input-wrapper">
                                                                            <label class="next-label next-label--switch" for="active-true">
                                                                                Hiển thị
                                                                            </label>
                                                                            <input type="radio" name="product[vendor][active]" id="active-true" value="1" class="next-radio" checked="checked">
                                                                            <span class="next-radio--styled"></span>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="next-input-wrapper">
                                                                            <label class="next-label next-label--switch" for="active-false">
                                                                                Ẩn
                                                                            </label>
                                                                            <input type="radio" name="product[vendor][active]" id="active-false" value="0" class="next-radio">
                                                                            <span class="next-radio--styled"></span>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                    <div class="ui-layout__item" id="images-container">
                                        <div class="next-card">
                                            <header class="next-card__header">
                                                <div class="next-grid next-grid--no-padding next-grid--vertically-centered">
                                                    <div class="next-grid__cell">
                                                        <h2 class="next-heading">Ảnh danh mục</h2>
                                                    </div>
                                                </div>
                                            </header>
                                            <div context="form" class="next-card__section">
                                                <div id="collection-image-drop">
                                                    <div class="aspect-ratio aspect-ratio--dropzone aspect-ratio--square aspect-ratio--interactive">
                                                        <div class="aspect-ratio__content" style="padding:15px;background:#fff;">
                                                            <div class="ui-stack ui-stack--wrap ui-stack--vertical type--centered">
                                                                <img id="img-product-type" src="/img/no-image.png" style="max-width:50%;margin:10% auto">
                                                                <div class="styled-file-input" style="padding-top:10px">
                                                                    <div class="btn btn-primary">
                                                                        <label for="collection-upload-image">Upload ảnh</label>
                                                                        <input type="file" name="product[vendor][image]" onchange="onChangeImageType(this)" id="product-type-image" class="image-drop-file-input">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="btn-change-image-product" style="padding: 10px 0 0;display: none;">
                                                        <span style="color:#2698EC;cursor:pointer" onclick="$('#product-type-image').click()">Thay ảnh</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ui-page-actions">
                            <div class="ui-page-actions__container">
                                <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                                    <div class="ui-page-actions__button-group">
                                        <a class="btn" data-allow-default="1" href=" "> Hủy </a>
                                        <button name="button" type="submit" class="btn js-btn-primary js-btn-loadable btn-primary has-loading"> Lưu </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    function onChangeImageType(input)
    {
        if(input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function(e)
            {
                var srcImg  =  e.target.result;
                $('#img-product-type').attr({ 'src': srcImg, 'style':'width:100%' }).next('.styled-file-input').css('display','none');
                $('.btn-change-image-product').css('display','block');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
@endsection