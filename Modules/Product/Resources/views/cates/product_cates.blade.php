@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Danh mục sản phẩm')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div id="products-new" class="product-detail page">
                    <header class="ui-title-bar-container   ui-title-bar-container--full-width">
                        <div class="ui-title-bar">
                            <div class="ui-title-bar__navigation">
                                <div class="ui-breadcrumbs">
                                    <a href="/admin/products" class="ui-button ui-button--transparent ui-breadcrumb">
                                        <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left-thinner"></use>
                                        </svg>
                                        <span class="ui-breadcrumb__item"> Sản phẩm </span>
                                    </a>
                                </div>
                            </div>
                            <div class="ui-title-bar__main-group">
                                <div class="ui-title-bar__heading-group">
                                    <span class="ui-title-bar__icon">
                                        <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-products"></use>
                                        </svg>
                                    </span>
                                    <h1 class="ui-title-bar__title"> Danh mục sản phẩm </h1>
                                </div>
                            </div>
                            <div class="ui-title-bar__actions-group">
                                <div class="ui-title-bar__actions">
                                    <a href="/admin/products/categories/create" class="ui-button ui-button--primary ui-title-bar__action">Tạo mới</a>
                                </div>
                            </div>
                        </div>
                    </header>
                    <div class="ui-layout ui-layout--full-width">
                        <div class="ui-layout__sections">
                            <div class="ui-layout__section">
                                <div class="ui-layout__item">
                                    <section class="ui-card">
                                        <div id="filterAndSavedSearch" context="filterer">
                                            <div class="next-tab__container ">
                                                <ul class="next-tab__list filter-tab-list" id="filter-tab-list" role="tablist" data-has-next-tab-controller="true">
                                                    <li class="filter-tab-item" data-tab-index="1">
                                                        <p class="filter-tab filter-tab-active show-all-items next-tab next-tab--is-active">Tất cả loại sản phẩm</p>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="ui-card__section has-bulk-actions collections" refresh="collections" id="collections-refresh">
                                            <div class="ui-type-container clearfix">
                                                <div class="table-wrapper" context="bulkActions">
                                                    <table id="price-rule-table" class="table-hover bulk-action-context expanded">
                                                        <thead>
                                                            <tr>
                                                                <th class="image"></th>
                                                                <th class="name"><span>Loại sản phẩm</span></th>
                                                                <th class="alias hidden-xs"><span>Đường dẫn</span></th>
                                                                <th class="summary hidden-xs" style="max-width:255px;text-align:justify;padding-right:5%"><span>Mô tả</span></th>
                                                                <th class="conditions"><span>Trạng thái</span></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($cates as $item)
                                                            <tr>
                                                                <td>
                                                                    <a class="aspect-ratio aspect-ratio--square aspect-ratio--square--50 aspect-ratio--interactive" href="/admin/products/categories/{{$item->pc_id}}-{{$item->pc_alias}}">
                                                                        @if($item->pc_avatar != '')
                                                                            <img class="aspect-ratio__content" src="/uploads/images/product/{{ $item->pc_avatar }}">
                                                                        @else
                                                                            <img class="aspect-ratio__content" src="/img/no-image.png" width='50%'>
                                                                        @endif
                                                                    </a>
                                                                </td>
                                                                <td>
                                                                    <a data-nested-link-target="true" href="/admin/products/categories/{{$item->pc_id}}-{{$item->pc_alias}}">{{ $item->pc_name }}</a>
                                                                </td>
                                                                <td class="hidden-xs">
                                                                    <span>{{ url('/') }}/{{ $item->pc_alias }}</span>
                                                                </td>
                                                                <td style="max-width:255px;text-align:justify;padding-right:5%" class="hidden-xs">
                                                                    <p>{!! str_limit($item->pc_summary, 200) !!}</p>
                                                                </td>
                                                                <td>
                                                                    <span>@if($item->pc_active == 1) Hiện @else Ẩn @endif</span>
                                                                </td>
                                                            </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="t-grid-pager-boder">
                                                    <div class="t-pager t-reset clearfix fix-margin-pager">
                                                        <div class="col-md-6 col-lg-6 hidden-xs hidden-sm no-padding">
                                                            <div class="t-status-text dataTables_info">
                                                                {{ $cates->links() }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection