@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Thêm mới sản phẩm')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div id="products-new" class="product-detail page">
                    <form autocomplete="off" data-context="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <header class="ui-title-bar-container">
                            <div class="ui-title-bar">
                                <div class="ui-title-bar__navigation">
                                    <div class="ui-breadcrumbs">
                                        <a href="/admin/products" class="ui-button ui-button--transparent ui-breadcrumb">
                                            <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left-thinner"></use>
                                            </svg>
                                            <span class="ui-breadcrumb__item"> Quay lại danh sách sản phẩm </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="ui-title-bar__main-group">
                                    <div class="ui-title-bar__heading-group">
                                        <span class="ui-title-bar__icon">
                                            <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-products"></use>
                                            </svg>
                                        </span>
                                        <h1 class="ui-title-bar__title"> Thêm mới sản phẩm </h1>
                                    </div>
                                </div>
                            </div>
                        </header>
                        
                        <div class="ui-layout">
                            <div class="ui-layout__sections">
                                <div class="ui-layout__section ui-layout__section--primary">
                                    <div class="ui-layout__item">
                                        <section class="ui-card" id="product-form-container">
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="product-name"> Tên sản phẩm </label>
                                                        <input required name="productName" autofocus="autofocus" id="product-name" onkeyup="metaTitle(this)" placeholder="Nhập tên sản phẩm" class="next-input" size="30" type="text">
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="Content"> Nội dung </label>
                                                        <textarea id="noidung" required class="next-input ckeditor" name="productContent"></textarea>
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <div bind-show="shownSummary">
                                                            <label class="next-label" for="product-summary"> Mô tả ngắn </label>
                                                            <textarea id="product-summary" class="next-input ckeditor" name="productSummary"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="ui-layout__item">
                                        <section class="ui-card">
                                            <header class="ui-card__header">
                                                <div class="ui-stack ui-stack--wrap">
                                                    <div class="ui-stack-item ui-stack-item--fill">
                                                        <h2 class="ui-heading">Xem trước kết quả tìm kiếm</h2>
                                                    </div>
                                                </div>
                                            </header>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="google-preview">
                                                        <span id="nva__google__title" class="google__title">tiêu đề</span>
                                                        <div class="google__url">
                                                            {{ url('/') }}/<span id="nva__google__alias">tieu-de</span>
                                                        </div>
                                                        <div class="google__description nva__google__description">Thẻ mô tả</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ui-card__section" data-bind-show="isVisible()">
                                                <div class="ui-type-container">
                                                    <div class="ui-form__section">
                                                        <div class="next-input-wrapper">
                                                            <div class="ui-form__label-wrapper">
                                                                <label class="next-label" for="seo-title-tag">Thẻ tiêu đề</label>
                                                                <span class="type--subdued">Số ký tự đã dùng: <b id="ltn__title">0</b>/70</span>
                                                            </div>
                                                            <input id="seo-title-tag" class="next-input" size="30" type="text" name="productMetaTitle" maxlength="70" required onkeyup="metaTitle(this)">
                                                        </div>
                                                        <div class="next-input-wrapper">
                                                            <div class="ui-form__label-wrapper">
                                                                <label class="next-label" for="seo-description-tag">Thẻ mô tả</label>
                                                                <span class="type--subdued">Số ký tự đã dùng: <b id="seo_description_lnt">0</b>/320</span>
                                                            </div>
                                                            <textarea id="seo-description-tag" onkeyup="onMetaDescription(this)" class="ui-text-area" name="productMetaDescription" maxlength="320" placeholder="Thẻ mô tả"></textarea>
                                                        </div>
                                                        <div class="ui-form__section">
                                                            <div class="next-input-wrapper">
                                                                <label class="next-label" for="object-handle">Đường dẫn / Alias</label>
                                                                <div class="next-input--stylized next-input--has-content">
                                                                    <span class="next-input__add-on next-input__add-on--before" style="padding-right:0">{{ url('/') }}/</span>
                                                                    <input value="tieu-de" id="object-handle" onkeyup="onAliasSeo(this)" class="next-input next-input--invisible" size="30" type="text" name="productAlias">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <div class="ui-layout__section ui-layout__section--secondary">
                                    <div class="ui-layout__item">
                                        <div class="next-card">
                                            <header class="next-card__header">
                                                <h3 class="ui-heading">Trạng thái</h3>
                                            </header>
                                            <section class="next-card__section">
                                                <div class="visibility" id="PublishingPanel">
                                                    <div class="ui-form__section">
                                                        <div class="ui-form__element">
                                                            <fieldset class="ui-choice-list">
                                                               
                                                                <ul>
                                                                    <li>
                                                                        <div class="next-input-wrapper">
                                                                            <label class="next-label next-label--switch" >
                                                                                Hiển thị
                                                                            </label>
                                                                            <input type="radio" name="productActive"  value="0" class="next-radio" checked="checked">
                                                                            <span class="next-radio--styled"></span>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="next-input-wrapper">
                                                                            <label class="next-label next-label--switch">
                                                                                Ẩn
                                                                            </label>
                                                                            <input type="radio" name="productActive" value="1" class="next-radio">
                                                                            <span class="next-radio--styled"></span>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                    <div class="ui-layout__item" id="images-container">
                                       <div class="next-card">
                                          <header class="next-card__header">
                                             <div class="next-grid next-grid--no-padding next-grid--vertically-centered">
                                                <div class="next-grid__cell">
                                                   <h2 class="next-heading">Ảnh bài viết</h2>
                                                </div>
                                             </div>
                                          </header>
                                          <div class="next-card__section">
                                                    <div class="next-upload-dropzone__wrapper">
                                                        <ol id="product-images" data-tg-refresh="product-images" class="product-photo-grid product-photo-grid--is-showing-all clearfix ui-sortable">
                                                            <li class="js-product-photo-grid-item product-photo-grid__item ui-sortable-handle">
                                                                <div class="aspect-ratio aspect-ratio--square">
                                                                    <img class="aspect-ratio__content" src="http://smswebsite.ta/img/no-image.png" style="max-width: 80px;">
                                                                    <div class="product-photo-hover-overlay drag">
                                                                        <div style="position:absolute;top:40%;width:100%;text-align:center;">
                                                                            <span class="btn btn-sm btn-info" onclick="$(this).next().click()"> Chọn ảnh </span>
                                                                            <input name="productImage" class="hide" onchange="fileInputChanged(this)" multiple="multiple" type="file">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ol>
                                                    </div>
                                                </div>
                                       </div>
                                    </div>
                                    <div class="ui-layout__item">
                                        <section class="ui-card ui-card--type-aside">
                                            <header class="ui-card__header">
                                                <h2 class="ui-heading">Phân loại</h2>
                                            </header>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="next-input-wrapper">
                                                        <label for="product_product_type">Loại</label>
                                                        <div class="ui-popover__container ui-popover__container--full-width-container">
                                                            <div>
                                                                <div class="next-field__connected-wrapper">
                                                                    <select id="type_prod" name="productProduct_type" style="width: 100%">
                                                                        @foreach($type as $item)
                                                                            <option value="{{ $item->pt_id }}"> {{ $item->pt_name }} </option>
                                                                        @endforeach
                                                                        
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label for="product_vendor">Nhà cung cấp</label>
                                                        <div class="ui-popover__container ui-popover__container--full-width-container">
                                                            <div>
                                                                <div class="next-field__connected-wrapper">
                                                                    <select id="vendor_prod" name="productProduct_vendor" style="width: 100%">
                                                                        @foreach($vendor as $item)
                                                                            <option value="{{ $item->pv_id }}">{{ $item->pv_name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <h2 class="ui-heading">Danh mục</h2>
                                                    <p class="next-input__help-text">Thêm sản phẩm vào danh mục để nó dễ dàng được tìm kiếm trên website của bạn.</p>
                                                    <div id="js-collections">
                                                        <div class="ui-popover__container ui-popover__container--full-width-container">
                                                            <div class="next-input-wrapper">
                                                                <select id="cate_prod" style="width: 100%" name="productProduct_cate">
                                                                    <option value="1"> -- Vui lòng chọn danh mục -- </option>
                                                                    @foreach($cate as $item)
                                                                        <option value="{{ $item->pc_id }}">{{ $item->pc_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div id="product-collections">
                                                            <ul class="js-collection-list product-collections-list next-list next-list--plain-divided next-list--row-hover next-list--no-vertical-padding">
                                                            </ul>
                                                            <div id="add-to-collection"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="next-grid next-grid--no-outside-padding">
                                                        <div class="next-grid__cell">
                                                            <label for="tags" class="next-label">Tags</label>
                                                        </div>
                                                    </div>
                                                    <div id="tags-event-bus">
                                                        <div class="ui-popover__container ui-popover__container--full-width-container">
                                                            <div>
                                                                <div class="">
                                                                    <input class="next-input js-no-dirty" name="productProduct_tags" placeholder="Nhập tag cho sản phẩm" required id="tags">
                                                                    <span style="font-size:13px;color:#999;padding:2px">Các thẻ được ngăn cách nhau bởi dấu ','</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ui-page-actions">
                            <div class="ui-page-actions__container">
                                <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                                    <div class="ui-page-actions__button-group">
                                        <a class="btn" data-allow-default="1" href=" "> Hủy </a>
                                        <button name="button" type="submit" class="btn js-btn-primary js-btn-loadable btn-primary has-loading"> Lưu </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
            CKEDITOR.replace( 'noidung');
   </script>
   <script type="text/javascript">
                    function changeImg(input){
                        if(input.files && input.files[0]){
                            var reader = new FileReader();
                            reader.onload = function(e){
                                $(input).prev().attr('src',e.target.result);
                            }
                            reader.readAsDataURL(input.files[0]);
                        }
                    }
                </script>
</main>
@endsection