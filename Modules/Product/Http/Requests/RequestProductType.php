<?php

namespace Modules\Product\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestProductType extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'product.type.name'  => 'required|unique:product_type,pt_name',
            'product.type.alias' => 'required|unique:product_type,pt_alias',
        ];
    }

    public function messages()
    {
        return [
            'product.type.name.required'  => 'Không được để trống trường này',
            'product.type.name.unique'    => 'Thể loại đã tồn tại',
            'product.type.alias.unique'   => 'Đường dẫn đã tồn tại',
            'product.type.alias.required' => 'Đường dẫn không được để trống',
        ];
    }

}
