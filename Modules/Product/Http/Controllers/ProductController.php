<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Notifications\NotifyProduct;
use Illuminate\Support\Facades\Redirect;
use Session;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductType;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Entities\ProductVendor;
use File;


class ProductController extends Controller
{
    public function index()
    {
        return view('product::index');
    }
    public function create()
    {
        $data['type'] = ProductType::get();
        $data['vendor'] = ProductVendor::get();
        $data['cate'] = ProductCategory::get();
        return view('product::create',$data);
    }
    public function add(Request $request)
    {
        $validatedData = $request->validate([
            'productName'=>'required'

        ],
        [
            'productName.required'=> 'Không được để trống',
            
        ]);
        $product = new Product();
        $file_name = $request->file('productImage')->getClientOriginalName();
        $product->pro_name         =     $request->productName;
        $product->pro_content      =     $request->productContent;
        $product->pro_summary      =     $request->productSummary;
        $product->pro_price        =     $request->productVariantPrice;
        $product->pro_com_price    =     $request->productVariantCompareAtPrice;
        $product->pro_code         =     $request->productVariantSku;
        $product->pro_barcode      =     $request->productVariantBarcode;
        $product->pro_qty          =     $request->productVariantInventoryQuantity;
        $product->pro_sta_shopping =     $request->productVariantRequiresShipping;
        $product->pro_mass         =     $request->productVariantWeight;
        $product->pro_size         =     $request->productOptionsSize;
        $product->pro_color        =     $request->productOptionsColor;
        $product->pro_material     =     $request->productOptionsMaterial;
        $product->pro_status       =     $request->productActive;
        $product->pro_tags         =     $request->productProduct_tags;
        $product->pro_meta_title   =     $request->productMetaTitle;
        $product->pro_meta_des     =     $request->productMetaDescription;
        $product->pro_alias        =     $request->productAlias;
        $product->pro_type         =     $request->productProduct_type;
        $product->pro_vendor       =     $request->productProduct_vendor;
        $product->pro_cate         =     $request->productProduct_cate;
        if($imgAvatar = $request->file('productImage'))
        {
            $nameAvatar = rand().$file_name;
            $imgAvatar->move('uploads/imageProduct/', $nameAvatar);
            $product->pro_images                 =     $nameAvatar;
        }
        $product->save();

        if ($product->id)
        {
            return redirect('admin/products/listPro')->with('success','thêm sản phẩm thành công');
        }
        else
        {
            foreach ($names as $img)
            {
                if (file_exists( public_path('uploads/imageProduct/'.$img) ))
                {
                    unlink( public_path('uploads/imageProduct/'.$img) );
                }
            }
            return back()->with('error','thêm sản phẩm thất bại');
        }
    }
    public function listProduct()
    {

        $data['products'] = Product::paginate(15);
        return view('product::listProduct',$data);
    }
    public function getEdit($id)
    {
        $data['type'] = ProductType::get();
        $data['vendor'] = ProductVendor::get();
        $data['cate'] = ProductCategory::get();
        $data['pro'] = Product::where('id', $id)->first();
        return view('product::editPro',$data);
    }
    public function postEidt(Request $request,$id)
    {
        $validatedData = $request->validate([
            'productName'=>'required'
            

        ],
        [
            'productName.required'=> 'Không được để trống',
            
        ]);
        $product =  Product::find($id);
        $img_hientai = 'uploads/productImage/'. $product->pro_images;
        // dd($img_hientai);
        $product->pro_name         =     $request->productName;
        $product->pro_content      =     $request->productContent;
        $product->pro_summary      =     $request->productSummary;
        $product->pro_price        =     $request->productVariantPrice;
        $product->pro_com_price    =     $request->productVariantCompareAtPrice;
        $product->pro_code         =     $request->productVariantSku;
        $product->pro_barcode      =     $request->productVariantBarcode;
        $product->pro_qty          =     $request->productVariantInventoryQuantity;
        $product->pro_sta_shopping =     $request->productVariantRequiresShipping;
        $product->pro_mass         =     $request->productVariantWeight;
        $product->pro_size         =     $request->productOptionsSize;
        $product->pro_color        =     $request->productOptionsColor;
        $product->pro_material     =     $request->productOptionsMaterial;
        $product->pro_status       =     $request->productActive;
        $product->pro_tags         =     $request->productProduct_tags;
        $product->pro_meta_title   =     $request->productMetaTitle;
        $product->pro_meta_des     =     $request->productMetaDescription;
        $product->pro_alias        =     $request->productAlias;
        $product->pro_type         =     $request->productProduct_type;
        $product->pro_vendor       =     $request->productProduct_vendor;
        $product->pro_cate         =     $request->productProduct_cate;
         if (!empty($request->file('productImage'))) {
            $file_name = rand().$request->file('productImage')->getClientOriginalName();
            $product->pro_images                 =     $file_name;
            $request->file('productImage')->move('uploads/imageProduct/',$file_name);
            if (File::exists($img_hientai)) {
                File::delete($img_hientai);
            }
        }else {
            echo 'ko có';
        }
        $product->save();
        if ($product->id)
        {
            return redirect('admin/products/listPro')->with('success','Cập nhật bài viết thành công');
        }
        else
        {
            return back()->with('error', 'Cập nhật bài viết không thành công');
        }
    }
    public function search(Request $request)
    {
        $keyword = trim($request->country);
        // $search = Product::where('pro_name',$keyword)->orWhere('pro_code',$keyword)->paginate(10);
        $html = '';
        if ($keyword == '') {
            dd(1);
             $search = Product::paginate(10);
        }else {
            // $search = Product::whereRaw('MATCH (pro_name) AGAINST("*' . $keyword . '*" IN BOOLEAN MODE)',$this->fullTextWildcards($request))->paginate(10);
            $search = Product::where('pro_name', 'like', "%".$keyword."%")->orWhere('pro_code', 'like', "%".$keyword."%")->paginate(10);
        }
        $stt = 0;
        foreach ($search as $item) {
            $stt = $stt +1;
            $html .= '<tr>';
            $html .= '  <td class="select">';
            $html .= ' '.$stt.' ';
            $html .= '  </td> ';
            $html .= ' <td> ';
            
            

            $html .= '<a class="aspect-ratio aspect-ratio--square aspect-ratio--square--50 aspect-ratio--interactive" href="/admin/products/12243781">' ;
            $html .= '  <img title="Gấu bông Teddy cute" class="aspect-ratio__content" src="http://mixone.ta/uploads/imageProduct/'.$item->pro_images.' "   alt=" '.$item->pro_name.' ">';
            $html .= '</a> ';
            $html .= '</td> ';
            $html .= '<td class="name"> ';
            $html .= '<div class="ui-stack ui-stack--wrap">';
            $html .= '<div class="ui-stack-item">';
            $html .= ' <a href="/admin/products/12243781" data-nested-link-target="true">'.$item->pro_name.'</a>';
            $html .= '  </div>';
            $html .= '  </div>';
            $html .= '   </td>';
            $html .= '   <td class="total">';
            $html .= '   <p>'.$item->pro_qty.' sản phẩm</p>';
            $html .= '   </td>';
            $html .= '   <td class="type">';
                               
            $typePr =  ProductType::where('pt_id', $item->pro_type)->first();
                              
            $html .=                   '<p>'.$typePr->pt_name.'</p>';
            $html .=               '</td>';

            $typePv =  ProductVendor::where('pv_id', $item->pro_type)->first();

            $html .= '   <td class="vendor">
                           <p>'.$typePv->pv_name.'</p>
                        </td>';  
            $html .= '</tr>';                                                  
        }
     return $html;
       
    }
       
    public function getDelete($id)
    {
        $pro = Product::where('id' , $id)->first();
        File::delete('uploads/imageProduct/'.$pro->pro_images);
        $pro->delete($id);
        return redirect('/admin/products/listPro');
    }
}
