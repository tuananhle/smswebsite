<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\ProductVendor;
use Validator;

class ProductVendorController extends Controller
{
    public function listVendor()
    {
        $data['vendor'] = ProductVendor::orderby('pv_id','desc')->paginate(20);
        return view('product::vendor.product_vendor', $data);
    }
    public function addVendor()
    {
        return view('product::vendor.product_vendor_add');
    }
    public function postAddVendor(Request $request)
    {
        $rules = [
            'product.vendor.name'  => 'required|unique:product_vendor,pv_name',
            'product.vendor.alias' => 'required|unique:product_vendor,pv_alias',
        ];
        $messages = [
            'product.vendor.name.required'  => 'Không được để trống trường này',
            'product.vendor.name.unique'    => 'Nhà cung cấp đã tồn tại',
            'product.vendor.alias.unique'   => 'Đường dẫn đã tồn tại',
            'product.vendor.alias.required' => 'Đường dẫn không được để trống',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }

        $name_avatar  = '';
        if($imgAvatar = $request->file('product.vendor.image'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/product', $nameAvatar);
            $name_avatar = $nameAvatar;
        }
        else { $name_avatar = ''; }

        $vendor = new ProductVendor;
        $vendor->pv_name     = $request->input('product.vendor.name');
        $vendor->pv_summary  = $request->input('product.vendor.summary');
        $vendor->pv_meta_title = $request->input('product.vendor.metaTitle');
        $vendor->pv_meta_description = $request->input('product.vendor.metaDescription');
        $vendor->pv_alias    = $request->input('product.vendor.alias');
        $vendor->pv_active   = $request->input('product.vendor.active');
        $vendor->pv_avatar   = $name_avatar;
        $vendor->save();

        if(!$vendor) {
            if (file_exists(public_path('uploads/images/product').'/'.$name_avatar)) {
                unlink(public_path('uploads/images/product').'/'.$name_avatar);
            }
            return back()->with('error', ' Thêm mới nhà cung cấp '.$request->input('product.vendor.name').' thất bại');
        }
        else {
            return redirect('/admin/products/vendor')->with('success', ' Thêm mới nhà cung cấp '.$request->input('product.vendor.name').' thành công!');
        }
    }
    public function deleteVendor($id)
    {
        $vendor = ProductVendor::where('pv_id', $id)->first();
        $name   = $vendor->pv_name;
        $avatar = $vendor->pv_avatar;
        if($avatar != '')
        {
            if (file_exists(public_path('uploads/images/product').'/'.$avatar)) {
                unlink(public_path('uploads/images/product').'/'.$avatar);
            }
        }
        
        $delvendor = $vendor->delete();
        if ($delvendor)
        {
            return redirect('/admin/products/vendor')->with('success', ' Xóa thành công nhà cung cấp '. $name );
        }
        else
        {
            return back()->with('error', 'Xóa thất bại. Vui lòng kiểm tra lại');
        }
        
    }
    public function editVendor($id, $alias)
    {
        $vendor = ProductVendor::where('pv_id', $id)->first();
        if (!$vendor) {
            return back()->with('error', 'Không tồn tại bản ghi. Vui lòng kiểm tra lại');
        }
        $data['vendor'] = $vendor;
        return view('product::vendor.product_vendor_edit', $data);
    }
    public function postEditVendor(Request $request, $id, $alias)
    {
        $rules = [
            'product.vendor.name'  => 'required|unique:product_vendor,pv_name,'.$id.',pv_id',
            'product.vendor.alias' => 'required|unique:product_vendor,pv_alias,'.$id.',pv_id',
        ];
        $messages = [
            'product.vendor.name.required'  => 'Không được để trống trường này',
            'product.vendor.name.unique'    => 'Thể loại đã tồn tại',
            'product.vendor.alias.unique'   => 'Đường dẫn đã tồn tại',
            'product.vendor.alias.required' => 'Đường dẫn không được để trống',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }

        $vendor = ProductVendor::where('pv_id', $id);
        $arrInp = [
            'pv_name'     => $request->input('product.vendor.name'),
            'pv_summary'  => $request->input('product.vendor.summary'),
            'pv_meta_title' => $request->input('product.vendor.metaTitle'),
            'pv_meta_description' => $request->input('product.vendor.metaDescription'),
            'pv_alias'    => $request->input('product.vendor.alias'),
            'pv_active'   =>  $request->input('product.vendor.active'),
        ];
        if($imgAvatar = $request->file('product.vendor.image'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/product', $nameAvatar);
            $name_avatar = $vendor->first()->pv_avatar;
            if ($name_avatar != '') {
                if (file_exists(public_path('uploads/images/product').'/'.$name_avatar)) {
                    unlink(public_path('uploads/images/product').'/'.$name_avatar);
                }
            }
            $arrInp['pv_avatar'] = $nameAvatar;
        }
        $success = $vendor->update($arrInp);
        if ($success) {
            return back()->with('success', ' Cập nhật thành công');
        }
        else {
            return back()->with('error', ' Cập nhật thất bại');
        }
    }
}
