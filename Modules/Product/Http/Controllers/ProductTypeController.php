<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\ProductType;
use Modules\Product\Http\Requests\RequestProductType;
use Modules\Product\Http\Requests\UpdateTypeProduct;
use Validator;

class ProductTypeController extends Controller
{
    public function listTypes()
    {
        $data['types'] = ProductType::orderby('pt_id','desc')->paginate(20);
        return view('product::types.product_type', $data);
    }
    public function addTypes()
    {
        return view('product::types.product_type_add');
    }
    public function postAddTypes(RequestProductType $request)
    {
        $name_avatar  = '';
        if($imgAvatar = $request->file('product.type.image'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/product', $nameAvatar);
            $name_avatar = $nameAvatar;
        }
        else { $name_avatar = ''; }

        $type = new ProductType;
        $type->pt_name     = $request->input('product.type.name');
        $type->pt_cate     = $request->input('productProduct_cate');
        $type->pt_summary  = $request->input('product.type.summary');
        $type->pt_meta_title = $request->input('product.type.metaTitle');
        $type->pt_meta_description = $request->input('product.type.metaDescription');
        $type->pt_alias    = $request->input('product.type.alias');
        $type->pt_active   = $request->input('product.type.active');
        $type->pt_avatar   = $name_avatar;
        $type->save();

        if(!$type) {
            if (file_exists(public_path('uploads/images/product').'/'.$name_avatar)) {
                unlink(public_path('uploads/images/product').'/'.$name_avatar);
            }
            return back()->with('error', ' Thêm mới loại sản phẩm '.$request->input('product.type.name').' thất bại');
        }
        else {
            return redirect('/admin/products/types')->with('success', ' Thêm mới loại sản phẩm '.$request->input('product.type.name').' thành công!');
        }
    }
    public function deleteTypes($id)
    {
        $type = ProductType::where('pt_id', $id)->first();
        $name = $type->pt_name;
        $avatar = $type->pt_avatar;
        if($avatar != '')
        {
            if (file_exists(public_path('uploads/images/product').'/'.$avatar)) {
                unlink(public_path('uploads/images/product').'/'.$avatar);
            }
        }
        
        $deltype = $type->delete();
        if ($deltype)
        {
            return redirect('/admin/products/types')->with('success', ' Xóa thành công loại sản phẩm '. $name );
        }
        else
        {
            return back()->with('error', 'Xóa thất bại. Vui lòng kiểm tra lại');
        }
        
    }
    public function editTypes($id, $alias)
    {
        $type = ProductType::where('pt_id', $id)->first();
        if (!$type) {
            return back()->with('error', 'Không tồn tại bản ghi. Vui lòng kiểm tra lại');
        }
        $data['type'] = $type;
        return view('product::types.product_type_edit', $data);
    }
    public function postEditTypes(Request $request, $id, $alias)
    {
        $rules = [
            'product.type.name'  => 'required|unique:product_type,pt_name,'.$id.',pt_id',
            'product.type.alias' => 'required|unique:product_type,pt_alias,'.$id.',pt_id',
        ];
        $messages = [
            'product.type.name.required'  => 'Không được để trống trường này',
            'product.type.name.unique'    => 'Thể loại đã tồn tại',
            'product.type.alias.unique'   => 'Đường dẫn đã tồn tại',
            'product.type.alias.required' => 'Đường dẫn không được để trống',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );

        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }

        $type = ProductType::where('pt_id', $id);
        $arrInp = [
            'pt_name'     => $request->input('product.type.name'),
            'pt_cate'     => $request->input('productProduct_cate'),
            'pt_summary'  => $request->input('product.type.summary'),
            'pt_meta_title' => $request->input('product.type.metaTitle'),
            'pt_meta_description' => $request->input('product.type.metaDescription'),
            'pt_alias'    => $request->input('product.type.alias'),
            'pt_active'   =>  $request->input('product.type.active'),
        ];
        if($imgAvatar = $request->file('product.type.image'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/product', $nameAvatar);
            $name_avatar = $type->first()->pt_avatar;
            if ($name_avatar != '') {
                if (file_exists(public_path('uploads/images/product').'/'.$name_avatar)) {
                    unlink(public_path('uploads/images/product').'/'.$name_avatar);
                }
            }
            $arrInp['pt_avatar'] = $nameAvatar;
        }
        $success = $type->update($arrInp);
        if ($success) {
            return back()->with('success', ' Cập nhật thành công');
        }
        else {
            return back()->with('error', ' Cập nhật thất bại');
        }
    }
}
