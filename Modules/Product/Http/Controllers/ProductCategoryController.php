<?php

namespace Modules\Product\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\ProductCategory;
use Validator;

class ProductCategoryController extends Controller
{
    public function listCategory()
    {
        $data['cates'] = ProductCategory::orderby('pc_id','desc')->paginate(20);
        return view('product::cates.product_cates', $data);
    }
     public function addCategory()
    {
        return view('product::cates.product_cates_add');
    }
    public function postAddCategory(Request $request)
    {
        $rules = [
            'product.cates.name'  => 'required|unique:product_categoies,pc_name',
            'product.cates.alias' => 'required|unique:product_categoies,pc_alias',
        ];
        $messages = [
            'product.cates.name.required'  => 'Không được để trống trường này',
            'product.cates.name.unique'    => 'Nhà cung cấp đã tồn tại',
            'product.cates.alias.unique'   => 'Đường dẫn đã tồn tại',
            'product.cates.alias.required' => 'Đường dẫn không được để trống',
        ];
        
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }

        $name_avatar  = '';
        if($imgAvatar = $request->file('product.cates.image'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/product', $nameAvatar);
            $name_avatar = $nameAvatar;
        }
        else { $name_avatar = ''; }

        $cates = new ProductCategory;
        $cates->pc_name     = $request->input('product.cates.name');
        $cates->pc_summary  = $request->input('product.cates.summary');
        $cates->pc_meta_title = $request->input('product.cates.metaTitle');
        $cates->pc_meta_description = $request->input('product.cates.metaDescription');
        $cates->pc_alias    = $request->input('product.cates.alias');
        $cates->pc_active   = $request->input('product.cates.active');
        $cates->pc_avatar   = $name_avatar;
        $cates->save();

        if(!$cates) {
            if (file_exists(public_path('uploads/images/product').'/'.$name_avatar)) {
                unlink(public_path('uploads/images/product').'/'.$name_avatar);
            }
            return back()->with('error', ' Thêm mới danh mục '.$request->input('product.cates.name').' thất bại');
        }
        else {
            return redirect('/admin/products/categories')->with('success', ' Thêm mới danh mục '.$request->input('product.cates.name').' thành công!');
        }
    }
    public function deleteCategory($id)
    {
        $cates = ProductCategory::where('pc_id', $id)->first();
        $name   = $cates->pc_name;
        $avatar = $cates->pc_avatar;
        if($avatar != '')
        {
            if (file_exists(public_path('uploads/images/product').'/'.$avatar)) {
                unlink(public_path('uploads/images/product').'/'.$avatar);
            }
        }
        
        $delcates = $cates->delete();
        if ($delcates)
        {
            return redirect('/admin/products/categories')->with('success', ' Xóa thành công nhà cung cấp '. $name );
        }
        else
        {
            return back()->with('error', 'Xóa thất bại. Vui lòng kiểm tra lại');
        }
        
    }
    public function editCategory($id, $alias)
    {
        $cates = ProductCategory::where('pc_id', $id)->first();
        if (!$cates) {
            return back()->with('error', 'Không tồn tại bản ghi. Vui lòng kiểm tra lại');
        }
        $data['cates'] = $cates;
        return view('product::cates.product_cates_edit', $data);
    }
    public function postEditCategory(Request $request, $id, $alias)
    {
        $rules = [
            'product.cates.name'  => 'required|unique:product_categoies,pc_name,'.$id.',pc_id',
            'product.cates.alias' => 'required|unique:product_categoies,pc_alias,'.$id.',pc_id',
        ];
        $messages = [
            'product.cates.name.required'  => 'Không được để trống trường này',
            'product.cates.name.unique'    => 'Thể loại đã tồn tại',
            'product.cates.alias.unique'   => 'Đường dẫn đã tồn tại',
            'product.cates.alias.required' => 'Đường dẫn không được để trống',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }

        $cates = ProductCategory::where('pc_id', $id);
        $arrInp = [
            'pc_name'     => $request->input('product.cates.name'),
            'pc_summary'  => $request->input('product.cates.summary'),
            'pc_meta_title' => $request->input('product.cates.metaTitle'),
            'pc_meta_description' => $request->input('product.cates.metaDescription'),
            'pc_alias'    => $request->input('product.cates.alias'),
            'pc_active'   =>  $request->input('product.cates.active'),
        ];
        if($imgAvatar = $request->file('product.cates.image'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/images/product', $nameAvatar);
            $name_avatar = $cates->first()->pc_avatar;
            if ($name_avatar != '') {
                if (file_exists(public_path('uploads/images/product').'/'.$name_avatar)) {
                    unlink(public_path('uploads/images/product').'/'.$name_avatar);
                }
            }
            $arrInp['pc_avatar'] = $nameAvatar;
        }
        $success = $cates->update($arrInp);
        if ($success) {
            return back()->with('success', ' Cập nhật thành công');
        }
        else {
            return back()->with('error', ' Cập nhật thất bại');
        }
    }
}
