<?php

Route::group([ 'middleware' => [ 'web','CheckAdmin' ], 'prefix' => 'admin/products', 'namespace' => 'Modules\Product\Http\Controllers' ], function()
{
    Route::get('/', 'ProductController@index');

    Route::get('create', 'ProductController@create');
    Route::post('create', 'ProductController@add');
    Route::get('listPro', 'ProductController@listProduct');
    Route::get('editPro/{id}', 'ProductController@getEdit');
    Route::post('editPro/{id}', 'ProductController@postEidt');
    Route::post('search','ProductController@search');
    Route::get('delete/{id}','ProductController@getDelete');

    Route::group(['prefix' => 'types'], function() {
    	Route::get('/', 'ProductTypeController@listTypes');
    	Route::get('create', 'ProductTypeController@addTypes');
    	Route::post('create', 'ProductTypeController@postAddTypes');
    	Route::get('{id}-{alias}', 'ProductTypeController@editTypes');
    	Route::post('{id}-{alias}', 'ProductTypeController@postEditTypes');
    	Route::get('delete/{id}', 'ProductTypeController@deleteTypes');
    });

    Route::group(['prefix' => 'vendor'], function() {
        Route::get('/', 'ProductVendorController@listVendor');
        Route::get('create', 'ProductVendorController@addVendor');
        Route::post('create', 'ProductVendorController@postAddVendor');
        Route::get('{id}-{alias}', 'ProductVendorController@editVendor');
        Route::post('{id}-{alias}', 'ProductVendorController@postEditVendor');
        Route::get('delete/{id}', 'ProductVendorController@deleteVendor');
    });

    Route::group(['prefix' => 'categories'], function() {
        Route::get('/', 'ProductCategoryController@listCategory');
        Route::get('create', 'ProductCategoryController@addCategory');
        Route::post('create', 'ProductCategoryController@postAddCategory');
        Route::get('{id}-{alias}', 'ProductCategoryController@editCategory');
        Route::post('{id}-{alias}', 'ProductCategoryController@postEditCategory');
        Route::get('delete/{id}', 'ProductCategoryController@deleteCategory');
    });

});
