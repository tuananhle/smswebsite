<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_categoies';
	protected $primaryKey = 'pc_id';
    protected $guarded = [];
    public function product()
    {
    	return $this->hasMany('Modules\Product\Entities\Product');
    }
}
