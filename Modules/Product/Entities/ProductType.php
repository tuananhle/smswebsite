<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductType extends Model
{
	protected $table = 'product_type';
	protected $primaryKey = 'pt_id';
    protected $guarded = [];

    public function product()
    {
        return $this->hasMany('Modules\Product\Entities\Product');
    	
    }
    public function ProductVendor()
    {
    	return $this->belongTo('Modules\Product\Entities\ProductVendor');
    }
}
