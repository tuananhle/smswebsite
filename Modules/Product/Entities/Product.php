<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    protected $guarded = [];

    public function cate()
    {
    	return $this->belongTo('Modules\Product\Entities\ProductCategory');
    }
    public function ProductType()
    {
        return $this->belongTo('Modules\Product\Entities\ProductType');
    	// return $this->belongTo(ProductType::class);
    }
    public function ProductVendor()
    {
    	return $this->belongTo('Modules\Product\Entities\ProductVendor');
    }
}
