<?php

namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductVendor extends Model
{
    protected $table = 'product_vendor';
	protected $primaryKey = 'pv_id';
    protected $guarded = [];
    public function product()
    {
    	return $this->hasMany('Modules\Product\Entities\Product');
    }
    public function ProductType()
    {
    	return $this->hasMany('Modules\Product\Entities\ProductType');
    }
}
