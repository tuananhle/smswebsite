@extends('admin.layouts.master')
@section('title', 'Hệ thống quản trị | Danh mục sản phẩm')
@section('main')
<main id="AppFrameMain" class="ui-app-frame__main">
    <div class="wrapper" id="wrapper">
        <div id="body" class="page-content clearfix" data-tg-refresh="body">
            <div id="content">
                <div id="blogs-new" class="blog-detail page">
                    <form autocomplete="off" data-context="form" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <header class="ui-title-bar-container">
                            <div class="ui-title-bar">
                                <div class="ui-title-bar__navigation">
                                    <div class="ui-breadcrumbs">
                                        <a href="/admin/blogs/categories" class="ui-button ui-button--transparent ui-breadcrumb">
                                            <svg class="next-icon next-icon--size-20 next-icon--no-nudge">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-left-thinner"></use>
                                            </svg>
                                            <span class="ui-breadcrumb__item"> Quay lại danh sách </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="ui-title-bar__main-group">
                                    <div class="ui-title-bar__heading-group">
                                        <span class="ui-title-bar__icon">
                                            <svg class="next-icon next-icon--color-slate-lighter next-icon--size-20">
                                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#next-blogs"></use>
                                            </svg>
                                        </span>
                                        <h1 class="ui-title-bar__title"> Danh mục Blog </h1>
                                    </div>
                                </div>
                            </div>
                        </header>
                        
                        <div class="ui-layout">
                            <div class="ui-layout__sections">
                                <div class="ui-layout__section ui-layout__section--primary">
                                    <div class="ui-layout__item">
                                        <section class="ui-card" id="blog-form-container">
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="blog-type-name"> Danh mục sản phẩm </label>
                                                        <input  name="blogCatesName" autofocus="autofocus" id="blog-type-name" 
                                                                placeholder="Danh mục sản phẩm" class="@if($errors->has('blogCatesName')) {{ 'input-error' }} @endif next-input" 
                                                                size="30" type="text" onkeyup="metaTitle(this)" value="{{ old('blogCatesName', isset($cates) ? $cates->bc_name : null) }}">
                                                        @if($errors->has('blogCatesName'))<span style="color: #f33"> {{ $errors->first('blogCatesName') }} </span>@endif
                                                    </div>
                                                    <div class="next-input-wrapper">
                                                        <label class="next-label" for="blog-type-summary"> Mô tả </label>
                                                        <textarea id="blog-type-summary" class="next-input ckeditor" name="blogCatesSummary">{!! old('blogCatesSummary', isset($cates) ? $cates->bc_summary : null) !!}</textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div class="ui-layout__item">
                                        <section class="ui-card">
                                            <header class="ui-card__header">
                                                <div class="ui-stack ui-stack--wrap">
                                                    <div class="ui-stack-item ui-stack-item--fill">
                                                        <h2 class="ui-heading">Xem trước kết quả tìm kiếm</h2>
                                                    </div>
                                                </div>
                                            </header>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="google-preview">
                                                        <span id="nva__google__title" class="google__title">tiêu đề</span>
                                                        <div class="google__url">
                                                            {{ url('/') }}/<span id="nva__google__alias">tieu-de</span>
                                                        </div>
                                                        <div class="google__description nva__google__description">Thẻ mô tả</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="ui-card__section">
                                                <div class="ui-type-container">
                                                    <div class="ui-form__section">
                                                        <div class="next-input-wrapper">
                                                            <div class="ui-form__label-wrapper">
                                                                <label class="next-label" for="seo-title-tag">Thẻ tiêu đề</label>
                                                                <span class="type--subdued">Số ký tự đã dùng: <b id="ltn__title">0</b>/70</span>
                                                            </div>
                                                            <input id="seo-title-tag" class="next-input" size="30" type="text" name="blogCatesMetaTitle" maxlength="70" onkeyup="metaTitle(this)" value="{{ old('blogCatesMetaTitle', isset($cates) ? $cates->bc_meta_title : null) }}">
                                                        </div>
                                                        <div class="next-input-wrapper">
                                                            <div class="ui-form__label-wrapper">
                                                                <label class="next-label" for="seo-description-tag">Thẻ mô tả</label>
                                                                <span class="type--subdued">Số ký tự đã dùng: <b id="seo_description_lnt">0</b>/320</span>
                                                            </div>
                                                            <textarea id="seo-description-tag" onkeyup="onMetaDescription(this)" class="ui-text-area" name="blogCatesMetaDescription" maxlength="320" placeholder="Thẻ mô tả">{!! old('blogCatesMetaDescription', isset($cates) ? $cates->bc_meta_description : null) !!}</textarea>
                                                        </div>
                                                        <div class="ui-form__section">
                                                            <div class="next-input-wrapper">
                                                                <label class="next-label" for="object-handle">Đường dẫn / Alias</label>
                                                                <div class="next-input--stylized next-input--has-content @if($errors->has('blogCatesAlias')) {{ 'input-error' }} @endif">
                                                                    <span class="next-input__add-on next-input__add-on--before" style="padding-right:0">{{ url('/') }}/</span>
                                                                    <input value="{{ old('blogCatesAlias', isset($cates) ? $cates->bc_alias : null) }}" id="object-handle" onkeyup="onAliasSeo(this)" class="next-input next-input--invisible" size="30" type="text" name="blogCatesAlias">
                                                                </div>
                                                                <span style="color: #f33"> {{ $errors->first('blogCatesAlias') }} </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </div>

                                <div class="ui-layout__section ui-layout__section--secondary">
                                    <div class="ui-layout__item">
                                        <div class="next-card">
                                            <header class="next-card__header">
                                                <h3 class="ui-heading">Trạng thái</h3>
                                            </header>
                                            <section class="next-card__section">
                                                <div class="visibility" id="PublishingPanel">
                                                    <div class="ui-form__section">
                                                        <div class="ui-form__element">
                                                            <fieldset class="ui-choice-list">
                                                                <ul>
                                                                    <li>
                                                                        <div class="next-input-wrapper">
                                                                            <label class="next-label next-label--switch" for="active-true">
                                                                                Hiển thị
                                                                            </label>
                                                                            <input type="radio" name="blogCatesActive" id="active-true" value="1" class="next-radio" checked="checked">
                                                                            <span class="next-radio--styled"></span>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="next-input-wrapper">
                                                                            <label class="next-label next-label--switch" for="active-false">
                                                                                Ẩn
                                                                            </label>
                                                                            <input type="radio" name="blogCatesActive" id="active-false" value="0" class="next-radio">
                                                                            <span class="next-radio--styled"></span>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                    <div class="ui-layout__item" id="images-container">
                                        <div class="next-card">
                                            <header class="next-card__header">
                                                <div class="next-grid next-grid--no-padding next-grid--vertically-centered">
                                                    <div class="next-grid__cell">
                                                        <h2 class="next-heading">Ảnh danh mục</h2>
                                                    </div>
                                                </div>
                                            </header>
                                            <div context="form" class="next-card__section">
                                                <div id="collection-image-drop">
                                                    <div class="aspect-ratio aspect-ratio--dropzone aspect-ratio--square aspect-ratio--interactive">
                                                        <div class="aspect-ratio__content" style="padding:15px;background:#fff;">
                                                            <div class="ui-stack ui-stack--wrap ui-stack--vertical type--centered">
                                                                <img id="img-blog-type" src="{{ url('uploads/imageCateBlog/'.$cates->bc_avatar) }}" style="max-width:50%;margin:10% auto">
                                                                <div class="styled-file-input" style="padding-top:10px">
                                                                    <div class="btn btn-primary">
                                                                        <label for="collection-upload-image">Upload ảnh</label>
                                                                        <input type="file" name="blogCatesImage" onchange="onChangeImageType(this)" id="blog-type-image" class="image-drop-file-input">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="btn-change-image-blog" style="padding: 10px 0 0;display: none;">
                                                        <span style="color:#2698EC;cursor:pointer" onclick="$('#blog-type-image').click()">Thay ảnh</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="ui-page-actions">
                            <div class="ui-page-actions__container">
                                <div class="ui-page-actions__actions ui-page-actions__actions--primary">
                                    <div class="ui-page-actions__button-group">
                                        <a class="btn" data-allow-default="1" href=" "> Hủy </a>
                                        <button name="button" type="submit" class="btn js-btn-primary js-btn-loadable btn-primary has-loading"> Lưu </button>
                                    </div>
                                </div>
                            </div>
                            <div class="ui-page-actions__actions ui-page-actions__actions--secondary">
                                <div class="ui-page-actions__button-group">
                                    <a class="ui-button btn-destroy" bind-event-click="deleteModal.show()" href="{{ url('admin/blog/categories/delete/'.$cates->bc_id) }}" onclick="return confirm('Bạn muốn xóa bài viết này không?')">Xóa bài viết</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
<script type="text/javascript">
    function onChangeImageType(input)
    {
        if(input.files && input.files[0])
        {
            var reader = new FileReader();
            reader.onload = function(e)
            {
                var srcImg  =  e.target.result;
                $('#img-blog-type').attr({ 'src': srcImg, 'style':'width:100%' }).next('.styled-file-input').css('display','none');
                $('.btn-change-image-blog').css('display','block');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
@endsection