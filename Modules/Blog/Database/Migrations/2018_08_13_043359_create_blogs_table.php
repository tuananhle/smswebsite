<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->increments('bl_id');
            $table->string('bl_title');
            $table->longText('bl_content')->nullable();
            $table->longText('bl_summary')->nullable();
            $table->longText('bl_titlepage')->nullable();
            $table->longText('bl_des')->nullable();
            $table->string('bl_alias');
            $table->string('bl_tags');
            $table->integer('bl_active')->default(0);
            $table->string('bl_images')->nullable();
            $table->string('bl_author')->default('admin');
            $table->integer('bl_cate')->unsigned();
            $table->foreign('bl_cate')->references('bc_id')->on('blog_cates')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blogs');
    }
}
