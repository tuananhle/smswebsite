<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('blog_cates', function (Blueprint $table) {
            $table->increments('bc_id');
            $table->string('bc_name');
            $table->longText('bc_summary')->nullable();
            $table->string('bc_alias')->nullable();
            $table->string('bc_meta_title')->nullable();
            $table->longText('bc_meta_description')->nullable();
            $table->string('bc_avatar')->nullable();
            $table->integer('bc_active')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('blog_cates');
    }
}
