<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{

    protected $table = 'blogs';
    protected $guarded = [];
    protected $primaryKey = 'bl_id';
    public function blogcate()
    {
    	return $this->belongTo('Modules\Blog\Entities\BlogCate');
    }

}
