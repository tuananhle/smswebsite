<?php

namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class BlogCate extends Model
{
    protected $table = 'blog_cates';
    protected $guarded = [];
    protected $primaryKey = 'bc_id';
     public function blog()
    {
    	return $this->hasMany('Modules\Blog\Entities\Blog');
    }
}
