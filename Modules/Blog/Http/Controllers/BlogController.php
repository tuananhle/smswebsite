<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Blog\Entities\Blog;
use Modules\Blog\Entities\BlogCate;
use File,Auth;




class BlogController extends Controller
{
    public function listBlog()
    {
        $data['blog'] = Blog::get();
        return view('blog::listBlog',$data);
    }
    public function create()
    {
        $data['cate'] = BlogCate::get();
        return view('blog::create',$data);
    }
    public function add(Request $request)
    {
        $validatedData = $request->validate([
           

        ],
        [
            
            
        ]);
        $getNews = Blog::where('bl_title', $request->blogName)->count();
        if($getNews > 0)
        {
            return back()->with('error', 'Tiêu đề bài viết đã tồn tại');
        }
        $file_name = $request->file('blogImage')->getClientOriginalName();
        $blog = new Blog();
        $blog->bl_title                  =     $request->blogName;
        $blog->bl_content                =     $request->blogContent;
        $blog->bl_summary                =     $request->blogSummary;
        $blog->bl_titlepage              =     $request->blogTitle;
        $blog->bl_des                    =     $request->blDes;
        $blog->bl_alias                  =     $request->blogAlias;
        $blog->bl_author                 =     Auth::guard('admin')->user()->name;
        $blog->bl_tags                   =     $request->blogTags;
        $blog->bl_active                 =     $request->blogActive;
        $blog->bl_cate                   =     $request->Blog_cate;
        // $request->file('blogImage')->move('uploads/imageBlog/',$file_name);
        if($imgAvatar = $request->file('blogImage'))
        {
            $nameAvatar = rand().$file_name;
            $imgAvatar->move('uploads/imageBlog/', $nameAvatar);
            $blog->bl_images                 =     $nameAvatar;
        }
        $blog->save();
        if ($blog->bl_id)
        {
            return back()->with('success','Thêm bài viết thành công');
        }
        else
        {
            return back()->with('error', 'Đăng bài viết không thành công');
        }
    }
    public function search(Request $request)
    {
        $keyword = trim($request->searchblog);
        // $search = Product::where('pro_name',$keyword)->orWhere('pro_code',$keyword)->paginate(10);
        $html = '';
        if ($keyword == '') {
             $search = Blog::paginate(10);
        }else {
            // $search = Product::whereRaw('MATCH (pro_name) AGAINST("*' . $keyword . '*" IN BOOLEAN MODE)',$this->fullTextWildcards($request))->paginate(10);
            $search = Blog::where('bl_title', 'like', "%".$keyword."%")->paginate(10);
        }
        $stt = 0;
        foreach ($search as $item) {
            $stt = $stt +1;
            $html .= '<tr>';
            $html .= '  <td class="select">';
            $html .= ' '.$stt.' ';
            $html .= '  </td> ';
            $html .= ' <td> ';
            
            
            $html .= '<a class="aspect-ratio aspect-ratio--square aspect-ratio--square--50 aspect-ratio--interactive" href="/admin/products/12243781">' ;
            $html .= '  <img title="Gấu bông Teddy cute" class="aspect-ratio__content" src="http://mixone.ta/uploads/imageBlog//'.$item->bl_images.' "   alt=" '.$item->bl_images.' ">';
            $html .= '</a> ';
            $html .= '</td> ';
            $html .= '<td class="name"> ';
            $html .= '<div class="ui-stack ui-stack--wrap">';
            $html .= '<div class="ui-stack-item">';
            $html .= ' <a href="/admin/products/12243781" data-nested-link-target="true">'.$item->bl_title.'</a>';
            $html .= '  </div>';
            $html .= '  </div>';
            $html .= '   </td>';
            $html .= '   <td class="total">';
            $html .= '   <p>Tin tức </p>';
            $html .= '   </td>';
            $html .= '   <td class="type">';
                               
            
                              
            $html .=                   '<p>'.$item->bl_author.'</p>';
            $html .=               '</td>';

            

            $html .= '   <td class="vendor">
                           <p>'.$item->created_at.'</p>
                        </td>';  
            $html .= '</tr>';                                                  
        }
     return $html;
       
    }
    public function getEdit($id)
    {
        $data['blog'] = Blog::where('bl_id' , $id)->first();
        return view('blog::edit',$data);
    }
    public function postEdit(Request $request,$id)
    {
        $blog = Blog::find($id);
        $img_hientai = 'uploads/imageBlog/'. $blog->bl_images;
        $blog->bl_title                  =     $request->blogName;
        $blog->bl_content                =     $request->blogContent;
        $blog->bl_summary                =     $request->blogSummary;
        $blog->bl_titlepage              =     $request->blogTitle;
        $blog->bl_des                    =     $request->blDes;
        $blog->bl_alias                  =     $request->blogAlias;
        $blog->bl_tags                   =     $request->blogTags;
        $blog->bl_active                 =     $request->blogActive;
        $blog->bl_cate                   =      $request->Blog_cate;
         if (!empty($request->file('blogImage'))) {
            $file_name = rand().$request->file('blogImage')->getClientOriginalName();
            $blog->bl_images                 =     $file_name;
            $request->file('blogImage')->move('uploads/imageBlog/',$file_name);
            if (File::exists($img_hientai)) {
                File::delete($img_hientai);
            }
        }else {
            echo 'ko có';
        }
        $blog->save();
        if ($blog->bl_id)
        {
            return back()->with('success','Cập nhật bài viết thành công');
        }
        else
        {
            return back()->with('error', 'Cập nhật bài viết không thành công');
        }
    }
    public function getDelete($id)
    {
        $blog = Blog::where('bl_id' , $id)->first();
        File::delete('uploads/imageBlog/'.$blog->bl_images);
        $blog->delete($id);
        return redirect('/admin/blog/listBlog');
    }
    
    

}
