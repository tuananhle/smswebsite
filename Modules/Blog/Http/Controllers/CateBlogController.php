<?php

namespace Modules\Blog\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Validator;
use File;
use Modules\Blog\Entities\BlogCate;


class CateBlogController extends Controller
{
    public function addCategory()
    {
        return view('blog::cateBlog.create');
    }
    public function postAddCategory(Request $request)
    {
        $rules = [
            'blogCatesName'  => 'required|unique:blog_cates,bc_name',
            'blogCatesAlias' => 'required|unique:blog_cates,bc_alias',
        ];
        $messages = [
            'blogCatesName.required'  => 'Không được để trống trường này',
            'blogCatesName.unique'    => 'Nhà cung cấp đã tồn tại',
            'blogCatesAlias.unique'   => 'Đường dẫn đã tồn tại',
            'blogCatesAlias.required' => 'Đường dẫn không được để trống',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }
        $name_avatar  = '';
        if($imgAvatar = $request->file('blogCatesImage'))
        {
            $nameAvatar = rand().$imgAvatar->getClientOriginalName();
            $imgAvatar->move('uploads/imageCateBlog', $nameAvatar);
            $name_avatar = $nameAvatar;
        }
        else { $name_avatar = ''; }
        $cate = new BlogCate();
        $cate->bc_name = $request->blogCatesName;
        $cate->bc_summary = $request->blogCatesSummary;
        $cate->bc_alias = $request->blogCatesAlias;
        $cate->bc_meta_title = $request->blogCatesMetaTitle;
        $cate->bc_meta_description = $request->blogCatesMetaDescription;
        $cate->bc_avatar = $name_avatar;
        $cate->bc_active = $request->blogCatesActive;
        $cate->save();
        if(!$cate) {
            if (file_exists(public_path('uploads/imageCateBlog').'/'.$name_avatar)) {
                unlink(public_path('uploads/imageCateBlog').'/'.$name_avatar);
            }
            return back()->with('error', ' Thêm mới danh mục blog '.$request->input('blogCatesName').' thất bại');
        }
        else {
            return redirect('/admin/blog/categories')->with('success', ' Thêm mới danh mục blog '.$request->input('blogCatesName').' thành công!');
        }

    }
    public function listCategory()
    {
        $data['cates'] = BlogCate::paginate(10);
        return view('blog::cateBlog.list',$data);
    }
    public function editCategory($id)
    {
        $data['cates'] = BlogCate::where('bc_id',$id)->first();
        return view('blog::cateBlog.edit',$data);
    }
    public function postEditCategory(Request $request,$id)
    {
        $rules = [
            'blogCatesName'  => 'required',
            'blogCatesAlias' => 'required',
        ];
        $messages = [
            'blogCatesName.required'  => 'Không được để trống trường này',
            'blogCatesAlias.required' => 'Đường dẫn không được để trống',
        ];
        $validator = Validator::make( $request->all(), $rules, $messages );
        if ($validator->fails()) {
            return back()->with('errors', $validator->errors());
        }
        $cate =  BlogCate::find($id);
        $img_hientai = 'uploads/imageCateBlog/'. $cate->bc_avatar;
        $cate->bc_name = $request->blogCatesName;
        $cate->bc_summary = $request->blogCatesSummary;
        $cate->bc_alias = $request->blogCatesAlias;
        $cate->bc_meta_title = $request->blogCatesMetaTitle;
        $cate->bc_meta_description = $request->blogCatesMetaDescription;
        $cate->bc_active = $request->blogCatesActive;
        if (!empty($request->file('blogCatesImage'))) {
            $file_name = rand().$request->file('blogCatesImage')->getClientOriginalName();
            $cate->bc_avatar                 =     $file_name;
            $request->file('blogCatesImage')->move('uploads/imageCateBlog/',$file_name);
            if (File::exists($img_hientai)) {
                File::delete($img_hientai);
            }
        }
        else 
        {
                echo 'ko có';
        }
        $cate->save();
        if ($cate->bc_id)
        {
            return redirect('admin/blog/categories')->with('success','Cập nhật bài viết thành công');
        }
        else
        {
            return back()->with('error', 'Cập nhật bài viết không thành công');
        }
    }
    public function deleteCategory($id)
    {
        $blogcate = BlogCate::where('bc_id',$id)->first();
        File::delete('uploads/imageCateBlog/'.$blogcate->bc_avatar);
        $blogcate->delete($id);
        return redirect('admin/blog/categories');
    }
}
