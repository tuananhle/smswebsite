<?php

Route::group([ 'middleware' => [ 'web','CheckAdmin' ], 'prefix' => 'admin/blog', 'namespace' => 'Modules\Blog\Http\Controllers' ], function()
{
    Route::get('/', 'BlogController@index');
    Route::get('/listBlog', 'BlogController@listBlog');
    Route::get('/create', 'BlogController@create');
    Route::post('/create', 'BlogController@add');
    Route::post('/search','BlogController@search');
    Route::get('/edit/{id}', 'BlogController@getEdit');
    Route::post('/edit/{id}', 'BlogController@postEdit');
    Route::get('/delete/{id}','BlogController@getDelete');

    Route::group(['prefix' => 'categories'], function() {
        Route::get('/', 'CateBlogController@listCategory');
        Route::get('/create', 'CateBlogController@addCategory');
        Route::post('/create', 'CateBlogController@postAddCategory');
        Route::get('/edit/{id}-{alias}', 'CateBlogController@editCategory');
        Route::post('/edit/{id}-{alias}', 'CateBlogController@postEditCategory');
        Route::get('/delete/{id}', 'CateBlogController@deleteCategory');
    });
});
